﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 8/29/2014
-- Description:	Updates records in the SurveyTowerSiteDetail table
-- =============================================
CREATE PROCEDURE [dbo].[UpdateSurveyTowerSiteDetail] 
	@SurveyTowerSiteDetailId int,
	@Cab1From varchar(50),
	@Cab1ToCabinetFt decimal(7,2),
	@Cab2From varchar(50),
	@Cab2ToCabinetFt decimal(7,2),
	@Cab3From varchar(50),
	@Cab3ToCabinetFt decimal(7,2),
	@Cab4From varchar(50),
	@Cab4ToCabinetFt decimal(7,2),
	@Notes varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE SurveyTowerSiteDetail
	SET Cab1From = @Cab1From,
		Cab1ToCabinetFt = @Cab1ToCabinetFt,
		Cab2From = @Cab2From,
		Cab2ToCabinetFt = @Cab2ToCabinetFt,
		Cab3From = @Cab3From,
		Cab3ToCabinetFt = @Cab3ToCabinetFt,
		Cab4From = @Cab4From,
		Cab4ToCabinetFt = @Cab4ToCabinetFt,
		Notes = @Notes
	WHERE SurveyTowerSiteDetailId = @SurveyTowerSiteDetailId
END