﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 8/29/2014
-- Description:	Inserts a record into the SurveyTowerSiteDetail table
-- =============================================
CREATE PROCEDURE [dbo].[InsertSurveyTowerSiteDetail]
	@SurveyId int,
	@Cab1From varchar(50),
	@Cab1ToCabinetFt decimal(7,2),
	@Cab2From varchar(50),
	@Cab2ToCabinetFt decimal(7,2),
	@Cab3From varchar(50),
	@Cab3ToCabinetFt decimal(7,2),
	@Cab4From varchar(50),
	@Cab4ToCabinetFt decimal(7,2),
	@Notes varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO SurveyTowerSiteDetail (
		SurveyId,
		Cab1From,
		Cab1ToCabinetFt,
		Cab2From,
		Cab2ToCabinetFt,
		Cab3From,
		Cab3ToCabinetFt,
		Cab4From,
		Cab4ToCabinetFt,
		Notes)
	VALUES (
		@SurveyId,
		@Cab1From,
		@Cab1ToCabinetFt,
		@Cab2From,
		@Cab2ToCabinetFt,
		@Cab3From,
		@Cab3ToCabinetFt,
		@Cab4From,
		@Cab4ToCabinetFt,
		@Notes)

	SELECT SCOPE_IDENTITY()
END