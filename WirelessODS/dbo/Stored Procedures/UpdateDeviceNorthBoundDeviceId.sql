﻿-- =============================================
-- Author:		Kirk Johnson	
-- Create date: 6/11/2014
-- Description:	Updates a north bound device id.
-- =============================================
CREATE PROCEDURE [dbo].[UpdateDeviceNorthBoundDeviceId]
	@DeviceId				INT,
	@NorthBoundDeviceId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE [dbo].[Device]
	SET
		NorthBoundDeviceId = @NorthBoundDeviceId
	WHERE DeviceId = @DeviceId
END