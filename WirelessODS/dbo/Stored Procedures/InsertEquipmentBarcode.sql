﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/12/2014
-- Description:	Inserts a new equipment barcode.
-- =============================================
CREATE PROCEDURE [dbo].[InsertEquipmentBarcode]
	@Barcode					VARCHAR(50),
	@EquipmentId				INT,
	@EquipmentBarcodeTypeId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[EquipmentBarcode]
			   ([Barcode]
			   ,[EquipmentId]
			   ,[EnumEquipmentBarcodeTypeId])
		 VALUES
			   (@Barcode
			   ,@EquipmentId
			   ,@EquipmentBarcodeTypeId)

	SELECT SCOPE_IDENTITY()
END