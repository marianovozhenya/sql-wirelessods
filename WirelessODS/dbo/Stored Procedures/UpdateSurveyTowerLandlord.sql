﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 8/28/2014
-- Description:	Updates the SurveyTowerLandlord table
-- =============================================
CREATE PROCEDURE [dbo].[UpdateSurveyTowerLandlord] 
	@SurveyTowerLandlordId int,
	@GroundLandlordName varchar(100),
	@GroundLandlordContact varchar(100),
	@StructureLandlordName varchar(100),
	@StructureLandlordContact varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE SurveyTowerLandlord
	SET GroundLandlordName = @GroundLandlordName,
		GroundLandlordContact = @GroundLandlordContact,
		StructureLandlordName = @StructureLandlordName,
		StructureLandlordContact = @StructureLandlordContact
	WHERE SurveyTowerLandlordId = @SurveyTowerLandlordId
END