﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 6/3/2014
-- Description:	Truncate Azotel import tables.
-- =============================================
CREATE PROCEDURE [dbo].[ImportAzotelTruncateTables]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE FROM [dbo].[Interface]
	DELETE FROM [dbo].[Device]
	DELETE FROM [dbo].[Site]
	DELETE FROM [dbo].[Location]
	DELETE FROM [dbo].[EquipmentBarcode]
	DELETE FROM [dbo].[Equipment]
END