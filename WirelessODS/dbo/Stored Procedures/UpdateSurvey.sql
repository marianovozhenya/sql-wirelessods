﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 8/31/2014
-- Description:	Updates a row in survey.
-- =============================================
CREATE PROCEDURE [dbo].[UpdateSurvey]
	@SurveyId			INT,
	@SiteId				INT,
	@WorkOrderId		INT,
	@ClosedByUserId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE	@CurrentSurveyId INT

	SELECT @CurrentSurveyId = [SurveyId]
	FROM [dbo].[Survey] s
	WHERE s.[SiteId] = @SiteId 
		AND s.[EnumSurveyTypeId] = (SELECT s2.[EnumSurveyTypeId]
									FROM [dbo].[Survey] s2
									WHERE s2.SurveyId = @SurveyId) 
		AND [IsCurrent] = 1

	UPDATE [dbo].[Survey]
	SET [IsCurrent] = 0
	WHERE [SurveyId] = @CurrentSurveyId

    -- Insert statements for procedure here
	UPDATE [dbo].[Survey]
	SET [EnumSurveyStatusId] = 2 -- Complete
		,[ClosedAtUtc] = GETUTCDATE()
		,[ClosedByUserId] = @ClosedByUserId
		,[IsCurrent] = 1
	WHERE [SurveyId] = @SurveyId

	DECLARE @SurveyIdChar VARCHAR(100)
	SET @SurveyIdChar = CONVERT(VARCHAR(100), @SurveyId)
	EXEC [dbo].[InsertSurveyChangeLog] @SiteId, @WorkOrderId, 'SurveyId', @SurveyIdChar, @ClosedByUserId
END