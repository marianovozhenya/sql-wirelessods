﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/20/2014
-- Description:	Selects the photos for a site ordered by the newest first.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTablePhotosBySiteId]
	@SiteId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Photo]
	WHERE SiteId = @SiteId AND IsSoftDeleted = 0
	ORDER BY CreatedAtUtc DESC
END