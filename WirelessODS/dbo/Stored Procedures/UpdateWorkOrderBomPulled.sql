﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 7/1/2014
-- Description:	This proceedure will update the number pulled on a work order bom
-- =============================================
CREATE PROCEDURE [dbo].[UpdateWorkOrderBomPulled] 
	-- Add the parameters for the stored procedure here
	@WorkOrderBomId int = 0, 
	@Pulled int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update WorkOrderBom
	set Pulled = @Pulled
	where WorkOrderBomId = @WorkOrderBomId
END