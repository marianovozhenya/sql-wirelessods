﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 5/19/2014
-- Description:	Selects a photo type by id
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowPhotoTypeById]
	@PhotoTypeId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[PhotoType]
	WHERE PhotoTypeId = @PhotoTypeId
END