﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 8/1/2014
-- Description:	Selects User records based on CrewId
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableUserByCrewId] 
	@CrewId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT 
		u.*,
		c.Username,
		d.Name [District],
		cr.Name [Crew]
	FROM [User] u
		JOIN UserCredential c ON c.UserId = u.UserId
		LEFT OUTER JOIN District d ON d.DistrictId = u.DistrictId
		LEFT OUTER JOIN Crew cr ON cr.CrewId = u.CrewId
	WHERE u.CrewId = @CrewId
END