﻿-- =============================================
-- Author:		Bryan Call
-- Create date: 4/23/2014
-- Description:	Updates a site in the database.
-- =============================================
CREATE PROCEDURE [dbo].[UpdateSite] 
	@SiteId					INT,
	@Name					VARCHAR(100),
	@EnumSiteTypeId			INT,
	@EnumSiteStatusId		INT,
	@Place					VARCHAR(MAX),
	@Directions				VARCHAR(MAX),
	@HubId					INT,
	@LastUpdatedByUserId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.Site
	SET
		Name = @Name,
		EnumSiteTypeId = @EnumSiteTypeId,
		EnumSiteStatusId = @EnumSiteStatusId,
		Place = @Place,
		Directions = @Directions,
		HubId = @HubId,
		LastUpdatedByUserId = @LastUpdatedByUserId,
		LastUpdatedAtUtc = GETUTCDATE()
	WHERE [SiteId] = @SiteId
END