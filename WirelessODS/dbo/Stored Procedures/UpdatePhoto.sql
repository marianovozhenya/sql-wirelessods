﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/20/2014
-- Description:	Updates a photo
-- =============================================
CREATE PROCEDURE [dbo].[UpdatePhoto]
	@PhotoId					INT,
	@Name						VARCHAR(100),
	@PhotoTypeId				INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE [dbo].[Photo]
	SET
		[Name] = @Name,
		PhotoTypeId = @PhotoTypeId
	WHERE PhotoId = @PhotoId
END