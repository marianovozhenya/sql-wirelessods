﻿-- =============================================
-- Author:		Bryan Call
-- Create date: 5/13/2014
-- Description:	Updates  Device table entry
-- =============================================
CREATE PROCEDURE [dbo].[UpdateDevice] 
	
	@DeviceId						INT,
	@CustomName						VARCHAR(250),
	@EnumDeviceUseTypeId			INT,
	@EnumDeviceStatusId				INT,
	@IPv4Address					BINARY(4),
	@MacAddress						BINARY(6),
	@WirelessMacAddress				BINARY(6),
	@LastUpdatedByUserId			INT,
	@NorthBoundDeviceId				INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert row into history
	INSERT INTO dbo.DeviceHistory
	        ( UpdatedAtUtc ,
	          UpdatedByUserId ,
	          DeviceId ,
	          Name ,
	          CustomName ,
	          EquipmentId ,
	          EnumDeviceUseTypeId ,
	          SiteId ,
	          IPv4Address ,
	          MacAddress ,
			  WirelessMacAddress,
	          EnumDeviceStatusId,
			  NorthBoundDeviceId,
			  WorkOrderId
	        )
	SELECT
		GETUTCDATE(),
		@LastUpdatedByUserId,
		@DeviceId,
		a.Name,
		CustomName,
		EquipmentId,
		EnumDeviceUseTypeId,
		SiteId,
		IPv4Address,
		MacAddress,
		WirelessMacAddress,
		EnumDeviceStatusId,
		NorthBoundDeviceId,
		WorkOrderId
	FROM dbo.Device a
	LEFT JOIN Interface b ON (a.DeviceId = b.DeviceId AND b.EnumInterfaceTypeId = 1)
	WHERE a.DeviceId = @DeviceId

    UPDATE dbo.Device
	SET
		Name = (SELECT StringRepresentation FROM DeviceIdMap WHERE DeviceId = @DeviceId) + '-' + @CustomName,
		CustomName = @CustomName,
		EnumDeviceUseTypeId = @EnumDeviceUseTypeId,
		EnumDeviceStatusId = @EnumDeviceStatusId,
		LastUpdatedAtUtc = GETUTCDATE(),
		LastUpdatedByUserId = @LastUpdatedByUserId,
		NorthBoundDeviceId = @NorthBoundDeviceId,
		WirelessMacAddress = @WirelessMacAddress
	WHERE DeviceId = @DeviceId

	UPDATE dbo.Interface
	SET
		IPv4Address = @IPv4Address,
		MacAddress = @MacAddress
	WHERE DeviceId = @DeviceId AND EnumInterfaceTypeId = 1
END