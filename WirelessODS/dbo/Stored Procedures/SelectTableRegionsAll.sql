﻿-- =============================================
-- Author:		John Rubin
-- Create date: 07/17/2014
-- Description:	Retrieves all Regions
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableRegionsAll] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * 
	FROM Region
END