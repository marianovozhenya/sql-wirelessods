﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/20/2014
-- Description:	Marks the photo as deleted
-- =============================================
CREATE PROCEDURE [dbo].[UpdatePhotoSoftDelete]
	@PhotoId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE [dbo].[Photo]
	SET
		IsSoftDeleted = 1,
		DeletedAtUtc = GETUTCDATE()
	WHERE PhotoId = @PhotoId
END