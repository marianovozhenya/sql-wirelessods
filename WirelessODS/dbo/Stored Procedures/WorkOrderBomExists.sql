﻿-- =============================================
-- Author:		John Rubin
-- Create date: 07/03/2014
-- Description:	Determines if a Work Order Bom containing certain foreign key values exists.
-- =============================================
CREATE PROCEDURE [dbo].[WorkOrderBomExists] 
	-- Add the parameters for the stored procedure here
	@WorkOrderId int = 0, 
	@EquipmentId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT WorkOrderBomId
	FROM WorkOrderBom 
	WHERE WorkOrderId = @WorkOrderId AND EquipmentId = @EquipmentId
END