﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 4/23/2014
-- Description:	Updates the IsSoftDelete field on an attachment to 1 for soft deletes of attachment.
-- =============================================
CREATE PROCEDURE [dbo].[UpdateAttachmentSoftDelete] 
	@AttachmentId					INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE Attachment
	SET
		IsSoftDeleted = 1,
		DeletedAtUtc = GETUTCDATE()
	WHERE AttachmentId = @AttachmentId
END