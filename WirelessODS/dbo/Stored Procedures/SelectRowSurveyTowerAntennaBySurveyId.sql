﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 9/2/2014
-- Description:	Selects a record from the SurveyTowerAntenna table by SurveyId
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowSurveyTowerAntennaBySurveyId] 
	@SurveyId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT *
	FROM [SurveyTowerAntenna]
	WHERE [SurveyId] = @SurveyId
END