﻿
-- =============================================
-- Author:		Andrew Larsson
-- Create date: 07/16/2014
-- Description:	Inserts a District Zip Code.
-- =============================================
CREATE PROCEDURE [dbo].[InsertZipCodeXDistrict]
	-- Add the parameters for the stored procedure here
	@DistrictId int,
	@ZipCode varchar(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[ZipCodeXDistrict]
           ([DistrictId]
           ,[ZipCode])
     VALUES
           (@DistrictId
           ,@ZipCode)

	SELECT SCOPE_IDENTITY()
END