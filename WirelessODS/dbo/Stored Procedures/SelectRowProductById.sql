﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/14/2014
-- Description:	Selects a row from the Product table by id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowProductById]
	@ProductId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Product]
	WHERE ProductId = @ProductId
END