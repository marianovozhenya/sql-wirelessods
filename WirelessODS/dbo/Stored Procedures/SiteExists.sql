﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 4/24/2014
-- Description:	This Proceedure will check to see if a site name already exists in the system
-- =============================================
CREATE PROCEDURE [dbo].[SiteExists] 
	-- Add the parameters for the stored procedure here
	@SiteId INT,
	@SiteName varchar(100) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF(@SiteId > 0)
	BEGIN
		SELECT SiteId
		FROM Site
		WHERE SiteId <> @SiteId AND Name = @SiteName
	END
	
	IF(@SiteId = 0)
	BEGIN
		SELECT SiteId
		FROM Site
		WHERE Name = @SiteName
	END

    -- Insert statements for procedure here
	
END