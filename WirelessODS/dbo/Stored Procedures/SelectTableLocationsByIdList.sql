﻿-- =============================================
-- Author:		Bryan Call
-- Create date: 4/18/2014
-- Description:	Selects location records by a list of ids.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableLocationsByIdList] 
	@IdList			AS IdList	READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Location]
	WHERE LocationId IN (SELECT Id FROM @IdList)
END