﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 7/8/2014
-- Description:	Updates a Work Order
-- =============================================
CREATE PROCEDURE [dbo].[UpdateWorkOrder] 
	@WorkOrderId int,
	@ContactName varchar(100),
	@ContactPhone varchar(20),
	@UpdatedByUserId int,
	@SiteId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE WorkOrder
	SET ContactName = @ContactName,
		ContactPhone = @ContactPhone,
		LastUpdatedByUserId = @UpdatedByUserId,
		LastUpdatedAtUtc = GETUTCDATE(),
		SiteId = @SiteId
    WHERE WorkOrderId = @WorkOrderId
END