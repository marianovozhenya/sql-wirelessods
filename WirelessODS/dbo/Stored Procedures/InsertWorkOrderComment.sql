﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 6/16/2014
-- Description:	This Proceedure will insert a new row into the workordercomments table
-- =============================================
CREATE PROCEDURE [dbo].[InsertWorkOrderComment] 
	-- Add the parameters for the stored procedure here
			@WorkOrderId  int
           ,@EnumWorkOrderCommentTypeId int
           ,@Rating int
           ,@EnumWorkOrderCommentPriorityId int
           ,@Comment varchar(max)
           ,@CreatedByUserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	--
	
	INSERT INTO [dbo].[WorkOrderComment]
           ([WorkOrderId]
           ,[EnumWorkOrderCommentTypeId]
           ,[Rating]
           ,[EnumWorkOrderCommentPriorityId]
           ,[Comment]
           ,[CreatedByUserId]
           ,[CreatedAtUtc])
     VALUES
           (@WorkOrderId
           ,@EnumWorkOrderCommentTypeId
           ,@Rating
           ,@EnumWorkOrderCommentPriorityId
           ,@Comment
           ,@CreatedByUserId
           ,GETUTCDATE())

		   select SCOPE_IDENTITY()

	--
END