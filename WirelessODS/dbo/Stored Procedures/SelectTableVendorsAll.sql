﻿-- =============================================
-- Author:		John Rubin
-- Create date: 05/08/2014
-- Description:	Retrieves all Vendors
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableVendorsAll] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM
	[dbo].[Vendor] 
	ORDER BY Name
END