﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 4/16/2014
-- Description:	Selects a Location by id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowLocationById]
	@LocationId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Location]
	WHERE LocationId = @LocationId
END