﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 9/3/2014
-- Description:	Selects a row from SurveyDevice by SurveyId
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableSurveyDeviceBySurveyId] 
	@SurveyId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT *
	FROM SurveyDevice
	WHERE SurveyId = @SurveyId
END