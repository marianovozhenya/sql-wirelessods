﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 6/16/2014
-- Description:	This proceedure will select a row from the workOrderComment table by id
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableWorkOrderCommentByWorkOrderId] 
	-- Add the parameters for the stored procedure here
	@WorkOrderId int,
	@TypeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT [WorkOrderCommentId]
      ,[WorkOrderId]
      ,[EnumWorkOrderCommentTypeId]
      ,[Rating]
      ,[EnumWorkOrderCommentPriorityId]
      ,[Comment]
      ,[CreatedByUserId]
      ,[CreatedAtUtc]
	FROM [dbo].[WorkOrderComment]
	where WOrkOrderId =  @WorkOrderId
	And EnumWorkOrderCommentTypeId = @TypeId
END

Grant Execute on [SelectTableWorkOrderCommentByWorkOrderId] to ODS