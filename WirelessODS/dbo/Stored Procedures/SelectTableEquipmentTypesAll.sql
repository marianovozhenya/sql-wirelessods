﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/8/2014
-- Description:	Selects all equipment types
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableEquipmentTypesAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[EquipmentType]
	ORDER BY Name
END