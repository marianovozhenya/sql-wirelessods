﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/17/2014
-- Description:	Updates a row in the service table.
-- =============================================
CREATE PROCEDURE [dbo].[UpdateService]
	@ServiceId		INT,
	@AddressId		INT,
	@DeviceId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Service]
	   SET [DeviceId] = @DeviceId
		  ,[AddressId] = @AddressId
	 WHERE [ServiceId] = @ServiceId
END