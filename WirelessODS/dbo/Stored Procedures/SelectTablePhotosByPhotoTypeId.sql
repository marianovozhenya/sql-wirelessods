﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/16/2014
-- Description:	Selects the photos for a type ordered by the newest first.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTablePhotosByPhotoTypeId]
	@PhotoTypeId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Photo]
	WHERE PhotoTypeId = @PhotoTypeId AND IsSoftDeleted = 0
	ORDER BY CreatedAtUtc DESC
END