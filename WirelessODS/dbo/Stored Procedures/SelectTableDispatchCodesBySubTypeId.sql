﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 6/16/2014
-- Description:	Selects dispatch codes by sub-type id
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableDispatchCodesBySubTypeId]
	@WorkOrderSubTypeId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[DispatchCode]
	WHERE WorkOrderSubTypeId = @WorkOrderSubTypeId
	ORDER BY Name
END