﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 9/4/2014
-- Description:	Selects all rows from survey
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableSurveyAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [dbo].[Survey]
END