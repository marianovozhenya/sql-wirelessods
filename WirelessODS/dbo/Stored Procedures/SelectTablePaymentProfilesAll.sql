﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/22/2014
-- Description:	Selects all payment profile rows.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTablePaymentProfilesAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		p.*,
		co.FirstName,
		co.LastName,
		co.EmailAddress,
		cu.BillingAddressId
	FROM [dbo].[PaymentProfile] p
	JOIN [dbo].[ContactXCustomer] cx ON cx.CustomerId = p.CustomerId AND cx.EnumCustomerContactTypeId = 1
	JOIN [dbo].[Contact] co ON co.ContactId = cx.ContactId
	JOIN [dbo].[Customer] cu ON cu.CustomerId = p.CustomerId
END