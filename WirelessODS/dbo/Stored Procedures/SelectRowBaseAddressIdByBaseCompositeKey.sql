﻿
-- =============================================
-- Author:		Troy Sampson
-- Create date: 7/14/2014
-- Description:	This stored proceedure will select a row from the BaseAddress table based on id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowBaseAddressIdByBaseCompositeKey] 
	-- Add the parameters for the stored procedure here
	@BaseCompositeKey			varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		[BaseAddressId]
	FROM [dbo].[BaseAddress]
	WHERE [BaseCompositeKey] = @BaseCompositeKey  
END