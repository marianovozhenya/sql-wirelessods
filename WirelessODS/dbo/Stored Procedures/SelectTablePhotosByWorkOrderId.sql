﻿-- =============================================
-- Author:		JOhn Rubin
-- Create date: 07/09/2014
-- Description:	Selects photos for a work order ordered by newest first.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTablePhotosByWorkOrderId]
	@WorkOrderId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Photo]
	WHERE WorkOrderId = @WorkOrderId AND IsSoftDeleted = 0
	ORDER BY CreatedAtUtc DESC
END