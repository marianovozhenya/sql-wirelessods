﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 7/31/2014
-- Description:	Selects a Crew record's associated District record
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowDistrictByCrewId] 
	@CrewId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT d.*
	FROM District d
		JOIN Crew c ON c.DistrictId = d.DistrictId
	WHERE c.CrewId = @CrewId
END