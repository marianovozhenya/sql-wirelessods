﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/20/2014
-- Description:	Inserts a photo
-- =============================================
CREATE PROCEDURE [dbo].[InsertPhoto]
	@Name						VARCHAR(100),
	@PhotoTypeId				INT,
	@EnumUploadStatusId			INT,
	@LocationId					INT,
	@SiteId						INT,
	@S3Bucket					VARCHAR(100),
	@S3LocationForFullSize		VARCHAR(MAX),
	@S3LocationForMediumSize	VARCHAR(MAX),
	@Thumbnail					VARBINARY(MAX),
	@CreatedByUserId			INT,
	@WorkOrderId				INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Photo]
			   ([Name]
			   ,[PhotoTypeId]
			   ,[S3Bucket]
			   ,[S3LocationForMediumSize]
			   ,[S3LocationForFullSize]
			   ,[LocationId]
			   ,[SiteId]
			   ,[EnumUploadStatusId]
			   ,[IsSoftDeleted]
			   ,[CreatedAtUtc]
			   ,[CreatedByUserId]
			   ,[Thumbnail]
			   ,[WorkOrderId])
		 VALUES
			   (@Name
			   ,@PhotoTypeId
			   ,@S3Bucket
			   ,@S3LocationForMediumSize
			   ,@S3LocationForFullSize
			   ,@LocationId
			   ,@SiteId
			   ,@EnumUploadStatusId
			   ,0
			   ,GETUTCDATE()
			   ,@CreatedByUserId
			   ,@Thumbnail
			   ,@WorkOrderId)

	SELECT SCOPE_IDENTITY()
END