﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/6/2014
-- Description:	Checks to see if a hub name already exists in the system
-- =============================================
CREATE PROCEDURE [dbo].[HubExists]
	@HubId		INT,
	@HubName	VARCHAR(100) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (@HubId > 0)
	BEGIN
		SELECT COUNT(HubId)
		FROM [dbo].[Hub]
		WHERE HubId <> @HubId AND Name = @HubName
	END
	
	IF (@HubId <= 0)
	BEGIN
		SELECT COUNT(HubId)
		FROM [dbo].[Hub]
		WHERE Name = @HubName
	END
END