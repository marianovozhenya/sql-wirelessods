﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/31/2014
-- Description:	Updates a row in the payment profile table.
-- =============================================
CREATE PROCEDURE [dbo].[UpdatePaymentProfile]
	@PaymentProfileId		INT,
	@CardLastFour			INT,
	@CardCode				INT,
	@CardHolder				VARCHAR(100),
	@CardType				VARCHAR(30),
	@ExpirationMonth		TINYINT,
	@ExpirationYear			INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[PaymentProfile]
	SET
		 [CardLastFour] = @CardLastFour
		,[ExpirationMonth] = @ExpirationMonth
		,[ExpirationYear] = @ExpirationYear
		,[CardCode] = @CardCode
		,[CardHolder] = @CardHolder
		,[CardType] = @CardType
	WHERE [PaymentProfileId] = @PaymentProfileId
END