﻿-- =============================================
-- Author:		John Rubin
-- Create date: 08/04/2014
-- Description:	Loads a dispatch code by id.  Dangit, why wasn't this in here years ago?
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowDispatchCodeById] 
	-- Add the parameters for the stored procedure here
	@DispatchCodeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * 
	FROM DispatchCode
	WHERE DispatchCodeId = @DispatchCodeId
END