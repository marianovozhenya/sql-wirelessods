﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 7/16/2014
-- Description:	This proceedure will insert a row into the Customer Table
-- =============================================
CREATE PROCEDURE [dbo].[InsertCustomer] 
	-- Add the parameters for the stored procedure here
	@AzotelId					INT,
    @BillingAddressId			INT,
    @PrivateNotes				VARCHAR(MAX),
	@AzotelAuthorizeNetToken	VARCHAR(100),
	@CreatedByUserId			INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Customer]
           ([AzotelId]
           ,[BillingAddressId]
           ,[PrivateNotes]
		   ,[AzotelAuthorizeNetToken]
		   ,[CreatedAtUtc]
		   ,[CreatedByUserId])
     VALUES
           (@AzotelId
           ,@BillingAddressId
           ,@PrivateNotes
		   ,@AzotelAuthorizeNetToken
		   ,GETUTCDATE()
		   ,@CreatedByUserId)

	SELECT SCOPE_IDENTITY()

END