﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 6/16/2014
-- Description:	This proceedure will select a row from the workOrderComment table by id
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowWorkOrderCommentById] 
	-- Add the parameters for the stored procedure here
	@WorkOrderCommentId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	SELECT [WorkOrderCommentId]
      ,[WorkOrderId]
      ,[EnumWorkOrderCommentTypeId]
      ,[Rating]
      ,[EnumWorkOrderCommentPriorityId]
      ,[Comment]
      ,[CreatedByUserId]
      ,[CreatedAtUtc]
	FROM [dbo].[WorkOrderComment]
	where WOrkOrderCommentId =  @WorkOrderCommentId
END