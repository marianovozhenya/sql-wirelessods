﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/13/2014
-- Description:	Checks to see if an equipment barcode already exists in the system
-- =============================================
CREATE PROCEDURE [dbo].[EquipmentBarcodeExists]
	@EquipmentId		INT,
	@Barcode			VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (@EquipmentId > 0)
	BEGIN
		SELECT COUNT(EquipmentBarcodeId)
		FROM [dbo].[EquipmentBarcode]
		WHERE EquipmentId <> @EquipmentId AND Barcode = @Barcode
	END

	IF (@EquipmentId <= 0)
	BEGIN
		SELECT COUNT(EquipmentBarcodeId)
		FROM [dbo].[EquipmentBarcode]
		WHERE Barcode = @Barcode
	END
END