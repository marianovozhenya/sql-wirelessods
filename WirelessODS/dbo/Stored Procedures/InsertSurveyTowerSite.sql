﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 8/29/2014
-- Description:	Inserts a row into SurveyTowerSite
-- =============================================
CREATE PROCEDURE [dbo].[InsertSurveyTowerSite] 
	-- Add the parameters for the stored procedure here
	@SurveyId int,
	@StructureHeight decimal(7,2),
	@ProposedTowerHeight decimal(7,2),
	@GroundTerrainOther varchar(100),
	@EnumInsideGroundTerrainId int,
	@EnumOutsideGroundTerrainId int,
	@EnumSurveyTowerTypeId int,
	@EnumSurveyTowerStatusId int,
	@EnumStructureTypeId int,
	@Notes varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO [dbo].[SurveyTowerSite] (
		[SurveyId],
		[StructureHeight],
		[ProposedTowerHeight],
		[GroundTerrainOther],
		[EnumInsideGroundTerrainId],
		[EnumOutsideGroundTerrainId],
		[EnumSurveyTowerTypeId],
		[EnumSurveyTowerStatusId],
		[EnumStructureTypeId],
		[Notes])
	VALUES (
		@SurveyId,
		@StructureHeight,
		@ProposedTowerHeight,
		@GroundTerrainOther,
		@EnumInsideGroundTerrainId,
		@EnumOutsideGroundTerrainId,
		@EnumSurveyTowerTypeId,
		@EnumSurveyTowerStatusId,
		@EnumStructureTypeId,
		@Notes)

	SELECT SCOPE_IDENTITY()
END