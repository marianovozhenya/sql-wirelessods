﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 7/24/2014
-- Description:	This proceedure will return if there is one or more of the contactType for a given customerid
-- =============================================
CREATE PROCEDURE [dbo].[SelectCustomerContactTypeExists] 
	-- Add the parameters for the stored procedure here
	@CustomerId int = 0, 
	@ContactTypeId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  Count(*)
	from ContactXCustomer
	where CustomerId = @CustomerId
	And EnumCustomerContactTypeId = @ContactTypeId
END