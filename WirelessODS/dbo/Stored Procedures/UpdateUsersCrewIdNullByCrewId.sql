﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 8/1/2014
-- Description:	Updates all Crew Users CrewId to null
-- =============================================
CREATE PROCEDURE [dbo].[UpdateUsersCrewIdNullByCrewId] 
	@CrewId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [User]
	SET CrewId = null
	WHERE CrewId = @CrewId
END