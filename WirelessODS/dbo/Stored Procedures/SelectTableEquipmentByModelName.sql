﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/9/2014
-- Description:	Selects equipment by model
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableEquipmentByModelName]
	@ModelName		VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Equipment]
	WHERE Model LIKE @ModelName
	ORDER BY Name
END