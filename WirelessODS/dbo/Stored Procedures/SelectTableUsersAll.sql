﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 4/10/2014
-- Description:	Selects all user rows
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableUsersAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		c.Username,
		u.*,
		ISNULL(d.Name, '') [District],
		ISNULL(cr.Name, '') [Crew]
	FROM dbo.[User] u
		INNER JOIN dbo.UserCredential c ON u.UserId = c.UserId
		LEFT OUTER JOIN dbo.District d ON d.DistrictId = u.DistrictId
		LEFT OUTER JOIN dbo.Crew cr ON cr.CrewId = u.CrewId
	ORDER BY u.FirstName
END