﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/6/2014
-- Description:	Selects a hub by id
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowHubById]
	@HubId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Hub]
	WHERE HubId = @HubId
END