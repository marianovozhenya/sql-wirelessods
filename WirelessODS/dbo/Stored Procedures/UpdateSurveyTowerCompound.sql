﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 8/29/2014
-- Description:	Updates the SurveyTowerCompound table
-- =============================================
CREATE PROCEDURE [dbo].[UpdateSurveyTowerCompound] 
	@SurveyTowerCompoundId INT,
	@EnumEquipmentPlacementId INT,
	@EquipmentOther VARCHAR(50),
	@CompoundLength DECIMAL(7,2),
	@CompoundWidth DECIMAL(7,2),
	@IsSpaceInCompound BIT,
	@IsAdaquateGrounding BIT,
	@IsSpaceInShelter BIT,
	@IsProximityIssue BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [dbo].[SurveyTowerCompound]
	SET [EnumEquipmentPlacementId] = @EnumEquipmentPlacementId,
		[EquipmentOther] = @EquipmentOther,
		[CompoundLength] = @CompoundLength,
		[CompoundWidth] = @CompoundWidth,
		[IsSpaceInCompound] = @IsSpaceInCompound,
		[IsAdaquateGrounding] = @IsAdaquateGrounding,
		[IsSpaceInShelter] = @IsSpaceInShelter,
		[IsProximityIssue] = @IsProximityIssue
	WHERE [SurveyTowerCompoundId] = @SurveyTowerCompoundId
END