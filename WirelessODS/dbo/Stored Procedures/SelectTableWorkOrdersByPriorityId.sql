﻿-- Stored Procedure

-- =============================================
-- Author:		Kirk Johnson
-- Create date: 6/14/2014
-- Description:	Selects a list from the work orders table based on type.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableWorkOrdersByPriorityId] 
	-- Add the parameters for the stored procedure here
	@PriorityId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	--
	SELECT 
	    wo.*
	  ,st.name as WorkOrderSubType
	  ,d.[Name] as DispatchCode
	  --,s.LocationId
	  ,s.Name as SiteName
	  ,a.AddressLine as AddressLine
	  ,ba.City
	  ,ba.State
	  ,ba.Zip
	  ,sy.SurveyId
  FROM [dbo].[WorkOrder] wo
  left Join Site s on wo.SiteId = s.SiteId
  left join Survey sy on wo.WorkOrderId = sy.WorkOrderId
  Inner Join [Address] a on wo.AddressId = a.AddressId
  Inner Join BaseAddress ba on a.BaseAddressId = ba.BaseAddressId
  Inner Join dbo.[WorkOrderSubType] st ON wo.WorkOrderSubTypeId = st.WorkOrderSubTypeId
  Inner Join [dbo].[DispatchCode] d on wo.[DispatchCodeId] = d.[DispatchCodeId]
  WHERE wo.EnumWorkOrderPriorityId = @PriorityId
  ORDER BY wo.WorkOrderId DESC

END