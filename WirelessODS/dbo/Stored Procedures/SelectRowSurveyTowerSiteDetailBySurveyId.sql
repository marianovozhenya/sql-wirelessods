﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 8/29/2014
-- Description:	Selects a row from SurveyTowerSiteDetail by SurveyTowerSiteDetailSurveyId
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowSurveyTowerSiteDetailBySurveyId] 
	@SurveyId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT *
	FROM SurveyTowerSiteDetail
	WHERE SurveyId = @SurveyId
END