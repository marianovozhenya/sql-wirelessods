﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/8/2014
-- Description:	Checks to see if an equipment name already exists in the system
-- =============================================
CREATE PROCEDURE [dbo].[EquipmentExists]
	@EquipmentId		INT,
	@EquipmentName		VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (@EquipmentId > 0)
	BEGIN
		SELECT EquipmentId
		FROM [dbo].[Equipment]
		WHERE EquipmentId <> @EquipmentId AND Name = @EquipmentName
	END

	IF (@EquipmentId <= 0)
	BEGIN
		SELECT EquipmentId
		FROM [dbo].[Equipment]
		WHERE Name = @EquipmentName
	END
END