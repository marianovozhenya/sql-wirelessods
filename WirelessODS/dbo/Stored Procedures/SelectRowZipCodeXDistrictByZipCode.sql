﻿
-- =============================================
-- Author:		Andrew Larsson
-- Create date: 07/17/14
-- Description:	Retrieves a District Zip Code by Zip Code
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowZipCodeXDistrictByZipCode]
	-- Add the parameters for the stored procedure here
	@ZipCode varchar(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT z.*
	FROM ZipCodeXDistrict z
	WHERE ZipCode = @ZipCode
END