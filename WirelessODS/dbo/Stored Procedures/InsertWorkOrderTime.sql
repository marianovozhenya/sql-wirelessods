﻿
-- =============================================
-- Author:		Andrew Larsson
-- Create date: 6/17/2014
-- Description:	Inserts a Work Order Time entry
-- =============================================
CREATE PROCEDURE [dbo].[InsertWorkOrderTime] 
	-- Add the parameters for the stored procedure here
	@UserId int,
	@WorkOrderId int,  
	@WorkOrderPhaseId int, 
	@EnteredPhaseAtUtc datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO
		[dbo].[WorkOrderTime] (
			UserId,
			WorkOrderId,
			WorkOrderPhaseId,
			InAtUtc,
			OutAtUtc
		) VALUES (
			@UserId,
			@WorkOrderId,
			@WorkOrderPhaseId,
			@EnteredPhaseAtUtc,
			GETUTCDATE()
		)

	SELECT SCOPE_IDENTITY();

END