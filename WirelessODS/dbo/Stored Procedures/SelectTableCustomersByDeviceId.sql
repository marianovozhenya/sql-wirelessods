﻿-- Stored Procedure

-- =============================================
-- Author:		Kirk Johnson
-- Create date: 7/22/2014
-- Description:	Returns customer related data based on device id
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableCustomersByDeviceId] 
	@DeviceId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		cu.CustomerId,
		co.FirstName,
		co.LastName,
		co.EmailAddress [Email],
		co.PhoneNumber [Phone],
		instaddr.AddressFull [InstallationAddress],
		ISNULL(billaddr.AddressFull, '') [BillingAddress]
	FROM Customer cu
		JOIN ContactXCustomer cxc ON cxc.CustomerId = cu.CustomerId
		JOIN Contact co ON co.ContactId = cxc.ContactId
		LEFT OUTER JOIN [Address] billaddr ON billaddr.AddressId = cu.BillingAddressId
		LEFT OUTER JOIN [Service] s ON s.CustomerId = cu.CustomerId
		LEFT OUTER JOIN [Address] instaddr ON instaddr.AddressId = s.AddressId
	WHERE s.DeviceId = @DeviceId
		AND cxc.EnumCustomerContactTypeId = 1
END