﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 5/20/2014
-- Description:	This Proceedure will get a Truck by ID
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowTruckModelById]
	-- Add the parameters for the stored procedure here
	@TruckModelId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT [TruckModelId], [Name], [TruckMakeId]
  FROM [dbo].[TruckModel]
  where TruckModelId = @TruckModelId


END