﻿-- =============================================
-- Author:		John Rubin
-- Create date: 08/04/2014
-- Description:	Retrieves a work order ClickSchedulingProfile by Id 
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowClickSchedulingProfileById] 
	-- Add the parameters for the stored procedure here
	@SchedulingProfileId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT *
	FROM ClickSchedulingProfile
	WHERE ClickSchedulingProfileId = @SchedulingProfileId
END