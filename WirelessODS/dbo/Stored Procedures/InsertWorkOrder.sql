﻿
-- =============================================
-- Author:		Clint Roberts
-- Create date: 6/12/2014
-- Description:	This stored Proceedure will insert a row into the work order table
-- =============================================
CREATE PROCEDURE [dbo].[InsertWorkOrder] 
	@EnumWorkOrderType			INT
	,@WorkOrderSubTypeId		INT
	,@DispatchCodeId			INT
	,@EnumWorkOrderPriorityId	INT
	,@EnumWorkOrderStatusId		INT
	,@ContactName				VARCHAR(100)
	,@ContactPhone				VARCHAR(20)
	,@SiteId					INT
	,@LocationId				INT
	,@CustomerId				INT
	,@ServiceId					INT
	,@IsClosed					BIT
	,@IsScheduled				BIT
	,@CreatedByUserId			INT
	,@CreatedFromWorkOrderId	INT
	,@WorkOrderNumber			VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @AddressId INT
	IF @ServiceId IS NOT NULL
	BEGIN
		SELECT @AddressId = AddressId
		FROM [dbo].[Service]
		WHERE ServiceId = @ServiceId
	END
	ELSE
	BEGIN
		SELECT @AddressId = AddressId
		FROM [dbo].[Location]
		WHERE LocationId = @LocationId
	END
	  
	INSERT INTO [dbo].[WorkOrder]
           ([EnumWorkOrderTypeId]
           ,[WorkOrderSubTypeId]
           ,[DispatchCodeId]
           ,[EnumWorkOrderPriorityId]
           ,[EnumWorkOrderStatusId]
           ,[ContactName]
           ,[ContactPhone]
           ,[SiteId]
		   ,[LocationId]
		   ,[CustomerId]
		   ,[ServiceId]
           ,[AddressId]
		   ,[IsClosed]
		   ,[IsScheduled]
		   ,[CreatedByUserId]
		   ,[CreatedAtUtc]
		   ,[CreatedFromWorkOrderId]
		   ,[WorkOrderNumber])
     VALUES
           (@EnumWorkOrderType
           ,@WorkOrderSubTypeId
           ,@DispatchCodeId
           ,@EnumWorkOrderPriorityId
           ,@EnumWorkOrderStatusId
           ,@ContactName
           ,@ContactPhone
           ,@SiteId
		   ,@LocationId
		   ,@CustomerId
		   ,@ServiceId
           ,@AddressId
		   ,@IsClosed
		   ,@IsScheduled
		   ,@CreatedByUserId
		   ,GETUTCDATE()
		   ,@CreatedFromWorkOrderId
		   ,@WorkOrderNumber)

	SELECT SCOPE_IDENTITY()

END