﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 07/10/2014
-- Description:	This proceedure will select all rows by equipmentId
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableEquipmentChangeLogByEquipmentId] 
	-- Add the parameters for the stored procedure here
	@EquipmentId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select *
	from [dbo].[EquipmentChangeLog]
	where EquipmentId = @EquipmentId
END