﻿-- =============================================
-- Author:		John Rubin
-- Create date: 07/11/14
-- Description:	Retrieves all districts by region id
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableZipCodesByDistrictId]
	-- Add the parameters for the stored procedure here
	@DistrictId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT z.*
	FROM ZipCodeXDistrict z
	WHERE DistrictId = @DistrictId
END