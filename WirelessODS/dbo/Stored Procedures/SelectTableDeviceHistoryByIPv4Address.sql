﻿
-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/10/2014
-- Description:	Selects a list of device history by ipv4 address
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableDeviceHistoryByIPv4Address]
	@IPv4Address		VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		d.*,
		w.WorkOrderNumber
	FROM [dbo].[DeviceHistory] d
	INNER JOIN [dbo].[WorkOrder] w
		ON d.WorkOrderId = w.WorkOrderId
	WHERE CONVERT(VARCHAR(8), d.IPv4Address, 2) LIKE @IPv4Address
END