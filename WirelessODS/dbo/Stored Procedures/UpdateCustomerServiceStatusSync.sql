﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 7/30/2014
-- Description:	This proceedure will update the customer and service table with the status flag that the have been Synced with Azotel
-- =============================================
CREATE PROCEDURE [dbo].[UpdateCustomerServiceStatusSync] 
	@CustomerId int,
	@ServiceId int,
	@EnumCustomerStatusId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update [dbo].[Customer]
	Set [EnumCustomerStatusId] = @EnumCustomerStatusId
	where CustomerId = @CustomerId


	update [dbo].[Service]
	set [EnumServiceStatusId] = @EnumCustomerStatusId
	where ServiceId = @ServiceId
	

END