﻿
-- =============================================
-- Author:		Kirk Johnson
-- Create date: 6/9/2014
-- Description:	Selects all rows (users) with the associated truck id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableUsersByTruckId] 
	-- Add the parameters for the stored procedure here
	@TruckId				INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		a.*,
		b.Username
	FROM dbo.[User] a
	INNER JOIN dbo.UserCredential b ON (a.UserId = b.UserId)
	WHERE TruckId = @TruckId
END