﻿-- =============================================
-- Author:		Bryan Call
-- Create date: 4/7/2014
-- Description:	Select the user role ids associated with a given user id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableUserRoleIdsByUserId] 
	@UserId					INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		UserRoleId
	FROM UserXUserRole
	WHERE UserId = @UserId
END