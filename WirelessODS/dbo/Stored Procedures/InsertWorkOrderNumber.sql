﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 6/20/2014
-- Description:	This proceedure will update the workOrderNumber field with the new number
-- =============================================
CREATE PROCEDURE [dbo].[InsertWorkOrderNumber] 
	-- Add the parameters for the stored procedure here
	@WorkOrderId int = 0, 
	@WorkOrderNumber varchar(20) = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	update workorder
	set WorkOrderNumber = @WorkOrderNumber
	where WorkOrderId = @WorkOrderId


END