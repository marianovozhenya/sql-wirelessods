﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 7/7/2014
-- Description:	Searches Attachments by Work Order id
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableAttachmentsByWorkOrderId] 
	@WorkOrderId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		*
	FROM [dbo].[Attachment]
	WHERE WorkOrderId = @WorkOrderId AND IsSoftDeleted = 0
END