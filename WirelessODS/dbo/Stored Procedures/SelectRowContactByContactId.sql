﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 7/15/2014
-- Description:	This Proceedure will load the selected row by id
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowContactByContactId] 
	-- Add the parameters for the stored procedure here
	@ContactId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select *
	from Contact
	where ContactId = @ContactId
END