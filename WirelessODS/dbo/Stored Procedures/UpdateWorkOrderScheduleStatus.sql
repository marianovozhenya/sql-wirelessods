﻿-- =============================================
-- Author:		John Rubin
-- Create date: 08/04/2014
-- Description:	Updates a work order post-schedule
-- =============================================
CREATE PROCEDURE [dbo].[UpdateWorkOrderScheduleStatus] 
	-- Add the parameters for the stored procedure here
	@WorkOrderId int,
	@AssignedUserId int,
	@AssignedCrewId int,
	@IsScheduled bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE WorkOrder
	SET 
		ClickAssignedUserId = @AssignedUserId,
		ClickAssignedCrewId = @AssignedCrewId,
		IsScheduled = @IsScheduled
	WHERE WorkOrderId = @WorkOrderId
END