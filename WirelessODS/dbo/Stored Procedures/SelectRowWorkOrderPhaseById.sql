﻿
-- =============================================
-- Author:		Andrew Larsson
-- Create date: 7/25/2014
-- Description:	Selects a Work Order Phase by id
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowWorkOrderPhaseById]
	@WorkOrderPhaseId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[WorkOrderPhase]
	WHERE WorkOrderPhaseId = @WorkOrderPhaseId
END