﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 7/14/2014
-- Description:	This stored proceedure will select a row from the BaseAddress table based on id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowBaseAddressById] 
	-- Add the parameters for the stored procedure here
	@BaseAddressId			INT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		ba.[BaseAddressId]
		,ba.[BaseAddressFull]
		,ba.[BaseAddressLine]
		,ba.[EnumAddressValidationServiceId]
		,ba.[StreetNumber]
		,ba.[PreDirection]
		,ba.[StreetName]
		,ba.[StreetSuffix]
		,ba.[PostDirection]
		,ba.[City]
		,ba.[State]
		,ba.[Zip]
		,ba.[ValidationDate]
		,ba.[ValidationKey]
		,ba.[BaseCompositeKey]
	FROM [dbo].[BaseAddress] as ba
	WHERE ba.[BaseAddressId] = @BaseAddressId  

END