﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/20/2014
-- Description:	Selects a photo by id
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowPhotoById]
	@PhotoId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Photo]
	WHERE PhotoId = @PhotoId AND IsSoftDeleted = 0
END