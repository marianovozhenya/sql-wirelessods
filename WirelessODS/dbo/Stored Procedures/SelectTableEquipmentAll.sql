﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/8/2014
-- Description:	Selects all equipment
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableEquipmentAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Equipment]
	ORDER BY Name
END