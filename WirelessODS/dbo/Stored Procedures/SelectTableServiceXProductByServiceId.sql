﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/17/2014
-- Description:	Selects the products for a given service id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableServiceXProductByServiceId]
	@ServiceId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		x.ServiceXProductId,
		x.ServiceId,
		p.*
	FROM [dbo].[Product] p
	INNER JOIN [dbo].[ServiceXProduct] x
	ON p.ProductId = x.ProductId
	WHERE x.ServiceId = @ServiceId
END