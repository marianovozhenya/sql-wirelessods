﻿-- =============================================
-- Author:		Bryan Call
-- Create date: 5/13/2014
-- Description:	Inserts a new device
-- =============================================
CREATE PROCEDURE [dbo].[InsertDevice] 
	@CustomName					VARCHAR(250),
	@EquipmentId				INT,
	@EnumDeviceUseTypeId		INT,
	@SiteId						INT,
	@EnumDeviceStatusId			INT,
	@CreatedByUserId			INT,
	@MacAddress					BINARY(6),
	@WirelessMacAddress			BINARY(6),
	@IPv4Address				BINARY(4),
	@NorthBoundDeviceId			INT,
	@WorkOrderId				INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @DeviceId INT

	INSERT INTO dbo.Device
	        ( 
	          CustomName ,
	          EquipmentId ,
	          EnumDeviceUseTypeId ,
	          SiteId ,
	          EnumDeviceStatusId ,
	          CreatedAtUtc ,
	          CreatedByUserId,
			  NorthBoundDeviceId,
			  WorkOrderId,
			  WirelessMacAddress
	        )
	VALUES  ( 
	          @CustomName , -- CustomName - varchar(250)
	          @EquipmentId , -- EquipmentId - int
	          @EnumDeviceUseTypeId , -- EnumDeviceUseTypeId - int
	          @SiteId , -- SiteId - int
	          @EnumDeviceStatusId , -- EnumDeviceStatusId - int
	          GETUTCDATE() , -- CreatedAtUtc - datetime
	          @CreatedByUserId , -- CreatedByUserId - int
			  @NorthBoundDeviceId, -- NorthBoundDeviceId - int
			  @WorkOrderId,
			  @WirelessMacAddress
	        )
	
	SET @DeviceId = SCOPE_IDENTITY()

	UPDATE dbo.Device
	SET
		Name = (SELECT StringRepresentation FROM DeviceIdMap WHERE DeviceId = @DeviceId) + '-' + @CustomName
	WHERE DeviceId = @DeviceId

	INSERT INTO dbo.Interface
	        ( DeviceId ,
	          EnumInterfaceTypeId ,
	          Name ,
	          MacAddress ,
	          IPv4Address
	        )
	VALUES  ( @DeviceId , -- DeviceId - int
	          1 , -- this is the primary admin interface type 
	          'Primary Admin Interface' , -- Name - varchar(50)
	          @MacAddress , -- MacAddress - binary
	          @IPv4Address  -- IPv4Address - binary
	        )

	UPDATE [dbo].[WorkOrderBom]
	SET
		DeviceCount = DeviceCount + 1
	WHERE WorkOrderId = @WorkOrderId AND EquipmentId = @EquipmentId

	SELECT @DeviceId
END