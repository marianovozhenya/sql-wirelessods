﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/30/2014
-- Description:	Selects equipment by type id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableEquipmentByTypeId]
	@EquipmentTypeId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Equipment]
	WHERE EquipmentTypeId = @EquipmentTypeId
	ORDER BY Name
END