﻿
-- =============================================
-- Author:		Andrew Larsson
-- Create date: 9/2/2014
-- Description:	Inserts a row into SurveyTowerSiteAccess
-- =============================================
CREATE PROCEDURE [dbo].[InsertSurveyTowerSiteAccess] 
	-- Add the parameters for the stored procedure here
	@SurveyId INT,
	@IsLandlordNotificationRequired BIT,
	@IsMultipleGates BIT,
	@IsLivestockRoaming BIT,
	@EnumRoadTypeId INT,
	@EnumRoadConditionId INT,
	@AccessRoadOther VARCHAR(100),
	@AdditionalAccessInformation VARCHAR(MAX),
	@GateCombo VARCHAR(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO [dbo].[SurveyTowerSiteAccess] (
		[SurveyId],
		[IsLandlordNotificationRequired],
		[IsMultipleGates],
		[IsLivestockRoaming],
		[EnumRoadTypeId],
		[EnumRoadConditionId],
		[AccessRoadOther],
		[AdditionalAccessInformation],
		[GateCombo])
	VALUES (
		@SurveyId,
		@IsLandlordNotificationRequired,
		@IsMultipleGates,
		@IsLivestockRoaming,
		@EnumRoadTypeId,
		@EnumRoadConditionId,
		@AccessRoadOther,
		@AdditionalAccessInformation,
		@GateCombo)

	SELECT SCOPE_IDENTITY()
END