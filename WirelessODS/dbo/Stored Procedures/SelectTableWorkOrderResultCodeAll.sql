﻿-- =============================================
-- Author:		John Rubin
-- Create date: 06/17/2014
-- Description:	Gets the result code table
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableWorkOrderResultCodeAll] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM WorkOrderResultCode
END