﻿-- =============================================
-- Author:		Bryan Call
-- Create date: 4/23/2014
-- Description:	Updates the meta data on an attachment.
-- =============================================
CREATE PROCEDURE [dbo].[UpdateAttachment] 
	@AttachmentId					INT,
	@Name							VARCHAR(100),
	@AttachmentTypeId				INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE Attachment
	SET
		[Name] = @Name,
		AttachmentTypeId = @AttachmentTypeId
	WHERE AttachmentId = @AttachmentId
END