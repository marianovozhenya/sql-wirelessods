﻿
-- =============================================
-- Author:		John Rubin
-- Create date: 07/14/2014
-- Description:	Updates a District Zip Code's districtId
-- =============================================
CREATE PROCEDURE [dbo].[UpdateZipCodeXDistrict] 
	-- Add the parameters for the stored procedure here
	@ZipCodeXDistrictId int,
	@DistrictId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE ZipCodeXDistrict
	SET DistrictId = @DistrictId
	WHERE ZipCodeXDistrictId = @ZipCodeXDistrictId
END