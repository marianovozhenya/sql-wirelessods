﻿
-- Stored Procedure

-- =============================================
-- Author:		Andrew Larsson
-- Create date: 7/14/2014
-- Description:	Returns Users that are assigned to a Work Order
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableUsersByWorkOrderIdWithUpdlock] 
	@WorkOrderId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		c.Username,
		u.*,
		ISNULL(d.Name, '') [District],
		ISNULL(cr.Name, '') [Crew]
	FROM dbo.[User] u WITH (UPDLOCK)
		INNER JOIN dbo.UserCredential c ON u.UserId = c.UserId
		LEFT OUTER JOIN dbo.District d ON d.DistrictId = u.DistrictId
		LEFT OUTER JOIN dbo.Crew cr ON cr.CrewId = u.CrewId
		INNER JOIN dbo.WorkOrder w ON w.WorkOrderId = @WorkOrderId AND w.ClickAssignedUserId = u.UserId

	UNION

	SELECT
		c.Username,
		u.*,
		ISNULL(d.Name, '') [District],
		ISNULL(cr.Name, '') [Crew]
	FROM dbo.[User] u WITH (UPDLOCK)
		INNER JOIN dbo.UserCredential c ON u.UserId = c.UserId
		LEFT OUTER JOIN dbo.District d ON d.DistrictId = u.DistrictId
		LEFT OUTER JOIN dbo.Crew cr ON cr.CrewId = u.CrewId
		INNER JOIN dbo.WorkOrder w ON w.WorkOrderId = @WorkOrderId AND w.ClickAssignedCrewId = u.CrewId AND w.ClickAssignedCrewId IS NOT NULL

	UNION

	SELECT
		c.Username,
		u.*,
		ISNULL(d.Name, '') [District],
		ISNULL(cr.Name, '') [Crew]
	FROM dbo.[User] u WITH (UPDLOCK)
		INNER JOIN dbo.UserCredential c ON u.UserId = c.UserId
		LEFT OUTER JOIN dbo.District d ON d.DistrictId = u.DistrictId
		LEFT OUTER JOIN dbo.Crew cr ON cr.CrewId = u.CrewId
		INNER JOIN dbo.WorkOrderAdditionalUser w ON w.WorkOrderId = @WorkOrderId AND w.UserId = u.UserId
	
END