﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 5/6/2014
-- Description:	Edit hub name, time stamp and user id for update.
-- =============================================
CREATE PROCEDURE [dbo].[UpdateHub] 
	-- Add the parameters for the stored procedure here
	@HubId int,
	@Name varchar(100),
	@UpdatedByUserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Hub
	SET 
		Name = @Name,
		LastUpdatedByUserId = @UpdatedByUserId,
		LastUpdatedAtUtc = GETDATE()
	WHERE 
		HubId = @HubId
END