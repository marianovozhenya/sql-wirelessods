﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/9/2014
-- Description:	Selects equipment barcodes for a list of equipment ids
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableEquipmentBarcodesByEquipmentIdList]
	@IdList			AS IdList	READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[EquipmentBarcode]
	WHERE EquipmentId IN (SELECT Id FROM @IdList)
END