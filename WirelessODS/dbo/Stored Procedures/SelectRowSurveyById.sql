﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 8/31/2014
-- Description:	Selects a survey row by id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowSurveyById]
	@SurveyId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [dbo].[Survey]
	WHERE [SurveyId] = @SurveyId
END