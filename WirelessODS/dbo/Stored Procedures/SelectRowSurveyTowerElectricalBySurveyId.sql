﻿-- =============================================
-- Author: Nate Andrus
-- Create date: 8/28/2014
-- Description:	Selects a SurveyTowerElectrical row by SurveyId
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowSurveyTowerElectricalBySurveyId] 
	@SurveyId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT *
	FROM SurveyTowerElectrical
	WHERE SurveyId = @SurveyId
END