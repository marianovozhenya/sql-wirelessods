﻿-- Stored Procedure

-- =============================================
-- Author:		Kirk Johnson
-- Create date: 6/13/2014
-- Description:	Selects a list of work orders by criteria
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableWorkOrdersByCriteria]
	@StatusId		INT,
	@TypeId			INT,
	@SubTypeId		INT,
	@PriorityId		INT,
	@StartDate		DATE,
	@EndDate		DATE
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
	    wo.*
	  ,st.name as WorkOrderSubType
	  ,d.[Name] as DispatchCode
	  --,s.LocationId
	  ,s.Name as SiteName
	  ,a.AddressLine as AddressLine
	  ,ba.City
	  ,ba.State
	  ,ba.Zip,sy.SurveyId
  FROM [dbo].[WorkOrder] wo
  left Join Site s on wo.SiteId = s.SiteId
  left join Survey sy on wo.WorkOrderId = sy.WorkOrderId
  Inner Join [Address] a on wo.AddressId = a.AddressId
  Inner Join BaseAddress ba on a.BaseAddressId = ba.BaseAddressId
  Inner Join dbo.[WorkOrderSubType] st ON wo.WorkOrderSubTypeId = st.WorkOrderSubTypeId
  Inner Join [dbo].[DispatchCode] d on wo.[DispatchCodeId] = d.[DispatchCodeId]
	WHERE
		(@StatusId IS NULL OR wo.EnumWorkOrderStatusId = @StatusId) AND
		(@TypeId IS NULL OR wo.EnumWorkOrderTypeId = @TypeId) AND
		(@SubTypeId IS NULL OR wo.WorkOrderSubTypeId = @SubTypeId) AND
		(@PriorityId IS NULL OR wo.EnumWorkOrderPriorityId = @PriorityId) AND
		((@StartDate IS NULL AND @EndDate IS NULL) OR (wo.CreatedAtUtc BETWEEN @StartDate AND @EndDate) )
		ORDER BY wo.WorkOrderId DESC

END