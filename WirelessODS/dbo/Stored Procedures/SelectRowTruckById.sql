﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 5/20/2014
-- Description:	This Proceedure will get a Truck by ID
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowTruckById] 
	-- Add the parameters for the stored procedure here
	@TruckId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT [TruckId]
      ,[TruckNumber]
      ,truck.TruckMakeId AS [TruckMakeId]
      ,truck.TruckModelId AS [TruckModelId]
      ,[Year]
      ,[RegistrationDate]
      ,[RegistrationState]
      ,[LicensePlate]
      ,[Gvw]
      ,truck.CreatedAtUtc AS [CreatedAtUtc]
      ,truck.CreatedByUserId AS [CreatedByUserId]
      ,[LastUpdatedAtUtc]
      ,[LastUpdatedByUserId]
	  ,truckMake.Name AS [TruckMakeName]
	  ,truckModel.Name AS [TruckModelName]
  FROM [dbo].[Truck] truck INNER JOIN [dbo].[TruckMake] truckMake ON truckMake.[TruckMakeId] = truck.[TruckMakeId]
					 INNER JOIN [dbo].[TruckModel] truckModel ON truckModel.[TruckModelId] = truck.[TruckModelId]
  where TruckId = @TruckId

END