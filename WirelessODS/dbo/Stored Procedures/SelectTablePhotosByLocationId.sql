﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/20/2014
-- Description:	Selects the photos for a location ordered by the newest first.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTablePhotosByLocationId]
	@LocationId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Photo]
	WHERE LocationId = @LocationId AND IsSoftDeleted = 0
	ORDER BY CreatedAtUtc DESC
END