﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 6/3/2014
-- Description:	This proceedure will return the id of the location by Name
-- =============================================
CREATE PROCEDURE [dbo].[LocationExists] 
	-- Add the parameters for the stored procedure here
	@Name varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [LocationId] from [dbo].[Location] 
	Where  [Name] = @Name
END