﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/29/2014
-- Description:	Selects all the upload statuses
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableEnumUploadStatusAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM [dbo].[EnumUploadStatus]
END