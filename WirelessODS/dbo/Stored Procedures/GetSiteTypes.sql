﻿-- =============================================
-- Author:		Troy Sampson
-- Create date: 04/17/2014
-- Description:	Retrieves the Site Types from the Database
-- =============================================
CREATE PROCEDURE [dbo].[GetSiteTypes] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.EnumSiteType
END