﻿-- =============================================
-- Author: Nate Andrus
-- Create date: 8/28/2014
-- Description:	Updates the SurveyTowerElectrical table
-- =============================================
CREATE PROCEDURE [dbo].[UpdateSurveyTowerElectrical] 
	@SurveyTowerElectricalId int,
	@CanSharePower bit,
	@IsMeterBank bit,
	@IsSpaceToAddMeter bit,
	@IsExistingMeterNumber bit,
	@LengthFromMeterToNewCabinet decimal(7,2),
	@ServiceProvider varchar(100),
	@ServiceContact varchar(100),
	@IsPermitRequiredForSite bit,
	@EstimatedCost smallmoney,
	@EnumMainPowerFeedId int,
	@EnumEnclosureId int,
	@Notes varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE SurveyTowerElectrical		
    SET CanSharePower = @CanSharePower,
    	IsMeterBank = @IsMeterBank,
    	IsSpaceToAddMeter = @IsSpaceToAddMeter,
    	IsExistingMeterNumber = @IsExistingMeterNumber,
    	LengthFromMeterToNewCabinet = @LengthFromMeterToNewCabinet,
    	ServiceProvider = @ServiceProvider,
		ServiceContact = @ServiceContact,
		IsPermitRequiredForSite = @IsPermitRequiredForSite,
		EstimatedCost = @EstimatedCost,
		EnumMainPowerFeedId = @EnumMainPowerFeedId,
		EnumEnclosureId = @EnumEnclosureId,
		Notes = @Notes
    WHERE SurveyTowerElectricalId = @SurveyTowerElectricalId
END