﻿-- =============================================
-- Author:		John Rubin
-- Create date: 05/20/2014
-- Description:	Retrieves all Trucks by TruckNumber
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableTruckByTruckNumber] 
	-- Add the parameters for the stored procedure here
	@TruckNumber varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT truck.*, 
		   truckMake.Name AS [TruckMakeName], 
		   truckModel.Name AS [TruckModelName] 
	FROM Truck truck INNER JOIN TruckMake truckMake ON truckMake.TruckMakeId = Truck.TruckMakeId
			   INNER JOIN TruckModel truckModel ON truckModel.TruckModelId = Truck.TruckModelId
	WHERE TruckNumber LIKE '%' + @TruckNumber + '%'
END