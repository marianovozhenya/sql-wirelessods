﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/17/2014
-- Description:	Inserts a row in the ServiceXProduct table.
-- =============================================
CREATE PROCEDURE [dbo].[InsertServiceXProduct]
	@ServiceId		INT,
	@ProductId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[ServiceXProduct]
			   ([ServiceId]
			   ,[ProductId])
		 VALUES
			   (@ServiceId
			   ,@ProductId)

	SELECT SCOPE_IDENTITY()
END