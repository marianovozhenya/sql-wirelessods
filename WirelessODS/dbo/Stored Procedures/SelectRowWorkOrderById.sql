﻿-- Stored Procedure

-- =============================================
-- Author:		Kirk Johnson
-- Create date: 6/12/2014
-- Description:	Selects a row from the work orders table to populate a work order object based on id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowWorkOrderById] 
	-- Add the parameters for the stored procedure here
	@WorkOrderId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	--
	SELECT 
	   wo.*
	  ,st.name as WorkOrderSubType
	  ,d.[Name] as DispatchCode
	  --,s.LocationId
	  ,s.Name as SiteName
	  ,a.AddressLine as AddressLine
	  ,ba.City
	  ,ba.State
	  ,ba.Zip
	  ,sy.SurveyId
  FROM [dbo].[WorkOrder] wo
  left Join Site s on wo.SiteId = s.SiteId
  left join Survey sy on wo.WorkOrderId = sy.WorkOrderId
  Inner Join [Address] a on wo.AddressId = a.AddressId
  Inner Join BaseAddress ba on a.BaseAddressId = ba.BaseAddressId
  Inner Join dbo.[WorkOrderSubType] st ON wo.WorkOrderSubTypeId = st.WorkOrderSubTypeId
  Inner Join [dbo].[DispatchCode] d on wo.[DispatchCodeId] = d.[DispatchCodeId]
  where wo.workorderid = @WorkOrderId

END