﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/8/2014
-- Description:	Checks to see if an equipment type name already exists in the system
-- =============================================
CREATE PROCEDURE [dbo].[EquipmentTypeExists]
	@Id		INT,
	@Name	VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (@Id > 0)
	BEGIN
		SELECT EquipmentTypeId
		FROM [dbo].[EquipmentType]
		WHERE EquipmentTypeId <> @Id AND Name = @Name
	END

	IF (@Id <= 0)
	BEGIN
		SELECT EquipmentTypeId
		FROM [dbo].[EquipmentType]
		WHERE Name = @Name
	END
END