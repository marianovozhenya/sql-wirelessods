﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/20/2014
-- Description:	Updates the attachment upload status
-- =============================================
CREATE PROCEDURE [dbo].[UpdateAttachmentUploadStatus]
	@AttachmentId			INT,
	@EnumUploadStatusId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE [dbo].[Attachment]
	SET
		EnumUploadStatusId = @EnumUploadStatusId
	WHERE AttachmentId = @AttachmentId
END