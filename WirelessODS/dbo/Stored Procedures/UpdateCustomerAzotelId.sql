﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 7/16/2014
-- Description:	This proceedure will update the Azotel id
-- =============================================
CREATE PROCEDURE [dbo].[UpdateCustomerAzotelId] 
	-- Add the parameters for the stored procedure here
	@CustomerId	INT = 0, 
	@AzotelId	INT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Customer]
	SET
		AzotelId = @AzotelId,
		CreatedInAzotelAtUtc = GETUTCDATE()
	WHERE  @CustomerId = CustomerId
END