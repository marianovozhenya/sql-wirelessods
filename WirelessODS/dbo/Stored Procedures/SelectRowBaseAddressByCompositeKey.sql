﻿
-- =============================================
-- Author:		Troy Sampson
-- Create date: 7/17/2014
-- Description:	Checks to see if a base address already exists in the system
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowBaseAddressByCompositeKey]
	@CompositeKey		VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		SELECT BaseAddressId
		FROM [dbo].[BaseAddress]
		WHERE BaseCompositeKey = @CompositeKey
END