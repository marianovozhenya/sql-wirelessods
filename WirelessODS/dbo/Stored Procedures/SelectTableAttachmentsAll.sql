﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 8/19/2014
-- Description:	Selects all the attachments.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableAttachmentsAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Attachment]
	WHERE IsSoftDeleted = 0
	ORDER BY CreatedAtUtc DESC
END