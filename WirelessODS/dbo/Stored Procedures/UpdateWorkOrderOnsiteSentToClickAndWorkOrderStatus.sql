﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 8/1/2014
-- Description:	Updates the WorkOrder status as OnSite and flags message has been sent to Click. 
-- =============================================
CREATE PROCEDURE [dbo].[UpdateWorkOrderOnsiteSentToClickAndWorkOrderStatus] 
	@WorkOrderId					INT,
	@WorkOrderStatusId				INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [dbo].[WorkOrder]
	SET
		[EnumWorkOrderStatusId] = @WorkOrderStatusId,
		[OnsiteSentToClickAtUtc] = GETDATE()
	WHERE WorkOrderId = @WorkOrderId
END