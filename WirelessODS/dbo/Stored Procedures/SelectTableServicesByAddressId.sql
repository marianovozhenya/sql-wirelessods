﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/16/2014
-- Description:	Selects service rows by address id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableServicesByAddressId]
	@AddressId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Service]
	WHERE AddressId = @AddressId
END