﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 7/17/2014
-- Description:	This proceedure will return a list of users that have the same name and number
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableContactExists] 
	-- Add the parameters for the stored procedure here
	@LastName Varchar(50), 
	@PhoneNumber bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select * from contact
	where LastName = @LastName 
	And PhoneNumber =  @PhoneNumber
END