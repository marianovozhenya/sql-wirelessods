﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 9/4/2014
-- Description:	Inserts a row into SiteAttribute
-- =============================================
CREATE PROCEDURE [dbo].[InsertSiteAttribute] 
	@SiteId INT,
	@Latitude DECIMAL(9, 6),
	@Longitude DECIMAL(9, 6),
	@Owner VARCHAR(100),
	@OwnerPhone VARCHAR(20),
	@CreatedAtUtc DATETIME,
	@CreatedByUserId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO [dbo].[SiteAttribute] (
		[SiteId],
		[Latitude],
		[Longitude],
		[Owner],
		[OwnerPhone],
		[CreatedAtUtc],
		[CreatedByUserId])
	VALUES (
		@SiteId,
		@Latitude,
		@Longitude,
		@Owner,
		@OwnerPhone,
		@CreatedAtUtc,
		@CreatedByUserId)

	SELECT SCOPE_IDENTITY()
END