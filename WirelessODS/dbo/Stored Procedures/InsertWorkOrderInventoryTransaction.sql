﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 7/1/2014
-- Description:	This Proceedure will insert a new WorkOrderInventoryTransaction
-- =============================================
CREATE PROCEDURE [dbo].[InsertWorkOrderInventoryTransaction] 
	-- Add the parameters for the stored procedure here
			@WorkOrderId int
           ,@EquipmentId int
           ,@Quantity int
           ,@CreatedByUserId int
           ,@RDARRB Varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
INSERT INTO [dbo].[WorkOrderInventoryTransaction]
           ([WorkOrderId]
           ,[EquipmentId]
           ,[Quantity]
           ,[PerformedAtUtc]
           ,[PerformedByUserId]
           ,[RDARRB])
     VALUES
           (@WorkOrderId
           ,@EquipmentId
           ,@Quantity
           ,GetUtcDate()
           ,@CreatedByUserId
           ,@RDARRB)

END