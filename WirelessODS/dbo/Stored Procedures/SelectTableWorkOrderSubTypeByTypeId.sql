﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 6/13/2014
-- Description:	this proceedure will return a single subType by id
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableWorkOrderSubTypeByTypeId] 
	-- Add the parameters for the stored procedure here
	@WorkOrderTypeId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--
	
	SELECT 
		[WorkOrderSubTypeId]
		,[Name]
		,[EnumWorkOrderTypeId] as WorkOrderTypeId
	FROM [dbo].[WorkOrderSubType]
	where [EnumWorkOrderTypeId] = @WorkOrderTypeId
END