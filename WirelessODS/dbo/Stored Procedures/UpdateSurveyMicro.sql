﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 9/4/2014
-- Description:	Updates a row in the SurveyMicro table
-- =============================================
CREATE PROCEDURE [dbo].[UpdateSurveyMicro] 
	@SurveyMicroId INT,
	@TechName VARCHAR(100),
	@ReferenceId VARCHAR(50),
	@DownStreamFrequency INT,
	@UpStreamFrequency INT,
	@AntennaType SMALLINT,
	@Polarity VARCHAR(5),
	@Azimuth DECIMAL(4, 1),
	@MagneticAzimuth DECIMAL(4, 1),
	@TiltElevation DECIMAL(3, 1),
	@TXPower DECIMAL(4, 1),
	@TargetRSSI DECIMAL(4, 1),
	@TargetCNR DECIMAL(4, 1),
	@Latitude DECIMAL(9, 6),
	@Longitude DECIMAL(9, 6),
	@Distance DECIMAL(4, 1),
	@Declination DECIMAL(4, 1),
	@PositionOnTower VARCHAR(50),
	@AP1Latitude DECIMAL(9, 6),
	@AP1Longitude DECIMAL(9, 6),
	@AP2Latitude DECIMAL(9, 6),
	@AP2Longitude DECIMAL(9, 6),
	@RTLatitude DECIMAL(9, 6),
	@RTLongitude DECIMAL(9, 6),
	@IsLineOfSiteToRADHeight BIT,
	@Notes VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE SurveyMicro
	SET TechName = @TechName,
		ReferenceId = @ReferenceId,
		DownStreamFrequency = @DownStreamFrequency,
		UpStreamFrequency = @UpStreamFrequency,
		AntennaType = @AntennaType,
		Polarity = @Polarity,
		Azimuth = @Azimuth,
		MagneticAzimuth = @MagneticAzimuth,
		TiltElevation = @TiltElevation,
		TXPower = @TXPower,
		TargetRSSI = @TargetRSSI,
		TargetCNR = @TargetCNR,
		Latitude = @Latitude,
		Longitude = @Longitude,
		Distance = @Distance,
		Declination = @Declination,
		PositionOnTower = @PositionOnTower,
		AP1Latitude = @AP1Latitude,
		AP1Longitude = @AP1Longitude,
		AP2Latitude = @AP2Latitude,
		AP2Longitude = @AP2Longitude,
		RTLatitude = @RTLatitude,
		RTLongitude = @RTLongitude,
		IsLineOfSiteToRADHeight = @IsLineOfSiteToRADHeight,
		Notes = @Notes
	WHERE SurveyMicroId = @SurveyMicroId
END