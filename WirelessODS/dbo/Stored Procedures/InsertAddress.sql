﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 7/14/2014
-- Description:	This stored proceedure will insert a row into the Address table
-- =============================================
CREATE PROCEDURE [dbo].[InsertAddress] 
	-- Add the parameters for the stored procedure here
	@BaseAddressId		INT, 
	@SuiteType			VARCHAR(20),
	@SuiteNumber		VARCHAR(20),
	@BoxType			VARCHAR(20),
	@BoxNumber			VARCHAR(20),
	@AddressFull		VARCHAR(MAX),
	@AddressLine		VARCHAR(MAX),
	@EnumAddressValidationServiceId	INT,
	@ValidationDate		DATE,
	@ValidationKey		VARCHAR(100),
	@CompositeKey		VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Address]
           ([BaseAddressId]
           ,[SuiteType]
           ,[SuiteNumber]
           ,[BoxType]
		   ,[BoxNumber]
		   ,[AddressFull]
		   ,[AddressLine]
		   ,[EnumAddressValidationServiceId]
		   ,[ValidationDate]
		   ,[ValidationKey]
		   ,[CompositeKey])
     VALUES
           (@BaseAddressId
           ,@SuiteType
           ,@SuiteNumber
           ,@BoxType
		   ,@BoxNumber
		   ,@AddressFull
		   ,@AddressLine
		   ,@EnumAddressValidationServiceId
		   ,@ValidationDate
		   ,@ValidationKey
		   ,@CompositeKey)

		   SELECT SCOPE_IDENTITY()
END