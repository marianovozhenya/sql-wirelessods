﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/30/2014
-- Description:	Updates rows in the ServiceXProduct table.
-- =============================================
CREATE PROCEDURE [dbo].[UpdateServiceXProduct]
	@ServiceId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[ServiceXProduct]
	   SET [CreatedInAzotelAtUtc] = GETUTCDATE()
	 WHERE [ServiceId] = @ServiceId
END