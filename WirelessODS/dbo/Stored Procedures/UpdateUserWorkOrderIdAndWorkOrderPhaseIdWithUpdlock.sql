﻿
-- =============================================
-- Author:		Andrew Larsson
-- Create date: 8/1/2014
-- Description:	Updates the User's Work Order Phase stateful properties. 
-- =============================================
CREATE PROCEDURE [dbo].[UpdateUserWorkOrderIdAndWorkOrderPhaseIdWithUpdlock] 
	@UserId					INT,
	@WorkOrderId			INT,
	@WorkOrderPhaseId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [dbo].[User]
	SET
		CurrentWorkOrderId = @WorkOrderId,
		CurrentWorkOrderPhaseId = @WorkOrderPhaseId,
		EnteredPhaseAtUtc = GETDATE()
	WHERE UserId = @UserId
END