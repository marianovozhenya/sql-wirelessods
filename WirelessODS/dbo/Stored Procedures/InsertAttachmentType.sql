﻿
-- =============================================
-- Author:		Bryan Call
-- Create date: 4/23/2014
-- Description:	Inserts a row into the AttachmentType table and returns the newly inserted id.
-- =============================================
CREATE PROCEDURE [dbo].[InsertAttachmentType] 
	@Name				VARCHAR(100),
	@CreatedByUserId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.AttachmentType
	        ( Name ,
	          CreatedAtUtc ,
	          CreatedByUserId
	        )
	VALUES  ( @Name , -- Name - varchar(100)
	          GETUTCDATE() , -- CreatedAtUtc - datetime
	          @CreatedByUserId
	        )

	SELECT SCOPE_IDENTITY()
END