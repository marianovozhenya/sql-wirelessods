﻿-- =============================================
-- Author:		Bryan Call
-- Create date: 4/9/2014
-- Description:	Inserts a row into the user table.
-- =============================================
CREATE PROCEDURE [dbo].[InsertUser] 
	@FirstName			VARCHAR(50),
	@LastName			VARCHAR(50),
	@PhoneNumber		VARCHAR(20),
	@CellNumber			VARCHAR(20),
	@EmailAddress		VARCHAR(50),
	@IsEnabled			BIT,
	@ManagerId			INT,
	@CreatedByUserId	INT,
	@DistrictId			INT,
	@CrewId				INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[User]
           ([FirstName]
           ,[LastName]
           ,[PhoneNumber]
           ,[CellNumber]
           ,[EmailAddress]
           ,[LastFailedLoginAttemptAtUtc]
           ,[FailedLoginAttemptCounter]
           ,[LastLoginAtUtc]
           ,[IsEnabled]
           ,[ManagerId]
           ,[CreatedByUserId]
           ,[CreatedAtUtc]
           ,[LastUpdatedByUserId]
           ,[LastUpdatedAtUtc]
		   ,[DistrictId]
		   ,[CrewId])
     VALUES
           (@FirstName
           ,@LastName
           ,@PhoneNumber
           ,@CellNumber
           ,@EmailAddress
           ,NULL
           ,0
           ,NULL
           ,@IsEnabled
           ,@ManagerId
           ,@CreatedByUserId
           ,GETUTCDATE()
           ,NULL
           ,NULL
		   ,@DistrictId
		   ,@CrewId)

	SELECT SCOPE_IDENTITY()
END