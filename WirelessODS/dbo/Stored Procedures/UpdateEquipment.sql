﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/12/2014
-- Description:	Updates an equipment row
-- =============================================
CREATE PROCEDURE [dbo].[UpdateEquipment]
	@EquipmentId			INT,
	@Name					VARCHAR(100),
	@ManufacturerId			INT,
	@EquipmentTypeId		INT,
	@VendorId				INT,
	@Sku					VARCHAR(50),
	@Model					VARCHAR(50),
	@IsActive				BIT,
	@IsDevice				Bit,
	@LastUpdatedByUserId	INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE [dbo].[Equipment]
	   SET [Name] = @Name
		  ,[VendorId] = @VendorId
		  ,[ManufacturerId] = @ManufacturerId
		  ,[Sku] = @Sku
		  ,[Model] = @Model
		  ,[EquipmentTypeId] = @EquipmentTypeId
		  ,[IsActive] = @IsActive
		  ,[IsDevice] = @IsDevice
		  ,[LastUpdatedAtUtc] = GETUTCDATE()
		  ,[LastUpdatedByUserId] = @LastUpdatedByUserId
	 WHERE [EquipmentId] = @EquipmentId
END