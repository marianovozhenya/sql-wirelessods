﻿-- Stored Procedure

-- =============================================
-- Author:		Jon Zelenik
-- Create date: 7/14/2014
-- Description:	Pulls Customer data by first and last name
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableCustomersByName] 
	@FirstName varchar(100),
	@LastName varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT
		cu.CustomerId,
		co.FirstName,
		co.LastName,
		co.EmailAddress [Email],
		co.PhoneNumber [Phone],
		instaddr.AddressFull [InstallationAddress],
		ISNULL(billaddr.AddressFull, '') [BillingAddress]
	FROM Customer cu
		JOIN ContactXCustomer cxc ON cxc.CustomerId = cu.CustomerId
		JOIN Contact co ON co.ContactId = cxc.ContactId
		LEFT OUTER JOIN [Address] billaddr ON billaddr.AddressId = cu.BillingAddressId
		LEFT OUTER JOIN [Service] s ON s.CustomerId = cu.CustomerId
		LEFT OUTER JOIN [Address] instaddr ON instaddr.AddressId = s.AddressId 
	WHERE ((@FirstName <> '' AND co.FirstName LIKE '%' + @FirstName + '%')
			OR (@LastName <> '' AND co.LastName LIKE '%' + @LastName + '%'))
		AND cxc.EnumCustomerContactTypeId = 1
	ORDER BY
		CASE
			WHEN co.FirstName = @FirstName
					AND co.LastName = @LastName
				THEN 0
			WHEN (co.FirstName = @FirstName
						AND co.LastName LIKE '%' + @LastName + '%')
				OR (co.LastName = @LastName
						AND co.FirstName LIKE '%' + @FirstName + '%')
				THEN 1
			WHEN co.FirstName LIKE '%' + @FirstName + '%'
					AND co.LastName LIKE '%' + @LastName + '%'
				THEN 2
			WHEN co.LastName = @LastName
				THEN 3
			WHEN co.LastName LIKE '%' + @LastName + '%'
				THEN 4
			ELSE 5
		END
END