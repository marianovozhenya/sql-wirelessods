﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 05/14/2014
-- Description:	Retrieves the Device Use Types from the Database
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableEnumDeviceUseTypeAll] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM dbo.EnumDeviceUseType
END