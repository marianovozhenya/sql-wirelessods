﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 8/28/2014
-- Description:	Inserts a row into SurveyTowerLandlord
-- =============================================
CREATE PROCEDURE [dbo].[InsertSurveyTowerLandlord] 
	-- Add the parameters for the stored procedure here
	@SurveyId int,
	@GroundLandlordName varchar(100),
	@GroundLandlordContact varchar(100),
	@StructureLandlordName varchar(100),
	@StructureLandlordContact varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO SurveyTowerLandlord (
		SurveyId,
		GroundLandlordName,
		GroundLandlordContact,
		StructureLandlordName,
		StructureLandlordContact)
	VALUES (
		@SurveyId,
		@GroundLandlordName,
		@GroundLandlordContact,
		@StructureLandlordName,
		@StructureLandlordContact)

	SELECT SCOPE_IDENTITY()
END