﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 05/09/2014
-- Description:	Checks to see if a manufacturer exists for given id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowManufacturerById] 
	-- Add the parameters for the stored procedure here
	@ManufacturerId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [dbo].[Manufacturer] 
	WHERE ManufacturerId = @ManufacturerId
END