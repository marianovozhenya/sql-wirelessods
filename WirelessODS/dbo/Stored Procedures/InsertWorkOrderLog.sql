﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 06/13/2014
-- Description:	This proceedure will insert a row into the WorkOrderLog table
-- =============================================
CREATE PROCEDURE [dbo].[InsertWorkOrderLog] 
	-- Add the parameters for the stored procedure here
			@WorkOrderId int
           ,@CreatedByUserId int
           ,@LogType int
           ,@Comment varchar(max)
		   ,@WorkOrderNumber varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[WorkOrderLog]
           ([WorkOrderId]
           ,[EnteredAtUtc]
           ,[EnteredByUserId]
           ,[EnumWorkOrderLogEntryTypeId]
           ,[Comments]
		   ,[WorkOrderNumber])
     VALUES
           (@WorkOrderId
           ,GETUTCDATE()
           ,@CreatedByUserId
           ,@LogType
           ,@Comment
		   ,@WorkOrderNumber)


END