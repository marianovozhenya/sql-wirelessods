﻿-- =============================================
-- Author:		John Rubin
-- Create date: 5/5/2014
-- Description:	Retrieves all LocationTypes
-- =============================================
CREATE PROCEDURE [dbo].[GetLocationTypes] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM EnumLocationType
END