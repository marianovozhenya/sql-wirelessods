﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/16/2914
-- Description:	Selects service rows by customer id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableServicesByCustomerId]
	@CustomerId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Service]
	WHERE CustomerId = @CustomerId
END