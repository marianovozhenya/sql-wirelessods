﻿-- =============================================
-- Author:		John Rubin
-- Create date: 06/17/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[UpdateWorkOrderResultCode] 
	-- Add the parameters for the stored procedure here
	@WorkOrderId int = 0,
	@WorkOrderResultCodeId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.WorkOrder
	SET WorkOrderResultCodeId = @WorkOrderResultCodeId, IsClosed = 1
	WHERE WorkOrderId = @WorkOrderId
END