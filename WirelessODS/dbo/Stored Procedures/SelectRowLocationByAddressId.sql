﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/18/2014
-- Description:	Selects a location by address id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowLocationByAddressId]
	@AddressId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Location]
	WHERE AddressId = @AddressId
END