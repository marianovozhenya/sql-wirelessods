﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 8/29/2014
-- Description:	Inserts a row into SurveyTowerCompound
-- =============================================
CREATE PROCEDURE [dbo].[InsertSurveyTowerCompound] 
	-- Add the parameters for the stored procedure here
	@SurveyId INT,
	@EnumEquipmentPlacementId INT,
	@EquipmentOther VARCHAR(50),
	@CompoundLength DECIMAL(7,2),
	@CompoundWidth DECIMAL(7,2),
	@IsSpaceInCompound BIT,
	@IsAdaquateGrounding BIT,
	@IsSpaceInShelter BIT,
	@IsProximityIssue BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO [dbo].[SurveyTowerCompound] (
		[SurveyId],
		[EnumEquipmentPlacementId],
		[EquipmentOther],
		[CompoundLength],
		[CompoundWidth],
		[IsSpaceInCompound],
		[IsAdaquateGrounding],
		[IsSpaceInShelter],
		[IsProximityIssue])
	VALUES (
		@SurveyId,
		@EnumEquipmentPlacementId,
		@EquipmentOther,
		@CompoundLength,
		@CompoundWidth,
		@IsSpaceInCompound,
		@IsAdaquateGrounding,
		@IsSpaceInShelter,
		@IsProximityIssue)

	SELECT SCOPE_IDENTITY()
END