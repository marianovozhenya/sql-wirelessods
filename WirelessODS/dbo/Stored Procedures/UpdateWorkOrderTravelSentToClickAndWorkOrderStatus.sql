﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 8/1/2014
-- Description:	Updates the WorkOrder status as Travel and flags message has been sent to Click. 
-- =============================================
CREATE PROCEDURE [dbo].[UpdateWorkOrderTravelSentToClickAndWorkOrderStatus] 
	@WorkOrderId					INT,
	@WorkOrderStatusId				INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [dbo].[WorkOrder]
	SET
		[EnumWorkOrderStatusId] = @WorkOrderStatusId,
		[TravelSentToClickAtUtc] = GETDATE()
	WHERE WorkOrderId = @WorkOrderId
END