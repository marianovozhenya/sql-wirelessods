﻿-- Stored Procedure

-- Stored Procedure

-- =============================================
-- Author:		Andrew Larsson
-- Create date: 4/17/2014
-- Description:	Inserts a row into the location table.
-- =============================================
CREATE PROCEDURE [dbo].[InsertLocation]
	@Name							VARCHAR(100),
	@EnumLocationTypeId				INT,
	@Latitude						FLOAT,
	@Longitude						FLOAT,
	@EnumMelissaGeoCodeQualityId	INT,
	@GeoCodePrecision				DECIMAL(6,3),
	@AddressId						INT,
	@CreatedByUserId				INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Location]
           ([Name]
           ,[EnumLocationTypeId]
           ,[Latitude]
           ,[Longitude]
		   ,[EnumMelissaGeoCodeQualityId]
		   ,[GeoCodePrecision]
		   ,[AddressId]
           ,[CreatedAtUtc]
           ,[CreatedByUserId])
     VALUES
           (@Name
           ,@EnumLocationTypeId
           ,@Latitude
           ,@Longitude
		   ,@EnumMelissaGeoCodeQualityId
		   ,@GeoCodePrecision
		   ,@AddressId
           ,GETUTCDATE()
           ,@CreatedByUserId)

	SELECT SCOPE_IDENTITY()
END