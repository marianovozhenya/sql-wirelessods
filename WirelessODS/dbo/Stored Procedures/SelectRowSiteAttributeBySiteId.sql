﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 9/4/2014
-- Description:	Selects a row from SiteAttribute by SiteId
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowSiteAttributeBySiteId] 
	@SiteId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT *
	FROM SiteAttribute
	WHERE SiteId = @SiteId
END