﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 5/19/2014
-- Description:	Checks to see if a photo type name already exists in the system
-- =============================================
CREATE PROCEDURE [dbo].[PhotoTypeExists]
	@Id		INT,
	@Name	VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (@Id > 0)
	BEGIN
		SELECT COUNT(PhotoTypeId)
		FROM [dbo].[PhotoType]
		WHERE PhotoTypeId <> @Id AND Name = @Name
	END

	IF (@Id <= 0)
	BEGIN
		SELECT COUNT(PhotoTypeId)
		FROM [dbo].[PhotoType]
		WHERE Name = @Name
	END
END