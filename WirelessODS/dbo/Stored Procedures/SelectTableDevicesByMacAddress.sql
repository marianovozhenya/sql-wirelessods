﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/15/2014
-- Description:	Selects a list of devices by mac address
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableDevicesByMacAddress]
	@MacAddress		VARCHAR(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		d.*,
		i.MacAddress,
		i.IPv4Address,
		w.WorkOrderNumber
	FROM [dbo].[Device] d
	INNER JOIN [dbo].[WorkOrder] w
		ON d.WorkOrderId = w.WorkOrderId
	INNER JOIN [dbo].[Interface] i
		ON d.DeviceId = i.DeviceId AND i.EnumInterfaceTypeId = 1
	WHERE CONVERT(VARCHAR(12), i.MacAddress, 2) LIKE @MacAddress
END