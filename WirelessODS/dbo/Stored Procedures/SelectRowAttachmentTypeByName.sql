﻿-- =============================================
-- Author:		John Rubin
-- Create date: 4/28/2014
-- Description:	Retrieves an AttachmentType by Name
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowAttachmentTypeByName] 
	-- Add the parameters for the stored procedure here
	@Name VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 1
		*
	FROM dbo.[AttachmentType]
	WHERE
		Name = @Name
END