﻿

-- =============================================
-- Author:		Troy Sampson
-- Create date: 8/1/2014
-- Description:	Gets all Result Code Rules
-- =============================================
CREATE PROCEDURE [dbo].[SelectWorkOrderResultCodeRulesByRuleId]
	@ResultCodeRuleId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT rcXrule.WorkOrderResultCodeXRuleId AS ResultCodeXRule, worc.WorkOrderResultCodeId AS ResultCodeId ,worc.Name AS ResultCodeName, rcRule.ResultCodeRuleId AS RuleId, rcRule.Name AS RuleName, rcRuleType.EnumResultCodeRuleTypeId AS RuleTypeId,rcRuleType.SystemName AS RuleTypeName 
	FROM WorkOrderResultCode worc
	JOIN WorkOrderResultCodeXRule rcXrule ON rcXrule.WorkOrderResultCodeId = worc.WorkOrderResultCodeId
	JOIN ResultCodeRule rcRule ON rcRule.ResultCodeRuleId = rcXrule.ResultCodeRuleId
	JOIN EnumResultCodeRuleType rcRuleType ON rcRuleType.EnumResultCodeRuleTypeId = rcRule.EnumResultCodeRuleTypeId
	WHERE rcRule.ResultCodeRuleId = @ResultCodeRuleId
END