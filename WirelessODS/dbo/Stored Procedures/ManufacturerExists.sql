﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 5/9/2014
-- Description:	Checks to see if a manufacturer name already exists in the system
-- =============================================
CREATE PROCEDURE [dbo].[ManufacturerExists]
	@ManufacturerId		INT,
	@ManufacturerName		VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (@ManufacturerId > 0)
	BEGIN
		SELECT ManufacturerId
		FROM [dbo].[Manufacturer]
		WHERE ManufacturerId <> ManufacturerId AND Name = @ManufacturerName
	END

	IF (@ManufacturerId <= 0)
	BEGIN
		SELECT ManufacturerId
		FROM [dbo].[Manufacturer]
		WHERE Name = @ManufacturerName
	END
END