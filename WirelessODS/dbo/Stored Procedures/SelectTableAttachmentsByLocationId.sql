﻿-- =============================================
-- Author:		Bryan Call
-- Create date: 4/25/2014
-- Description:	Selects the attachments for a location ordered by the newest first.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableAttachmentsByLocationId] 
	@LocationId					INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM dbo.Attachment
	WHERE LocationId = @LocationId AND IsSoftDeleted = 0
	ORDER BY CreatedAtUtc DESC
END