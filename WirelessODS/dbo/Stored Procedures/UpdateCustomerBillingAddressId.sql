﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 7/16/2014
-- Description:	This proceedure will update the BillingAddressId 
-- =============================================
CREATE PROCEDURE [dbo].[UpdateCustomerBillingAddressId] 
	-- Add the parameters for the stored procedure here
	@CustomerId int = 0, 
	@BillingAddressId  int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Customer
	Set BillingAddressId  = @BillingAddressId
	where  @CustomerId = CustomerId

END