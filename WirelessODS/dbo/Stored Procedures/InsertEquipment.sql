﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/12/2014
-- Description:	Inserts a new equipment
-- =============================================
CREATE PROCEDURE [dbo].[InsertEquipment]
	@Name				VARCHAR(100),
	@ManufacturerId		INT,
	@EquipmentTypeId	INT,
	@VendorId			INT,
	@Sku				VARCHAR(50),
	@Model				VARCHAR(50),
	@IsActive			BIT,
	@IsDevice			Bit,
	@CreatedByUserId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Equipment]
			   ([Name]
			   ,[VendorId]
			   ,[ManufacturerId]
			   ,[Sku]
			   ,[Model]
			   ,[EquipmentTypeId]
			   ,[IsActive]
			   ,[IsDevice]
			   ,[CreatedAtUtc]
			   ,[CreatedByUserId])
		 VALUES
			   (@Name
			   ,@VendorId
			   ,@ManufacturerId
			   ,@Sku
			   ,@Model
			   ,@EquipmentTypeId
			   ,@IsActive
			   ,@IsDevice
			   ,GETUTCDATE()
			   ,@CreatedByUserId)

	SELECT SCOPE_IDENTITY()
END