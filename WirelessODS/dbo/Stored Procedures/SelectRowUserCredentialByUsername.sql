﻿-- =============================================
-- Author:		Bryan Call
-- Create date: 4/1/2014
-- Description:	Selects a row from the UserCredential table by username.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowUserCredentialByUsername] 
	@Username						VARCHAR(50),
	@EnumCredentialTypeId			INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT
		*
	FROM dbo.UserCredential
	WHERE Username = @Username AND EnumCredentialTypeId = @EnumCredentialTypeId
END