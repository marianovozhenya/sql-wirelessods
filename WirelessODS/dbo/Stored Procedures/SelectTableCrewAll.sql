﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 7/31/2014
-- Description:	Selects all rows from the Crew table
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableCrewAll] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT *
	FROM Crew
END