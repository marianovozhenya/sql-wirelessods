﻿
-- =============================================
-- Author:		Kirk Johnson
-- Create date: 4/15/2014
-- Description:	Selects all site rows
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableSitesAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		s.*,
		a.AddressFull LocationAddressFull,
		r.SurveyId CurrentSurveyId
	FROM [dbo].[Site] s
	JOIN [dbo].[Location] l
		ON l.LocationId = s.LocationId
	JOIN [dbo].[Address] a
		ON a.AddressId = l.AddressId
	LEFT JOIN [dbo].[Survey] r
		ON r.SiteId = s.SiteId AND r.IsCurrent = 1
	ORDER BY s.Name
END