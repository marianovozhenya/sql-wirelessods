﻿-- Stored Procedure

-- =============================================
-- Author:		Kirk Johnson
-- Create date: 8/1/2014
-- Description:	Selects TravelSentToClickAtUtc for the WorkOrderId passed in.
-- =============================================
CREATE PROCEDURE [dbo].[SelectHasTravelBeenSentToClickForWorkOrderId]
	@WorkOrderId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		TravelSentToClickAtUtc
	FROM [dbo].[WorkOrder]
	WHERE WorkOrderId = @WorkOrderId
END