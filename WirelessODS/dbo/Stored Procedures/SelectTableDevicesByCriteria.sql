﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/15/2014
-- Description:	Selects a list of devices by criteria
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableDevicesByCriteria]
	@Name			VARCHAR(256),
	@IPv4Address	VARCHAR(20),
	@MacAddress		VARCHAR(20),
	@SiteId			INT,
	@WorkOrderId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		d.*,
		i.MacAddress,
		i.IPv4Address,
		w.WorkOrderNumber
	FROM [dbo].[Device] d
	INNER JOIN [dbo].[WorkOrder] w
		ON d.WorkOrderId = w.WorkOrderId
	INNER JOIN [dbo].[Interface] i
		ON d.DeviceId = i.DeviceId AND i.EnumInterfaceTypeId = 1
	WHERE
		(@Name IS NULL OR d.Name LIKE @Name) AND
		(@IPv4Address IS NULL OR CONVERT(VARCHAR(8), i.IPv4Address, 2) LIKE @IPv4Address) AND
		(@MacAddress IS NULL OR CONVERT(VARCHAR(12), i.MacAddress, 2) LIKE @MacAddress) AND
		(@SiteId IS NULL OR d.SiteId = @SiteId) AND
		(@WorkOrderId IS NULL OR d.WorkOrderId = @WorkOrderId)
END