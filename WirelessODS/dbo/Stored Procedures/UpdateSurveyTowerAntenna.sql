﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 9/3/2014
-- Description:	Updates a record in the SurveyTowerAntenna table
-- =============================================
CREATE PROCEDURE [dbo].[UpdateSurveyTowerAntenna] 
	@SurveyTowerAntennaId INT,
	@VivintRAD VARCHAR(50),
	@RADCenterCount TINYINT,
	@EnumRAD1MountTypeId INT,
	@RAD1BracketType VARCHAR(50),
	@RAD1EstAntennaRADCenterFt DECIMAL(7, 2),
	@RAD1EstMicrowaveRADCenterFt DECIMAL(7, 2),
	@EnumRAD2MountTypeId INT,
	@RAD2BracketType VARCHAR(50),
	@RAD2EstAntennaRADCenterFt DECIMAL(7, 2),
	@RAD2EstMicrowaveRADCenterFt DECIMAL(7, 2),
	@EnumRAD3MountTypeId INT,
	@RAD3BracketType VARCHAR(50),
	@RAD3EstAntennaRADCenterFt DECIMAL(7, 2),
	@RAD3EstMicrowaveRADCenterFt DECIMAL(7, 2),
	@EnumRAD4MountTypeId INT,
	@RAD4BracketType VARCHAR(50),
	@RAD4EstAntennaRADCenterFt DECIMAL(7, 2),
	@RAD4EstMicrowaveRADCenterFt DECIMAL(7, 2),
	@IsUnderIceShield BIT,
	@IsInstallNewSupports BIT,
	@Qty INT,
	@TypeOfSupports VARCHAR(100),
	@Notes VARCHAR(MAX),
	@EnumAccessOfCableId INT,
	@EnumWhereToRunCableId INT,
	@EnumClimbingOfMonopoleId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE SurveyTowerAntenna
	SET VivintRAD = @VivintRAD,
		RADCenterCount = @RADCenterCount,
		EnumRAD1MountTypeId = @EnumRAD1MountTypeId,
		RAD1BracketType = @RAD1BracketType,
		RAD1EstAntennaRADCenterFt = @RAD1EstAntennaRADCenterFt,
		RAD1EstMicrowaveRADCenterFt = @RAD1EstMicrowaveRADCenterFt,
		EnumRAD2MountTypeId = @EnumRAD2MountTypeId,
		RAD2BracketType = @RAD2BracketType,
		RAD2EstAntennaRADCenterFt = @RAD2EstAntennaRADCenterFt,
		RAD2EstMicrowaveRADCenterFt = @RAD2EstMicrowaveRADCenterFt,
		EnumRAD3MountTypeId = @EnumRAD3MountTypeId,
		RAD3BracketType = @RAD3BracketType,
		RAD3EstAntennaRADCenterFt = @RAD3EstAntennaRADCenterFt,
		RAD3EstMicrowaveRADCenterFt = @RAD3EstMicrowaveRADCenterFt,
		EnumRAD4MountTypeId = @EnumRAD4MountTypeId,
		RAD4BracketType = @RAD4BracketType,
		RAD4EstAntennaRADCenterFt = @RAD4EstAntennaRADCenterFt,
		RAD4EstMicrowaveRADCenterFt = @RAD4EstMicrowaveRADCenterFt,
		IsUnderIceShield = @IsUnderIceShield,
		IsInstallNewSupports = @IsInstallNewSupports,
		Qty = @Qty,
		TypeOfSupports = @TypeOfSupports,
		Notes = @Notes,
		EnumAccessOfCableId = @EnumAccessOfCableId,
		EnumWhereToRunCableId = @EnumWhereToRunCableId,
		EnumClimbingOfMonopoleId = @EnumClimbingOfMonopoleId
	WHERE SurveyTowerAntennaId = @SurveyTowerAntennaId
END