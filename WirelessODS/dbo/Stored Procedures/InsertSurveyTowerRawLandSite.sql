﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 9/3/2014
-- Description:	Inserts a row into SurveyTowerRawLandSite
-- =============================================
CREATE PROCEDURE [dbo].[InsertSurveyTowerRawLandSite] 
	-- Add the parameters for the stored procedure here
	@SurveyId INT,
	@SquareFootageLeased INT,
	@ProposedStructureHeight DECIMAL(7,2),
	@IsPowerAvailable BIT,
	@PowerAvailableFt DECIMAL(7,2),
	@EnumLandZonedId INT,
	@IsFloodConcern BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO [dbo].[SurveyTowerRawLandSite] (
		[SurveyId],
		[SquareFootageLeased],
		[ProposedStructureHeight],
		[IsPowerAvailable],
		[PowerAvailableFt],
		[EnumLandZonedId],
		[IsFloodConcern])
	VALUES (
		@SurveyId,
		@SquareFootageLeased,
		@ProposedStructureHeight,
		@IsPowerAvailable,
		@PowerAvailableFt,
		@EnumLandZonedId,
		@IsFloodConcern)

	SELECT SCOPE_IDENTITY()
END