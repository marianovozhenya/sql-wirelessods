﻿-- =============================================
-- Author:  John Rubin
-- Create date: 07/17/2014
-- Description: Retrieves a district by zipcode.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowDistrictByZipCode] 
 -- Add the parameters for the stored procedure here
 @ZipCode varchar(5)
AS
BEGIN
 -- SET NOCOUNT ON added to prevent extra result sets from
 -- interfering with SELECT statements.
 SET NOCOUNT ON;

    -- Insert statements for procedure here
 SELECT d.* 
 FROM District d join ZipCodeXDistrict z on d.DistrictId = z.DistrictId
 WHERE z.ZipCode = @ZipCode 

END