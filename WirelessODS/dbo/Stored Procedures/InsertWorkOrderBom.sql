﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 06/29/2014
-- Description:	This proceedure will insert a row into the WorkOrderBom table
-- =============================================
CREATE PROCEDURE [dbo].[InsertWorkOrderBom] 
	-- Add the parameters for the stored procedure here
	@WorkOrderId int, 
	@EquipmentId int,
	@Quantity int,
	@IsInventoryShip bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[WorkOrderBom]
           ([WorkOrderId]
           ,[EquipmentId]
           ,[Quantity]
           ,[Pulled]
           ,[IsInventoryShip]
		   ,[DeviceCount])
     VALUES
           (@WorkOrderId
           ,@EquipmentId
           ,@Quantity
           ,0
           ,@IsInventoryShip
		   ,0)
END