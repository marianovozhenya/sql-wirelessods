﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 4/10/2014
-- Description:	Stored Proceedure for inserting User Credentials
-- =============================================
CREATE PROCEDURE [dbo].[InsertUserCredential] 
	 @UserID int,
     @UserName varchar(50),
     @CredentialTypeID int,
     @CredentialValue varchar(max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

	INSERT INTO [dbo].[UserCredential]
           ([UserId], [Username], [EnumCredentialTypeId], [CredentialValue])
     VALUES
			(@UserID, @UserName, @CredentialTypeID, @CredentialValue)

END