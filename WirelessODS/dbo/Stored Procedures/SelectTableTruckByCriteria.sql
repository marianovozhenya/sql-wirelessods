﻿-- =============================================
-- Author:		John Rubin
-- Create date: 05/20/2014
-- Description:	Retrieves Trucks by a set of criteria
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableTruckByCriteria] 
	-- Add the parameters for the stored procedure here
	@TruckNumber varchar(100),
	@UserName varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT t.* ,
		   truckMake.Name AS [TruckMakeName],
		   truckModel.Name AS [TruckModelName]
	FROM Truck t INNER JOIN [dbo].[User] u ON t.TruckId = u.TruckId  
				 INNER JOIN [dbo].[UserCredential] uc ON uc.UserId = u.UserId
				 INNER JOIN dbo.TruckMake truckMake ON truckMake.TruckMakeId = t.TruckMakeId
				 INNER JOIN dbo.TruckModel truckModel ON truckModel.TruckModelId = t.TruckModelId
	WHERE (u.FirstName + ' ' + u.LastName) LIKE '%' + @UserName + '%'  AND t.TruckNumber LIKE '%' + @TruckNumber + '%'
	OR uc.Username LIKE '%' + @UserName + '%' AND TruckNumber LIKE '%' + @TruckNumber + '%'
END