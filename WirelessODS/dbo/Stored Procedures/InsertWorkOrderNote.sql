﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 6/17/2014
-- Description:	This proceedure will insert a row into the work ordernotes table
-- =============================================
CREATE PROCEDURE [dbo].[InsertWorkOrderNote] 
	-- Add the parameters for the stored procedure here
	@WorkOrderId int,  
	@CreatedByUserId int, 
	@Note varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Insert Into [dbo].[WorkOrderNote] ([WorkOrderId], [CreatedByUserId],[CreatedAtUtc], [Note])
	values (@WorkOrderId, @CreatedByUserId, GETUTCDATE(), @Note)

	select SCOPE_IDENTITY();

END