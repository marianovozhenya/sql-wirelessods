﻿-- =============================================
-- Author:		John Rubin
-- Create date: 05/07/2014
-- Description:	Checks a vendor's existence.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowVendorByName]
	-- Add the parameters for the stored procedure here
	@Name varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 1 *
	FROM [dbo].[Vendor] 
	WHERE Name = @Name
END