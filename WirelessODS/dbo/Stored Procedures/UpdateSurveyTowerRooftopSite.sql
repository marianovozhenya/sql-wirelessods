﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 9/2/2014
-- Description:	Updates a row in SurveyTowerRooftopSite.
-- =============================================
CREATE PROCEDURE [dbo].[UpdateSurveyTowerRooftopSite]
	@SurveyTowerRooftopSiteId	INT,
	@Description				VARCHAR(100),
	@BuildingName				VARCHAR(50),
	@NumberOfStories			TINYINT,
	@SquareFootageLeased		DECIMAL(7,2),
	@IsAntennaSpaceAvailable	BIT,
	@EnumRooftopMountTypeId		INT,
	@IsElevatorToEquipmentSpace	BIT,
	@IsPowerAvailable			BIT,
	@PowerAvailableFt			DECIMAL(7,2),
	@IsUnrestrictedAccess		BIT,
	@UnrestrictedAccessNote		VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[SurveyTowerRooftopSite]
		SET	[Description] = @Description
			,[BuildingName] = @BuildingName
			,[NumberOfStories] = @NumberOfStories
			,[SquareFootageLeased] = @SquareFootageLeased
			,[IsAntennaSpaceAvailable] = @IsAntennaSpaceAvailable
			,[EnumRooftopMountTypeId] = @EnumRooftopMountTypeId
			,[IsElevatorToEquipmentSpace] = @IsElevatorToEquipmentSpace
			,[IsPowerAvailable] = @IsPowerAvailable
			,[PowerAvailableFt] = @PowerAvailableFt
			,[IsUnrestrictedAccess] = @IsUnrestrictedAccess
			,[UnrestrictedAccessNote] = @UnrestrictedAccessNote
	WHERE [SurveyTowerRooftopSiteId] = @SurveyTowerRooftopSiteId
END