﻿
-- =============================================
-- Author:		Andrew Larsson
-- Create date: 9/2/2014
-- Description:	Updates the SurveyTowerSiteAccess table
-- =============================================
CREATE PROCEDURE [dbo].[UpdateSurveyTowerSiteAccess] 
	@SurveyTowerSiteAccessId INT,
	@IsLandlordNotificationRequired BIT,
	@IsMultipleGates BIT,
	@IsLivestockRoaming BIT,
	@EnumRoadTypeId INT,
	@EnumRoadConditionId INT,
	@AccessRoadOther VARCHAR(100),
	@AdditionalAccessInformation VARCHAR(MAX),
	@GateCombo VARCHAR(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [dbo].[SurveyTowerSiteAccess]
	SET [IsLandlordNotificationRequired] = @IsLandlordNotificationRequired,
		[IsMultipleGates] = @IsMultipleGates,
		[IsLivestockRoaming] = @IsLivestockRoaming,
		[EnumRoadTypeId] = @EnumRoadTypeId,
		[EnumRoadConditionId] = @EnumRoadConditionId,
		[AccessRoadOther] = @AccessRoadOther,
		[AdditionalAccessInformation] = @AdditionalAccessInformation,
		[GateCombo] = @GateCombo
	WHERE [SurveyTowerSiteAccessId] = @SurveyTowerSiteAccessId
END