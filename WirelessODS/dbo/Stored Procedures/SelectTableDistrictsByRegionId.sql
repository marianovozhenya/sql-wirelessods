﻿-- =============================================
-- Author:		John Rubin
-- Create date: 07/11/14
-- Description:	Retrieves all districts by region id
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableDistrictsByRegionId]
	-- Add the parameters for the stored procedure here
	@RegionId varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT d.*
	FROM District d
	WHERE RegionId = @RegionId
END