﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 8/3/2014
-- Description:	Selects a row with customer and service columns by service id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowCustomerServiceByServiceId]
	@ServiceId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		s.ServiceId,
		s.CustomerId,
		s.AddressId,
		s.DeviceId,
		cu.AzotelId,
		co.FirstName,
		co.LastName,
		co.PhoneNumber,
		a.AddressLine,
		ba.City,
		ba.[State]
	FROM [dbo].[Service] s
	JOIN [dbo].[ContactXCustomer] x ON x.CustomerId = s.CustomerId AND x.EnumCustomerContactTypeId = 1
	JOIN [dbo].[Customer] cu ON cu.CustomerId = x.CustomerId
	JOIN [dbo].[Contact] co ON co.ContactId = x.ContactId
	JOIN [dbo].[Address] a ON a.AddressId = s.AddressId
	JOIN [dbo].[BaseAddress] ba ON ba.BaseAddressId = a.BaseAddressId
	WHERE s.ServiceId = @ServiceId
END