﻿-- =============================================
-- Author:		John Rubin
-- Create date: 07/16/2014
-- Description:	Inserts a WpdaLog entry
-- =============================================
CREATE PROCEDURE [dbo].[InsertWpdaLog] 
	-- Add the parameters for the stored procedure here
	@UserId int,
	@RequestContext varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO WpdaLog(UserId, RequestContext, StartDateTimeLocal, WasSuccessful)
	VALUES(@UserId, @RequestContext, GETDATE(), 0)

	SELECT SCOPE_IDENTITY()
END