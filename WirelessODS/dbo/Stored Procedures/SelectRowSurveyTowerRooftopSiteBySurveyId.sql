﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 9/2/2014
-- Description:	Selects a row from SurveyTowerRooftopSite by survey id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowSurveyTowerRooftopSiteBySurveyId]
	@SurveyId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [dbo].[SurveyTowerRooftopSite]
	WHERE [SurveyId] = @SurveyId
END