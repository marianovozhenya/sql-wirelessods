﻿-- =============================================
-- Author:		Troy Sampson
-- Create date: 6/27/2014
-- Description:	Selects Site Equipment Current
-- =============================================
CREATE PROCEDURE [dbo].[SelectSiteEquipmentCurrent]
	@SiteId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		b.SiteId,
		c.EquipmentId,	
		c.Name,
		SUM(Quantity) AS Quantity
	from WorkOrderInventoryTransaction a
	INNER JOIN WorkOrder b ON (a.WorkOrderId = b.WorkOrderId)
	INNER JOIN Equipment c ON (a.EquipmentId = c.EquipmentId)
	WHERE b.SiteId = @SiteId
	GROUP BY b.SiteId, c.EquipmentId, c.Name
END