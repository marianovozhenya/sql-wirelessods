﻿

-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/10/2014
-- Description:	Selects a list of device history by work order id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableDeviceHistoryByWorkOrderId]
	@WorkOrderId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		d.*,
		w.WorkOrderNumber
	FROM [dbo].[DeviceHistory] d
	INNER JOIN [dbo].[WorkOrder] w
		ON d.WorkOrderId = w.WorkOrderId
	WHERE d.WorkOrderId = @WorkOrderId
END