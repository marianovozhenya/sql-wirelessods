﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 7/16/2014
-- Description:	This proceedure will insert a row into the ContactXCustomer table
-- =============================================
CREATE PROCEDURE [dbo].[InsertContactXCustomer] 
	-- Add the parameters for the stored procedure here
	@ContactId int,
    @CustomerId int,
    @EnumCustomerContactTypeId int,
    @Role varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[ContactXCustomer]
           ([ContactId]
           ,[CustomerId]
           ,[EnumCustomerContactTypeId]
           ,[Role])
     VALUES
           (@ContactId
           ,@CustomerId
           ,@EnumCustomerContactTypeId
           ,@Role)

END