﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/9/2014
-- Description:	Selects equipment barcodes by equipment id
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableEquipmentBarcodesByEquipmentId]
	@EquipmentId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[EquipmentBarcode]
	WHERE EquipmentId = @EquipmentId
END