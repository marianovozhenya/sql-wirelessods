﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/22/2014
-- Description:	Inserts a new payment profile row.
-- =============================================
CREATE PROCEDURE [dbo].[InsertPaymentProfile]
	@AuthorizeNetProfileId			VARCHAR (50),
	@AuthorizeNetPaymentProfileId	VARCHAR (50),
	@CustomerId						INT,
	@CardLastFour					INT,
	@CardCode						INT,
	@CardHolder						VARCHAR(100),
	@CardType						VARCHAR(30),
	@ExpirationMonth				TINYINT,
	@ExpirationYear					INT,
	@CreatedByUserId				INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[PaymentProfile]
			   ([AuthorizeNetProfileId]
			   ,[AuthorizeNetPaymentProfileId]
			   ,[CustomerId]
			   ,[CardLastFour]
			   ,[ExpirationMonth]
			   ,[ExpirationYear]
			   ,[CardCode]
			   ,[CreatedAtUtc]
			   ,[CreatedByUserId]
			   ,[CardHolder]
			   ,[CardType])
		 VALUES
			   (@AuthorizeNetProfileId
			   ,@AuthorizeNetPaymentProfileId
			   ,@CustomerId
			   ,@CardLastFour
			   ,@ExpirationMonth
			   ,@ExpirationYear
			   ,@CardCode
			   ,GETUTCDATE()
			   ,@CreatedByUserId
			   ,@CardHolder
			   ,@CardType)

	SELECT SCOPE_IDENTITY()
END