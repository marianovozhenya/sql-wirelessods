﻿
-- =============================================
-- Author:		Andrew Larsson
-- Create date: 7/28/2014
-- Description:	Selects Phases by WorkOrder Id
-- =============================================
CREATE PROCEDURE [dbo].SelectTableWorkOrderPhaseByWorkOrderId
	@WorkOrderId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		WorkOrderPhase.*
	FROM [dbo].[WorkOrderPhase]
		JOIN [dbo].[WorkOrderXPhase] ON WorkOrderPhase.WorkOrderPhaseId = WorkOrderXPhase.WorkOrderPhaseId
	WHERE WorkOrderXPhase.WorkOrderId = @WorkOrderId
END