﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 4/10/2014
-- Description:	This stored proceedure will insert a row into the UserxUserRoles table
-- =============================================
CREATE PROCEDURE [dbo].[InsertUserRole] 
	-- Add the parameters for the stored procedure here
	@UserID int = 0, 
	@RoleID int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	INSERT INTO [dbo].[UserXUserRole]
           ([UserId]
           ,[UserRoleId])
     VALUES
           (@UserID
           ,@RoleID)

END