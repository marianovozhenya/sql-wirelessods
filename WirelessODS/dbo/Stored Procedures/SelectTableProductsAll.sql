﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/14/2014
-- Description:	Selects all the rows from the Product table.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableProductsAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Product]
END