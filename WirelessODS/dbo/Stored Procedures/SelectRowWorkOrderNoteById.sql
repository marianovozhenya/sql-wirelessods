﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 6/17/2014
-- Description:	This proceedure will load a workOrderNote by id
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowWorkOrderNoteById] 
	-- Add the parameters for the stored procedure here
	@WorkOrderNoteId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [WorkOrderNoteId]
      ,[WorkOrderId]
      ,[CreatedByUserId]
      ,[CreatedAtUtc]
      ,[Note]
	FROM [dbo].[WorkOrderNote]
	where workOrderNoteId = @WorkOrderNoteId

END