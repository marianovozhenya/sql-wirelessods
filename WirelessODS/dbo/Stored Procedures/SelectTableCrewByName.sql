﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 8/4/2014
-- Description:	Selects all crew members with a Name matching wild card @Name
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableCrewByName] 
	@Name varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT *
	FROM Crew
	WHERE Name LIKE '%' + @Name + '%'
END