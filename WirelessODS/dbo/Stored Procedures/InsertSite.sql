﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 4/22/2014
-- Description:	This stored proceedure will insert a new row into the Site table
-- =============================================
CREATE PROCEDURE [dbo].[InsertSite] 
	-- Add the parameters for the stored procedure here
		   @LocationId int,
           @Name varchar(100),
           @SiteTypeId int,
           @SiteStatusId int,
           @Place varchar(max),
           @Directions varchar(max),
           @HubId int,
           @CreatedById int
           
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO [dbo].[Site]
           ([LocationId]
           ,[Name]
           ,[EnumSiteTypeId]
           ,[EnumSiteStatusId]
           ,[Place]
           ,[Directions]
           ,[HubId]
		   ,[CreatedAtUtc]
           ,[CreatedByUserId])
     VALUES
           (@LocationId
           ,@Name
           ,@SiteTypeId
           ,@SiteStatusId
           ,@Place
           ,@Directions
           ,@HubId
           ,GETUTCDATE()
           ,@CreatedById)

		   SELECT SCOPE_IDENTITY()

END