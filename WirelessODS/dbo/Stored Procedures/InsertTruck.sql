﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 05/20/2014
-- Description:	This Proceedure will insert a row into Trucks
-- =============================================
CREATE PROCEDURE [dbo].[InsertTruck] 
	-- Add the parameters for the stored procedure here
		@TruckNum Varchar(50)
        ,@TruckMakeId int
        ,@TruckModelId int
        ,@Year int
        ,@RegistrationDate datetime 
        ,@RegistrationState varchar(2)
        ,@LicensePlate varchar(15) 
        ,@Gvw int 
        ,@CreatedByUserId int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Truck]
           ([TruckNumber]
           ,[TruckMakeId]
           ,[TruckModelId]
           ,[Year]
           ,[RegistrationDate]
           ,[RegistrationState]
           ,[LicensePlate]
           ,[Gvw]
           ,[CreatedByUserId]
		   ,[CreatedAtUtc])
           
     VALUES
           (@TruckNum
           ,@TruckMakeId
           ,@TruckModelId
           ,@Year
           ,@RegistrationDate
           ,@RegistrationState
           ,@LicensePlate
           ,@Gvw
           ,@CreatedByUserId
		   , GETUTCDATE())

Select SCOPE_IDENTITY()

END