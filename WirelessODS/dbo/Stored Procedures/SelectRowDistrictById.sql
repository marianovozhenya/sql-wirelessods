﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 7/31/2014
-- Description:	Selects a District record by DistrictId
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowDistrictById] 
	@DistrictId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT *
	FROM District
	WHERE DistrictId = @DistrictId
END