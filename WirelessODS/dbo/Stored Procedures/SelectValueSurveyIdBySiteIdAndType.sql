﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 9/4/2014
-- Description:	Selects a survey id by site id and survey type.
-- =============================================
CREATE PROCEDURE [dbo].[SelectValueSurveyIdBySiteIdAndType]
	@SiteId				INT,
	@EnumSurveyTypeId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [SurveyId]
	FROM [dbo].[Survey]
	WHERE [SiteId] = @SiteId AND [EnumSurveyTypeId] = @EnumSurveyTypeId AND [IsCurrent] = 1
END