﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 07/15/2014
-- Description:	This proceedure will insert a row into the contact table
-- =============================================
CREATE PROCEDURE [dbo].[InsertContact] 
	-- Add the parameters for the stored procedure here
		@FirstName			VARCHAR(50),
        @LastName			VARCHAR(50),
        @EnumPhoneTypeId	INT,
        @PhoneNumber		BIGINT,
        @EmailAddress		VARCHAR(255),
		@CreatedByUserId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Contact]
           ([FirstName]
           ,[LastName]
           ,[EnumPhoneTypeId]
           ,[PhoneNumber]
           ,[EmailAddress]
		   ,[CreatedAtUtc]
		   ,[CreatedByUserId])
     VALUES
           (@FirstName
           ,@LastName
           ,@EnumPhoneTypeId
           ,@PhoneNumber
           ,@EmailAddress
		   ,GETUTCDATE()
		   ,@CreatedByUserId)

	SELECT SCOPE_IDENTITY()

END