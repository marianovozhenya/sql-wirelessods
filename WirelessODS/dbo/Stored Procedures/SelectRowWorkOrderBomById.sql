﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 6/29/2014
-- Description:	This proceedure will select a row from the work order bom table
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowWorkOrderBomById] 
	-- Add the parameters for the stored procedure here
	@WorkOrderBomId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		b.*,
		e.Name [EquipmentName],
		e.IsDevice [EquipmentIsDevice]
	FROM [dbo].[WorkOrderBom] b
		JOIN [dbo].Equipment e ON e.EquipmentId = b.EquipmentId
	where b.WorkOrderBomId = @WorkOrderBomId

END