﻿-- =============================================
-- Author:		John Rubin
-- Create date: 4/22/2014
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[UpdateLocation]
	@LocationId				INT,
	@Name					VARCHAR(100),
	@EnumLocationTypeId		INT,
	@LastUpdatedByUserId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Location]
	SET
		Name = @Name,
		EnumLocationTypeId = @EnumLocationTypeId,
		LastUpdatedByUserId = @LastUpdatedByUserId,
		LastUpdatedAtUtc = GETUTCDATE()
	WHERE LocationId = @LocationId
END