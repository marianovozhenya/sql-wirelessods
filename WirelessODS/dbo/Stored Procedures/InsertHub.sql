﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 5/5/2014
-- Description:	This stored proceedure will insert a row into the hub table
-- =============================================
CREATE PROCEDURE [dbo].[InsertHub] 
	-- Add the parameters for the stored procedure here
	@SiteId int, 
	@Name varchar(100),
	@CreatedByUserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Hub]
           ([Name]
           ,[SiteId]
           ,[CreatedAtUtc]
           ,[CreatedByUserId])
     VALUES
           (@Name
           ,@SiteId
           ,GetUTCDate()
           ,@CreatedByUserId)

		   SELECT SCOPE_IDENTITY()
END