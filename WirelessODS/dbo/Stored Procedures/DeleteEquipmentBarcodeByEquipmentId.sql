﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/12/2014
-- Description:	Deletes an equipment barcode by equipment id
-- =============================================
CREATE PROCEDURE [dbo].[DeleteEquipmentBarcodeByEquipmentId]
	@EquipmentId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DELETE
	FROM [dbo].[EquipmentBarcode]
	WHERE EquipmentId = @EquipmentId
END