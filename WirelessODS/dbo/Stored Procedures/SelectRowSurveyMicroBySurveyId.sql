﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 9/4/2014
-- Description:	Selects a row from SurveyMicro by SurveId
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowSurveyMicroBySurveyId] 
	@SurveyId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT *
	FROM SurveyMicro
	WHERE SurveyId = @SurveyId
END