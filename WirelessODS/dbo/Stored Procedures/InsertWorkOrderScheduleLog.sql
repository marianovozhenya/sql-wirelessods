﻿

-- =============================================
-- Author:		Andrew Larsson
-- Create date: 8/1/2014
-- Description:	Inserts a Work Order Schedule Log entry
-- =============================================
CREATE PROCEDURE [dbo].[InsertWorkOrderScheduleLog] 
	-- Add the parameters for the stored procedure here
	@WorkOrderId int,
	@AppointmentWindowStart datetime,  
	@AppointmentWindowEnd datetime, 
	@ChangedByUserId int,
	@StatusChange varchar(50),
	@Reason varchar(100),
	@AssignedUserId int,
	@AssignedCrewId int,
	@AcknowledgedByClickAtUtc datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO
		[dbo].[WorkOrderScheduleLog] (
			WorkOrderId,
			DateEnteredUtc,
			AppointmentWindowStart,
			AppointmentWindowEnd,
			ChangedByUserId,
			StatusChange,
			Reason,
			AssignedUserId,
			AssignedCrewId,
			AcknowledgedByClickAtUtc
		) VALUES (
			@WorkOrderId,
			GETUTCDATE(),
			@AppointmentWindowStart,
			@AppointmentWindowEnd,
			@ChangedByUserId,
			@StatusChange,
			@Reason,
			@AssignedUserId,
			@AssignedCrewId,
			@AcknowledgedByClickAtUtc
		);

	SELECT SCOPE_IDENTITY();

END