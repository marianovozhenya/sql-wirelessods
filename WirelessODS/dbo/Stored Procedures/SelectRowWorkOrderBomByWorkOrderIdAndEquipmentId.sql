﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 7/1/2014
-- Description:	this proceedure will get the workorderbom by workorderId and EquipmentId
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowWorkOrderBomByWorkOrderIdAndEquipmentId] 
	-- Add the parameters for the stored procedure here
	@WorkOrderId int = 0, 
	@EquipmentId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*,
		eq.Name AS EquipmentName,
		eq.IsDevice AS EquipmentIsDevice
	from WorkOrderBom woBom INNER JOIN Equipment eq ON eq.EquipmentId = woBom.EquipmentId
	where WorkOrderId = @WorkOrderId
	And eq.EquipmentId =  @EquipmentId
END