﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 6/26/2014
-- Description:	This proceedure will get a list of all open workorderbom records
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableWorkOrderBomBySiteId] 
	-- Add the parameters for the stored procedure here
	@SiteId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
       SUM(Quantity) AS Quantity,
       SUM(Pulled) AS Pulled,
       c.Name
	FROM dbo.WorkOrderBom a
	INNER JOIN WorkOrder b ON (a.WorkOrderId = b.WorkOrderId)
	INNER JOIN Equipment c ON (a.EquipmentId = c.EquipmentId)
	WHERE b.SiteId = @SiteId AND b.IsClosed = 0
	GROUP BY c.Name

END