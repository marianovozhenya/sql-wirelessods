﻿-- =============================================
-- Author:		Kirk Johnson	
-- Create date: 5/9/2014
-- Description:	Selects manufacturer by name
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableManufacturerByName]
	@Name		VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Manufacturer]
	WHERE Name LIKE @Name
	ORDER BY Name
END