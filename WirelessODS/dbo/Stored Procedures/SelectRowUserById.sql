﻿
-- =============================================
-- Author:		Bryan Call
-- Create date: 4/2/2014
-- Description:	Selects a row from the user table along with related information to populate a user object.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowUserById] 
	-- Add the parameters for the stored procedure here
	@UserId				INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		a.*,
		b.Username,
		ISNULL(d.Name, '') [District],
		ISNULL(cr.Name, '') [Crew]
	FROM dbo.[User] a
		INNER JOIN dbo.UserCredential b ON (a.UserId = b.UserId)
		LEFT OUTER JOIN dbo.District d ON (d.DistrictId = a.DistrictId)
		LEFT OUTER JOIN dbo.Crew cr ON (cr.CrewId = a.CrewId)
	WHERE a.UserId = @UserId
END