﻿
-- =============================================
-- Author:		Andrew Larsson
-- Create date: 9/2/2014
-- Description:	Selects a Survery Tower Site Access Information section row by SurveyId.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowSurveyTowerSiteAccessBySurveyId]
	@SurveyId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[SurveyTowerSiteAccess]
	WHERE [SurveyId] = @SurveyId
END