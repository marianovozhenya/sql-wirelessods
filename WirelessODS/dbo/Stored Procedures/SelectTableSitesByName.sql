﻿-- =============================================
-- Author:		<Kirk Johnson>
-- Create date: <4/15/14>
-- Description:	<Select sites by based on name>
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableSitesByName]
	-- Add the parameters for the stored procedure here
	@Name	VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		s.*,
		a.AddressFull LocationAddressFull,
		r.SurveyId CurrentSurveyId
	FROM [dbo].[Site] s
	JOIN [dbo].[Location] l
		ON l.LocationId = s.LocationId
	JOIN [dbo].[Address] a
		ON a.AddressId = l.AddressId
	LEFT JOIN [dbo].[Survey] r
		ON r.SiteId = s.SiteId AND r.IsCurrent = 1
	WHERE s.Name LIKE @Name 
	ORDER BY s.Name
END