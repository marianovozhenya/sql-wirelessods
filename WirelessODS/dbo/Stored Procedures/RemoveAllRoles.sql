﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 4/14/2014
-- Description:	This Stored Proceedure will remove all roles from the selected User
-- =============================================
CREATE PROCEDURE [dbo].[RemoveAllRoles] 
	-- Add the parameters for the stored procedure here
	@UserId int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM [dbo].[UserXUserRole]
      WHERE UserId = @UserId
    
END