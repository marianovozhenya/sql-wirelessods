﻿-- =============================================
-- Author:		John Rubin
-- Create date: 06/17/2014
-- Description:	Gets a result code by id
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowWorkOrderResultCodeById] 
	-- Add the parameters for the stored procedure here
	@WorkOrderResultCodeId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * FROM WorkOrderResultCode WHERE WorkOrderResultCodeId = @WorkOrderResultCodeId
END