﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 05/09/2014
-- Description:	Checks if a manufacturer existences for the given name passed in.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowManufacturerByName]
	-- Add the parameters for the stored procedure here
	@Name varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 1 *
	FROM [dbo].[Manufacturer] 
	WHERE Name = @Name
END