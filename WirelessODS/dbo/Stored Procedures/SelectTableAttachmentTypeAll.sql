﻿-- =============================================
-- Author:		Bryan Call
-- Create date: 4/23/2014
-- Description:	Selects all the rows from the AttachmentType table ordered by name.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableAttachmentTypeAll] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM dbo.AttachmentType
	ORDER BY Name
END