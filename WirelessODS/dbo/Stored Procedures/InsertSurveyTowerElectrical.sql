﻿-- =============================================
-- Author: Nate Andrus
-- Create date: 8/28/2014
-- Description:	Inserts a row into SurveyTowerElectrical
-- =============================================
CREATE PROCEDURE [dbo].[InsertSurveyTowerElectrical] 
	-- Add the parameters for the stored procedure here
	@SurveyId int,
	@CanSharePower bit,
	@IsMeterBank bit,
	@IsSpaceToAddMeter bit,
	@IsExistingMeterNumber bit,
	@LengthFromMeterToNewCabinet decimal(7,2),
	@ServiceProvider varchar(100),
	@ServiceContact varchar(100),
	@IsPermitRequiredForSite bit,
	@EstimatedCost smallmoney,
	@EnumMainPowerFeedId int,
	@EnumEnclosureId int,
	@Notes varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO SurveyTowerElectrical (
		SurveyId,
		CanSharePower,
		IsMeterBank,
		IsSpaceToAddMeter,
		IsExistingMeterNumber,
		LengthFromMeterToNewCabinet,
		ServiceProvider,
		ServiceContact,
		IsPermitRequiredForSite,
		EstimatedCost,
		EnumMainPowerFeedId,
		EnumEnclosureId,
		Notes)
	VALUES (
		@SurveyId,
		@CanSharePower,
		@IsMeterBank,
		@IsSpaceToAddMeter,
		@IsExistingMeterNumber,
		@LengthFromMeterToNewCabinet,
		@ServiceProvider,
		@ServiceContact,
		@IsPermitRequiredForSite,
		@EstimatedCost,
		@EnumMainPowerFeedId,
		@EnumEnclosureId,
		@Notes)

	SELECT SCOPE_IDENTITY()
END