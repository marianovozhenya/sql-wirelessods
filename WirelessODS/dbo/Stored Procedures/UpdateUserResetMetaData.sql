﻿
-- =============================================
-- Author:		Kirk Johnson
-- Create date: 5/1/2014
-- Description:	Updates metadata associated to reseting the user's password.
-- =============================================
CREATE PROCEDURE [dbo].[UpdateUserResetMetaData]
	@UserId		INT,
	@LastUpdatedByUserId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[User]
	SET LastFailedLoginAttemptAtUtc = NULL,
		FailedLoginAttemptCounter = 0,
		LastUpdatedByUserId = @LastUpdatedByUserId,
		LastUpdatedAtUtc = GETDATE()
	WHERE UserId = @UserId
END