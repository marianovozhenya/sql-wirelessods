﻿-- Stored Procedure

-- =============================================
-- Author:		Jon Zelenik
-- Create date: 7/14/2014
-- Description:	Returns customer related data based on address id
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableCustomersByAddressId] 
	@AddressId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		cu.CustomerId,
		co.FirstName,
		co.LastName,
		co.EmailAddress [Email],
		co.PhoneNumber [Phone],
		instaddr.AddressFull [InstallationAddress],
		ISNULL(billaddr.AddressFull, '') [BillingAddress]
	FROM Customer cu
		JOIN ContactXCustomer cxc ON cxc.CustomerId = cu.CustomerId
		JOIN Contact co ON co.ContactId = cxc.ContactId
		LEFT OUTER JOIN [Address] billaddr ON billaddr.AddressId = cu.BillingAddressId
		LEFT OUTER JOIN [Service] s ON s.CustomerId = cu.CustomerId
		LEFT OUTER JOIN [Address] instaddr ON instaddr.AddressId = s.AddressId
	WHERE (billaddr.AddressId = @AddressId
			OR instaddr.AddressId = @AddressId)
		AND cxc.EnumCustomerContactTypeId = 1
END