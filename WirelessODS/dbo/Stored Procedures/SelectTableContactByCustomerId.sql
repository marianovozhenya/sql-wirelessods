﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 7/15/2014
-- Description:	This Proceedure will load the list by Customer id
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableContactByCustomerId] 
	-- Add the parameters for the stored procedure here
	@CustomerId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select c.*
	from Contact c
	Inner Join [dbo].[ContactXCustomer] x on c.ContactId = x.contactId
	where x.CustomerId = @CustomerId
END