﻿

-- Stored Procedure

-- =============================================
-- Author:		Andrew Larsson
-- Create date: 8/7/2014
-- Description:	Returns Users that are checked into to a Work Order
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableUsersByCurrentWorkOrderIdWithUpdlock] 
	@CurrentWorkOrderId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		c.Username,
		u.*,
		ISNULL(d.Name, '') [District],
		ISNULL(cr.Name, '') [Crew]
	FROM dbo.[User] u WITH (UPDLOCK)
		INNER JOIN dbo.UserCredential c ON u.UserId = c.UserId
		LEFT OUTER JOIN dbo.District d ON d.DistrictId = u.DistrictId
		LEFT OUTER JOIN dbo.Crew cr ON cr.CrewId = u.CrewId
	WHERE
		u.CurrentWorkOrderId = @CurrentWorkOrderId
	
END