﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 9/4/2014
-- Description:	Updates a row in SiteAttribute
-- =============================================
CREATE PROCEDURE [dbo].[UpdateSiteAttribute] 
	@SiteAttributeId INT,
	@Latitude DECIMAL(9, 6),
	@Longitude DECIMAL(9, 6),
	@Owner VARCHAR(100),
	@OwnerPhone VARCHAR(20),
	@CreatedAtUtc DATETIME,
	@CreatedByUserId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [dbo].[SiteAttribute]
	SET [Latitude] = @Latitude,
		[Longitude] = @Longitude,
		[Owner] = @Owner,
		[OwnerPhone] = @OwnerPhone,
		[CreatedAtUtc] = @CreatedAtUtc,
		[CreatedByUserId] = @CreatedByUserId
	WHERE [SiteAttributeId] = @SiteAttributeId
END