﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 9/5/2014
-- Description:	Inserts a row into SurveyChangeLog
-- =============================================
CREATE PROCEDURE [dbo].[InsertSurveyChangeLog]
	@SiteId				INT,
	@WorkOrderId		INT,
	@Name				VARCHAR(100),
	@Value				VARCHAR(100),
	@CreatedByUserId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[SurveyChangeLog]
		([SiteId]
		,[Name]
		,[Value]
		,[CreatedAtUtc]
		,[CreatedByUserId]
		,[WorkOrderId])
	VALUES
		(@SiteId
		,@Name
		,@Value
		,GETUTCDATE()
		,@CreatedByUserId
		,@WorkOrderId)

	SELECT SCOPE_IDENTITY()
END