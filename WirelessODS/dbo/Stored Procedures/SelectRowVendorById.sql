﻿-- =============================================
-- Author:		John Rubin
-- Create date: 05/07/2014
-- Description:	Checks a vendor's existence.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowVendorById] 
	-- Add the parameters for the stored procedure here
	@VendorId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [dbo].[Vendor] 
	WHERE VendorId = @VendorId
END