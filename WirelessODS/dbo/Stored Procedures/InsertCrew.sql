﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 8/1/2014
-- Description:	Inserts a record into Crew
-- =============================================
CREATE PROCEDURE [dbo].[InsertCrew] 
	@Name varchar(100),
	@DistrictId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO Crew (
		Name,
		DistrictId)
	VALUES (
		@Name,
		@DistrictId)

	SELECT SCOPE_IDENTITY()
END