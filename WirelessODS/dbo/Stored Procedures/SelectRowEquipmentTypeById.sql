﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/8/2014
-- Description:	Selects an equipment type by id
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowEquipmentTypeById]
	@EquipmentTypeId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[EquipmentType]
	WHERE EquipmentTypeId = @EquipmentTypeId
END