﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 9/3/2014
-- Description:	Inserts a row into SurveyDevice
-- =============================================
CREATE PROCEDURE [dbo].[InsertSurveyDevice] 
	@SurveyId INT,
	@DeviceName VARCHAR(100),
	@SSID VARCHAR(50),
	@RadioType VARCHAR(50),
	@IPAddress VARCHAR(50),
	@WirelessMAC VARCHAR(50),
	@ParentIP VARCHAR(50),
	@ParentDevice VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO SurveyDevice (
		SurveyId,
		DeviceName,
		SSID,
		RadioType,
		IPAddress,
		WirelessMAC,
		ParentIP,
		ParentDevice)
	VALUES (
		@SurveyId,
		@DeviceName,
		@SSID,
		@RadioType,
		@IPAddress,
		@WirelessMAC,
		@ParentIP,
		@ParentDevice)

	SELECT SCOPE_IDENTITY()
END