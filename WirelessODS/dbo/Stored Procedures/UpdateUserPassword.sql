﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 4/14/2014
-- Description:	Updates the user's password
-- =============================================
CREATE PROCEDURE [dbo].[UpdateUserPassword]
	@UserId		INT,
	@Password	VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[UserCredential]
	SET
		CredentialValue = @Password
	WHERE UserId = @UserId
END