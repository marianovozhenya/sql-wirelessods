﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 9/2/2014
-- Description:	Inserts a row into SurveyTowerRooftopSite.
-- =============================================
CREATE PROCEDURE [dbo].[InsertSurveyTowerRooftopSite]
	@SurveyId					INT,
	@Description				VARCHAR(100),
	@BuildingName				VARCHAR(50),
	@NumberOfStories			TINYINT,
	@SquareFootageLeased		DECIMAL(7,2),
	@IsAntennaSpaceAvailable	BIT,
	@EnumRooftopMountTypeId		INT,
	@IsElevatorToEquipmentSpace	BIT,
	@IsPowerAvailable			BIT,
	@PowerAvailableFt			DECIMAL(7,2),
	@IsUnrestrictedAccess		BIT,
	@UnrestrictedAccessNote		VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[SurveyTowerRooftopSite]
		([SurveyId]
		,[Description]
		,[BuildingName]
		,[NumberOfStories]
		,[SquareFootageLeased]
		,[IsAntennaSpaceAvailable]
		,[EnumRooftopMountTypeId]
		,[IsElevatorToEquipmentSpace]
		,[IsPowerAvailable]
		,[PowerAvailableFt]
		,[IsUnrestrictedAccess]
		,[UnrestrictedAccessNote])
	VALUES
		(@SurveyId					
		 ,@Description				
		 ,@BuildingName				
		 ,@NumberOfStories			
		 ,@SquareFootageLeased		
		 ,@IsAntennaSpaceAvailable	
		 ,@EnumRooftopMountTypeId		
		 ,@IsElevatorToEquipmentSpace	
		 ,@IsPowerAvailable			
		 ,@PowerAvailableFt			
		 ,@IsUnrestrictedAccess
		 ,@UnrestrictedAccessNote)

	SELECT SCOPE_IDENTITY()
END