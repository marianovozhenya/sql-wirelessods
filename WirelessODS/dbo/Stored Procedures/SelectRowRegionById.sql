﻿-- =============================================
-- Author:		John Rubin
-- Create date: 07/11/14
-- Description:	Retrieves a region by district id
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowRegionById]
	-- Add the parameters for the stored procedure here
	@RegionId varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT r.*
	FROM Region r
	WHERE RegionId = @RegionId
END