﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 7/15/2014
-- Description:	This proceedure will update the selected row
-- =============================================
CREATE PROCEDURE [dbo].[UpdateContact] 
	-- Add the parameters for the stored procedure here
	@ContactId int,
	@FirstName  varchar(50),
	@LastName  varchar(50),
	@EnumPhoneTypeId int,
	@PhoneNumber bigint,
	@EmailAddress  varchar(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Contact]
	   SET [FirstName] = @FirstName
		  ,[LastName] = @LastName
		  ,[EnumPhoneTypeId] = @EnumPhoneTypeId
		  ,[PhoneNumber] = @PhoneNumber
		  ,[EmailAddress] = @EmailAddress
	 WHERE ContactId = @ContactId
	
END