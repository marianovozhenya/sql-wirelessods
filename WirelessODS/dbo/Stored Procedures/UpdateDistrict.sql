﻿-- =============================================
-- Author:		John Rubin
-- Create date: 07/14/2014
-- Description:	Updates a district's region and/or name
-- =============================================
CREATE PROCEDURE [dbo].[UpdateDistrict] 
	-- Add the parameters for the stored procedure here
	@DistrictId int,
	@RegionId int,
	@Name varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE District
	SET RegionId = @RegionId, Name = @Name
	WHERE DistrictId = @DistrictId
END