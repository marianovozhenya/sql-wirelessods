﻿
-- =============================================
-- Author:		Andrew Larsson
-- Create date: 07/17/14
-- Description:	Retrieves a District Zip Code by Zip Code Id
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowZipCodeXDistrictByZipCodeId]
	-- Add the parameters for the stored procedure here
	@ZipCodeXDistrictId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT z.*
	FROM ZipCodeXDistrict z
	WHERE ZipCodeXDistrictId = @ZipCodeXDistrictId
END