﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/8/2014
-- Description:	Inserts a new equipment type
-- =============================================
CREATE PROCEDURE [dbo].[InsertEquipmentType]
	@Name				VARCHAR(100),
	@CreatedByUserId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[EquipmentType]
			   ([Name]
			   ,[CreatedAtUtc]
			   ,[CreatedByUserId])
		 VALUES
			   (@Name
			   ,GETUTCDATE()
			   ,@CreatedByUserId)

	SELECT SCOPE_IDENTITY()
END