﻿
-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/10/2014
-- Description:	Selects a list of device history by criteria
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableDeviceHistoryByCriteria]
	@Name			VARCHAR(256),
	@IPv4Address	VARCHAR(20),
	@MacAddress		VARCHAR(20),
	@SiteId			INT,
	@WorkOrderId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		d.*,
		w.WorkOrderNumber
	FROM [dbo].[DeviceHistory] d
	INNER JOIN [dbo].[WorkOrder] w
		ON d.WorkOrderId = w.WorkOrderId
	WHERE
		(@Name IS NULL OR d.Name LIKE @Name) AND
		(@IPv4Address IS NULL OR CONVERT(VARCHAR(8), d.IPv4Address, 2) LIKE @IPv4Address) AND
		(@MacAddress IS NULL OR CONVERT(VARCHAR(12), d.MacAddress, 2) LIKE @MacAddress) AND
		(@SiteId IS NULL OR d.SiteId = @SiteId) AND
		(@WorkOrderId IS NULL OR d.WorkOrderId = @WorkOrderId)
END