﻿-- =============================================
-- Author:		Bryan Call
-- Create date: 4/2/2014
-- Description:	Inserts a record into the AuditLog table.
-- =============================================
CREATE PROCEDURE [dbo].[InsertAuditLog] 
	-- Add the parameters for the stored procedure here
	@EnumAuditLogEntryTypeId	INT,
	@UserId						INT,
	@IPAddress					VARCHAR(45),
	@AffectedUserId				INT,
	@Details					VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO dbo.AuditLog
	        ( EnteredAtUtc ,
	          EnumAuditLogEntryTypeId ,
	          UserId ,
	          IPAddress ,
	          AffectedUserId ,
	          Details
	        )
	VALUES  ( GETUTCDATE() , -- EnteredAtUtc - datetime
	          @EnumAuditLogEntryTypeId , -- EnumAuditLogEntryTypeId - int
	          @UserId , -- UserId - int
	          @IPAddress , -- IPAddress - varchar(45)
	          @AffectedUserId , -- AffectedUserId - int
	          @Details  -- Details - varchar(max)
	        )
END