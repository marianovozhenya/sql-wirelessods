﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 05/20/2014
-- Description:	This Proceedure will insert a row into Trucks
-- =============================================
Create PROCEDURE [dbo].[InsertTruckMake]
	-- Add the parameters for the stored procedure here
		@Name Varchar(50)
        ,@CreatedByUserId int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[TruckMake]
           ([Name]
           ,[CreatedByUserId]
		   ,[CreatedAtUtc])
           
     VALUES
           (@Name
           ,@CreatedByUserId
		   , GETUTCDATE())

Select SCOPE_IDENTITY()

END