﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/6/2014
-- Description:	Selects a hub by name
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableHubsByName]
	@Name		VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		*
	FROM [dbo].[Hub]
	WHERE Name LIKE @Name
	ORDER BY Name
END