﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 4/24/2014
-- Description:	Selects an AttachmentType row by Id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowAttachmentTypeById]
	@AttachmentTypeId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT
		*
	FROM [dbo].[AttachmentType]
	WHERE AttachmentTypeId = @AttachmentTypeId
END