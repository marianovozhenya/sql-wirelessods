﻿-- Stored Procedure

-- =============================================
-- Author:		Kirk Johnson
-- Create date: 7/14/2014
-- Description:	This stored proceedure will insert a row into the BaseAddress table
-- =============================================
CREATE PROCEDURE [dbo].[InsertBaseAddress] 
	-- Add the parameters for the stored procedure here
	@BaseCompositeKey		VARCHAR(50), 
	@StreetNumber			VARCHAR(100),
	@PreDirection			VARCHAR(5),
	@StreetName				VARCHAR(120),
	@StreetSuffix			VARCHAR(20),
	@PostDirection			VARCHAR(5),
	@City					VARCHAR(50),
	@State					VARCHAR(2),
	@Zip					VARCHAR(10),
	@BaseAddressFull		VARCHAR(MAX),
	@BaseAddressLine		VARCHAR(MAX),
	@EnumAddressValidationServiceId		INT,
	@ValidationDate			DATE,
	@ValidationKey			VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[BaseAddress]
           ([BaseCompositeKey]
           ,[StreetNumber]
           ,[PreDirection]
           ,[StreetName]
		   ,[StreetSuffix]
		   ,[PostDirection]
		   ,[City]
		   ,[State]
		   ,[Zip]
		   ,[BaseAddressFull]
		   ,[BaseAddressLine]
		   ,[EnumAddressValidationServiceId]
		   ,[ValidationDate]
		   ,[ValidationKey])
     VALUES
           (@BaseCompositeKey
           ,@StreetNumber
           ,@PreDirection
           ,@StreetName
		   ,@StreetSuffix
		   ,@PostDirection
		   ,@City
		   ,@State
		   ,@Zip
		   ,@BaseAddressFull
		   ,@BaseAddressLine
		   ,@EnumAddressValidationServiceId
		   ,@ValidationDate
		   ,@ValidationKey)

	SELECT SCOPE_IDENTITY()
END