﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 7/1/2014
-- Description:	This proceedure will insert a new row into WorkOrderLogicalDevice
-- =============================================
CREATE PROCEDURE [dbo].[InsertWorkOrderLogicalDevice] 
	-- Add the parameters for the stored procedure here
		@WorkOrderId int
        ,@EquipmentId int 
        ,@Provisioned bit
        ,@ProvisionedByUserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	INSERT INTO [dbo].[WorkOrderLogicalDevice]
           ([WorkOrderId]
           ,[EquipmentId]
           ,[Provisioned]
           ,[ProvisionedByUserId]
           ,[ProvisionedAtUtc])
     VALUES
           (@WorkOrderId
           ,@EquipmentId
           ,@Provisioned
           ,@ProvisionedByUserId
           ,GetUtcDate())

END