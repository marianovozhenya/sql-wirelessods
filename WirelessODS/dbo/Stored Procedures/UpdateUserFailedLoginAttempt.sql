﻿
-- =============================================
-- Author:		Bryan Call
-- Create date: 4/2/2014
-- Description:	Increments the FailedLoginAttemptCounter and updates LastFailedLoginAttemptAtUtc 
-- =============================================
CREATE PROCEDURE [dbo].[UpdateUserFailedLoginAttempt] 
	-- Add the parameters for the stored procedure here
	@UserId				INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE dbo.[User]
	SET
		FailedLoginAttemptCounter = FailedLoginAttemptCounter + 1,
		LastFailedLoginAttemptAtUtc = GETUTCDATE()
	WHERE UserId = @UserId
END