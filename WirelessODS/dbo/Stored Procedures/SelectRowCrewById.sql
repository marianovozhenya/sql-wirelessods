﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 8/1/2014
-- Description:	Selects a Crew record by CrewId
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowCrewById] 
	@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT *
	FROM Crew
	WHERE CrewId = @Id
END