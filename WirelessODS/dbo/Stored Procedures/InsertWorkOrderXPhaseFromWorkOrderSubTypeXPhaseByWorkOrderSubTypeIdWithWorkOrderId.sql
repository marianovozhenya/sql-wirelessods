﻿
-- =============================================
-- Author:		Andrew Larsson
-- Create date: 7/28/2014
-- Description:	Inserts into WorkOrderXPhase from (WorkOrderSubTypeXPhase by WorkOrderSubTypeId ) with WorkOrderId
-- =============================================
CREATE PROCEDURE [dbo].InsertWorkOrderXPhaseFromWorkOrderSubTypeXPhaseByWorkOrderSubTypeIdWithWorkOrderId
	@WorkOrderSubTypeId		INT,
	@WorkOrderId			INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[WorkOrderXPhase] (WorkOrderId, WorkOrderPhaseId)
	SELECT @WorkOrderId, WorkOrderPhaseId
	FROM WorkOrderSubTypeXPhase
	WHERE WorkOrderSubTypeId = @WorkOrderSubTypeId
END