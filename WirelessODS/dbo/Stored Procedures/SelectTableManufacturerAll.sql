﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 5/9/2014
-- Description:	Selects all manufacturers and ordering by name.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableManufacturerAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Manufacturer]
	ORDER BY Name
END