﻿-- =============================================
-- Author:		<Andrew Larsson>
-- Create date: <4/24/2014>
-- Description:	<Selects users based on the filter criteria (UserRoleId, IsEnabled).>
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableUsersByCriteria] 
	-- Add the parameters for the stored procedure here
	@Name VARCHAR(100),
	@UserRoleId INT,
	@IsEnabled BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		c.Username,
		u.*,
		ISNULL(d.Name, '') [District],
		ISNULL(cr.Name, '') [Crew]
	FROM dbo.[User] u 
		INNER JOIN dbo.UserCredential c ON (u.UserId = c.UserId)
		LEFT OUTER JOIN dbo.District d ON (d.DistrictId = u.DistrictId)
		LEFT OUTER JOIN dbo.Crew cr ON (cr.CrewId = u.CrewId)
	WHERE (@Name IS NULL 
			OR u.FirstName + ' ' + u.LastName LIKE @Name 
			OR c.Username LIKE @Name) 
		AND (@UserRoleId IS NULL 
			OR EXISTS (SELECT x.*
						FROM dbo.UserXUserRole x
						WHERE x.UserId = u.UserId
							AND x.UserRoleId = @UserRoleId)) 
		AND (@IsEnabled IS NULL 
			OR u.IsEnabled = @IsEnabled)
END