﻿-- =============================================
-- Author:		Adan Pineda
-- Create date: 8/29/2014
-- Description:	Updates the SurveyTowerTelco table
-- =============================================
CREATE PROCEDURE [dbo].[UpdateSurveyTowerTelco] 
	@SurveyTowerTelcoId INT,
	@EnumEnclosureId INT,
	@LengthFromTelcoToNewCabinet DECIMAL(7,2),
	@EstimatedCost SMALLMONEY,
	@Notes VARCHAR(MAX),
	@TelcoCompany VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [SurveyTowerTelco]
	SET [LengthFromTelcoToNewCabinet] = @LengthFromTelcoToNewCabinet,
		[EnumEnclosureId] = @EnumEnclosureId,
		[EstimatedCost] = @EstimatedCost,
		[Notes] = @Notes,
		[TelcoCompany] = @TelcoCompany
	WHERE [SurveyTowerTelcoId] = @SurveyTowerTelcoId
END