﻿-- =============================================
-- Author:		JohnRubin
-- Create date: 05/20/2014
-- Description:	Updates a Truck
-- =============================================
CREATE PROCEDURE [dbo].[UpdateTruck] 
	-- Add the parameters for the stored procedure here
	@TruckId int,
	@TruckNum varchar(100),
	@TruckMakeId int,
	@TruckModelId int,
	@Year int,
	@RegistrationDate datetime,
	@RegistrationState varchar(2),
	@LicensePlate varchar(15),
	@Gvw int,
	@LastUpdatedByUserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE Truck
	SET
		TruckNumber = @TruckNum,
		TruckMakeId = @TruckMakeId,
		TruckModelId = @TruckModelId,
		Truck.[Year] = @Year,
		RegistrationDate = @RegistrationDate,
		RegistrationState = @RegistrationState,
		LicensePlate = @LicensePlate,
		Gvw = @Gvw,
		LastUpdatedAtUtc = GETUTCDATE(),
		LastUpdatedByUserId = @LastUpdatedByUserId
	WHERE TruckId = @TruckId
END