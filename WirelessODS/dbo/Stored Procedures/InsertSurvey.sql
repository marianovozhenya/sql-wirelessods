﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 8/31/2014
-- Description:	Inserts a row into Survey
-- =============================================
CREATE PROCEDURE [dbo].[InsertSurvey]
	@SiteId				INT,
	@WorkOrderId		INT,
	@EnumSurveyTypeId	INT,
	@CreatedByUserId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Survey]
		([SiteId]
		,[WorkOrderId]
		,[EnumSurveyTypeId]
		,[EnumSurveyStatusId]
		,[CreatedAtUtc]
		,[CreatedByUserId]
		,[IsCurrent])
	VALUES
		(@SiteId
		,@WorkOrderId
		,@EnumSurveyTypeId
		,1 -- In Progress
		,GETUTCDATE()
		,@CreatedByUserId
		,0)

	SELECT SCOPE_IDENTITY()
END