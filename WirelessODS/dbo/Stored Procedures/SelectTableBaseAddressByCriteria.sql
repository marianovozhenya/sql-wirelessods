﻿-- =============================================
-- Author:		John Rubin
-- Create date: 07/17/2014
-- Description:	Searches for addresses by criteria
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableBaseAddressByCriteria] 
	-- Add the parameters for the stored procedure here
	@StreetNumber varchar(100),
	@StreetName	  varchar(120),
	@City		  varchar(50),
	@State		  varchar(2),
	@Zip	      varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * 
	FROM BaseAddress
	WHERE
		[State] = @State AND StreetNumber LIKE @StreetNumber + '%' AND StreetName LIKE @StreetName + '%' AND City LIKE @City + '%' AND Zip LIKE @Zip + '%'
END