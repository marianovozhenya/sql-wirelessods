﻿
-- =============================================
-- Author:		<Kirk Johnson>
-- Create date: <4/16/14>
-- Description:	<Select sites by based on name with filter criteria (Type, Status, Hub)>
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableSitesByCriteria]
	@Name		VARCHAR(100),
	@TypeId		INT,
	@HubId		INT,
	@StatusId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT
		s.*,
		a.AddressFull LocationAddressFull,
		r.SurveyId CurrentSurveyId
	FROM [dbo].[Site] s
	JOIN [dbo].[Location] l
		ON l.LocationId = s.LocationId
	JOIN [dbo].[Address] a
		ON a.AddressId = l.AddressId
	LEFT JOIN [dbo].[Survey] r
		ON r.SiteId = s.SiteId AND r.IsCurrent = 1
	WHERE
		(@Name IS NULL OR s.Name LIKE @Name) AND
		(@HubId IS NULL OR s.HubId = @HubId) AND
		(@StatusId IS NULL OR s.EnumSiteStatusId = @StatusId) AND
		(@TypeId IS NULL OR s.EnumSiteTypeId = @TypeId)
	ORDER BY s.Name
END