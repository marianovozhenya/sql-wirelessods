﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 4/22/2014
-- Description:	Select User Role Id's by User Id List
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableUserRoleIdsByUserIdList]
	-- Add the parameters for the stored procedure here
	@IdList			AS IdList	READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		r.UserId,
		r.UserRoleId
	FROM dbo.UserXUserRole r
	WHERE UserId IN (SELECT Id FROM @IdList)
END