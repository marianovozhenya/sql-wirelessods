﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 8/1/2014
-- Description:	Updates a Crew record's Name and DistrictId
-- =============================================
CREATE PROCEDURE [dbo].[UpdateCrew] 
	@CrewId int,
	@Name varchar(100),
	@DistrictId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE Crew
	SET Name = @Name,
		DistrictId = @DistrictId
	WHERE CrewId = @CrewId
END