﻿-- =============================================
-- Author:		John Rubin
-- Create date: 07/02/2014
-- Description:	Searches for Equipment by name, or barcode, and asserts that the result set does not exist in a work order's bom list.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableEquipmentBomByEquipmentName] 
	-- Add the parameters for the stored procedure here
	@WorkOrderId int,
	@EquipmentName varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @EquipmentIds TABLE(EquipmentId int not null)

	INSERT INTO @EquipmentIds
	SELECT wob.EquipmentId
	FROM WorkOrderBom as wob 
	WHERE WorkOrderId = @WorkOrderId AND Quantity <> 0

	SELECT eq.*
	FROM Equipment eq
	WHERE Eq.Name LIKE '%' + @EquipmentName + '%' AND NOT EXISTS(SELECT * FROM @EquipmentIds eqId WHERE eq.EquipmentId = eqId.EquipmentId)
END