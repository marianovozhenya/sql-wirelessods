﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 8/1/2014
-- Description:	Updates a User record's CrewId
-- =============================================
CREATE PROCEDURE [dbo].[UpdateUserCrewId] 
	@UserId int,
	@CrewId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [User]
	SET CrewId = @CrewId
	WHERE UserId = @UserId
END