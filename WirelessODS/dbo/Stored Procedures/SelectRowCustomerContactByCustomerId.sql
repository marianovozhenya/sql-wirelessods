﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 07/24/2014
-- Description:	This proceedure will return a customerContact model
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowCustomerContactByCustomerId] 
	-- Add the parameters for the stored procedure here
	@CustomerId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		x.CustomerId,
		co.FirstName,
		co.LastName,
		co.EmailAddress,
		cu.AzotelId,
		cu.AzotelAuthorizeNetToken,
		cu.BillingAddressId
	FROM [dbo].[Contact] co
	INNER JOIN [dbo].[ContactXCustomer] x on co.ContactId = x.ContactId
	INNER JOIN [dbo].[Customer] cu on cu.CustomerId = x.CustomerId
	WHERE x.CustomerId = @CustomerId
	AND x.EnumCustomerContactTypeId = 1
END