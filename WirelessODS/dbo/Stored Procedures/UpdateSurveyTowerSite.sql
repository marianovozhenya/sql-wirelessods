﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 8/29/2014
-- Description:	Updates the SurveyTowerSite table
-- =============================================
CREATE PROCEDURE [dbo].[UpdateSurveyTowerSite] 
	@SurveyTowerSiteId int,
	@StructureHeight decimal(7,2),
	@ProposedTowerHeight decimal(7,2),
	@GroundTerrainOther varchar(100),
	@EnumInsideGroundTerrainId int,
	@EnumOutsideGroundTerrainId int,
	@EnumSurveyTowerTypeId int,
	@EnumSurveyTowerStatusId int,
	@EnumStructureTypeId int,
	@Notes varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [dbo].[SurveyTowerSite]
	SET [StructureHeight] = @StructureHeight,
		[ProposedTowerHeight] = @ProposedTowerHeight,
		[GroundTerrainOther] = @GroundTerrainOther,
		[EnumInsideGroundTerrainId] = @EnumInsideGroundTerrainId,
		[EnumOutsideGroundTerrainId] = @EnumOutsideGroundTerrainId,
		[EnumSurveyTowerTypeId] = @EnumSurveyTowerTypeId,
		[EnumSurveyTowerStatusId] = @EnumSurveyTowerStatusId,
		[EnumStructureTypeId] = @EnumStructureTypeId,
		[Notes] = @Notes
	WHERE [SurveyTowerSiteId] = @SurveyTowerSiteId
END