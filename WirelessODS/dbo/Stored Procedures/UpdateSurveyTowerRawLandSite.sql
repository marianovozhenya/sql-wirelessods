﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 9/3/2014
-- Description:	Updates the SurveyTowerRawLandSite table
-- =============================================
CREATE PROCEDURE [dbo].[UpdateSurveyTowerRawLandSite] 
	@SurveyTowerRawLandSiteId INT,
	@SquareFootageLeased INT,
	@ProposedStructureHeight DECIMAL(7,2),
	@IsPowerAvailable BIT,
	@PowerAvailableFt DECIMAL(7,2),
	@EnumLandZonedId INT,
	@IsFloodConcern BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [dbo].[SurveyTowerRawLandSite]
	SET [SquareFootageLeased] = @SquareFootageLeased,
		[ProposedStructureHeight] = @ProposedStructureHeight,
		[IsPowerAvailable] = @IsPowerAvailable,
		[PowerAvailableFt] = @PowerAvailableFt,
		[EnumLandZonedId] = @EnumLandZonedId,
		[IsFloodConcern] = @IsFloodConcern
	WHERE [SurveyTowerRawLandSiteId] = @SurveyTowerRawLandSiteId
END