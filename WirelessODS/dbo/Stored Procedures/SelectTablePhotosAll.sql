﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 8/23/2014
-- Description:	Selects all photos ordered by the newest first.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTablePhotosAll]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Photo]
	WHERE IsSoftDeleted = 0
	ORDER BY CreatedAtUtc DESC
END