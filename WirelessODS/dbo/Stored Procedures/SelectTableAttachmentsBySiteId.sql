﻿-- =============================================
-- Author:		Bryan Call
-- Create date: 4/25/2014
-- Description:	Selects the attachments for a site ordered by the newest first.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableAttachmentsBySiteId]
	@SiteId					INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM dbo.Attachment
	WHERE SiteId = @SiteId AND IsSoftDeleted = 0
	ORDER BY CreatedAtUtc DESC
END