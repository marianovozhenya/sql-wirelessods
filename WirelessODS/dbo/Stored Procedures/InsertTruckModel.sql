﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 05/20/2014
-- Description:	This Proceedure will insert a row into Trucks
-- =============================================
CREATE PROCEDURE [dbo].[InsertTruckModel]
	-- Add the parameters for the stored procedure here
		@Name Varchar(50)
		,@TruckMakeId int
        ,@CreatedByUserId int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[TruckModel]
           ([Name]
		   ,[TruckMakeId]
           ,[CreatedByUserId]
		   ,[CreatedAtUtc])
           
     VALUES
           (@Name
		   ,@TruckMakeId
           ,@CreatedByUserId
		   , GETUTCDATE())

	 SELECT SCOPE_IDENTITY()

END