﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 6/26/2014
-- Description:	This proceedure will get a list of all open workorderbom records
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableWorkOrderBomByWorkOrderId] 
	-- Add the parameters for the stored procedure here
	@WorkOrderId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
       wob.*,
	   e.Name AS EquipmentName,
	   e.IsDevice AS EquipmentIsDevice
	FROM dbo.WorkOrderBom  wob
	INNER JOIN Equipment e
		ON e.EquipmentId = wob.EquipmentId
	WHERE WorkOrderId = @WorkOrderId
	AND Quantity <> 0
END