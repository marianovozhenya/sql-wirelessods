﻿-- =============================================
-- Author:		John Rubin
-- Create date: 07/16/2014
-- Description:	Updates a WpdaLog entry
-- =============================================
CREATE PROCEDURE [dbo].[UpdateWpdaLog] 
	-- Add the parameters for the stored procedure here
	@WpdaLogId int,
	@UserId int,
	@RequestContext varchar(max),
	@WasSuccessful bit,
	@ExceptionDetails varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE WpdaLog
	SET
	UserId = @UserId,
	RequestContext = @RequestContext,
	EndDateTimeLocal = GETDATE(),
	WasSuccessful = @WasSuccessful,
	ExceptionDetails = @ExceptionDetails
	
	WHERE WpdaLogId = @WpdaLogId
END