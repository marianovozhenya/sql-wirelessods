﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 5/20/2014
-- Description:	This Proceedure will get a Truck by ID
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableTruckModelAll]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT [TruckModelId], [TruckMakeId], [Name]
  FROM [dbo].[TruckModel]
  ORDER BY [Name]

END