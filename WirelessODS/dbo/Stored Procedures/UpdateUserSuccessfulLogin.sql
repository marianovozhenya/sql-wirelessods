﻿-- =============================================
-- Author:		Bryan Call
-- Create date: 4/3/2014
-- Description:	Updates a user record to reset its failed login counter and update the last login date.
-- =============================================
CREATE PROCEDURE [dbo].[UpdateUserSuccessfulLogin] 
	@UserId			INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [User]
	SET
		FailedLoginAttemptCounter = 0,
		LastLoginAtUtc = GETUTCDATE()
	WHERE UserId = @UserId
END