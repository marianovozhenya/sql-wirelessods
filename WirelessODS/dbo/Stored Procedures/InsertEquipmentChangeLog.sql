﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 7/10/2014
-- Description:	This proceedure will Insert a row into the EquipmentChangeLog
-- =============================================
CREATE PROCEDURE [dbo].[InsertEquipmentChangeLog] 
	-- Add the parameters for the stored procedure here
			@EquipmentId int
           ,@ChangeTypeId int
           ,@Change varchar(50)
           ,@ChangeByUserId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[EquipmentChangeLog]
           ([EquipmentId]
           ,[ChangeTypeId]
           ,[Change]
           ,[ChangeByUserId]
           ,[ChangedDate])
     VALUES
           (@EquipmentId
           ,@ChangeTypeId
           ,@Change
           ,@ChangeByUserId
           ,GETUTCDATE())

END