﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 05/8/2014
-- Description:	Inserts a Manufacturer
-- =============================================
CREATE PROCEDURE [dbo].[InsertManufacturer] 
	-- Add the parameters for the stored procedure here
	@Name varchar(100),
	@CreatedById int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Manufacturer]
		(
			[Name],
			[CreatedAtUtc],
			[CreatedByUserId]
		)
		VALUES
		(
			@Name,
			GETUTCDATE(),
			@CreatedById
		)

	SELECT SCOPE_IDENTITY()
END