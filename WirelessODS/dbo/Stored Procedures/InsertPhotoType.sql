﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 5/19/2014
-- Description:	Inserts a new photo type
-- =============================================
CREATE PROCEDURE [dbo].[InsertPhotoType]
	@Name				VARCHAR(100),
	@CreatedByUserId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[PhotoType]
			   ([Name]
			   ,[CreatedAtUtc]
			   ,[CreatedByUserId])
		 VALUES
			   (@Name
			   ,GETUTCDATE()
			   ,@CreatedByUserId)

	SELECT SCOPE_IDENTITY()
END