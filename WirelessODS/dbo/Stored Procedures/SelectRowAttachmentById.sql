﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 4/23/2014
-- Description:	Selects an attachment by Id
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowAttachmentById]
	@AttachmentId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Attachment]
	WHERE AttachmentId = @AttachmentId AND IsSoftDeleted = 0
END