﻿-- =============================================
-- Author:		JohnRubin
-- Create date: 05/20/2014
-- Description:	Retrieves all trucks.
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableTruckAll] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT truck.* ,
		   truckMake.Name AS [TruckMakeName],
		   truckModel.Name AS [TruckModelName]
	FROM Truck truck
	INNER JOIN dbo.TruckMake truckMake ON truckMake.TruckMakeId = Truck.TruckMakeId
	INNER JOIN dbo.TruckModel truckModel ON truckModel.TruckModelId = Truck.TruckModelId
END