﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 9/3/2014
-- Description:	Updates a row in SurveyDevice
-- =============================================
CREATE PROCEDURE [dbo].[UpdateSurveyDevice] 
	@SurveyDeviceId INT,
	@DeviceName VARCHAR(100),
	@SSID VARCHAR(50),
	@RadioType VARCHAR(50),
	@IPAddress VARCHAR(50),
	@WirelessMAC VARCHAR(50),
	@ParentIP VARCHAR(50),
	@ParentDevice VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE SurveyDevice
	SET DeviceName = @DeviceName,
		SSID = @SSID,
		RadioType = @RadioType,
		IPAddress = @IPAddress,
		WirelessMAC = @WirelessMAC,
		ParentIP = @ParentIP,
		ParentDevice = @ParentDevice
	WHERE SurveyDeviceId = @SurveyDeviceId
END