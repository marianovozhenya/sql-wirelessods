﻿-- =============================================
-- Author:		Adan Pineda
-- Create date: 8/29/2014
-- Description:	Inserts a row into SurveyTowerTelco
-- =============================================
CREATE PROCEDURE [dbo].[InsertSurveyTowerTelco] 
	-- Add the parameters for the stored procedure here
		@SurveyId INT,
		@EnumEnclosureId INT,
		@LengthFromTelcoToNewCabinet DECIMAL(7,2),
		@EstimatedCost SMALLMONEY,
		@Notes VARCHAR(MAX),
		@TelcoCompany VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [SurveyTowerTelco] (
		[SurveyId],
		[EnumEnclosureId],
		[LengthFromTelcoToNewCabinet],
		[EstimatedCost],
		[Notes] ,
		[TelcoCompany])
	VALUES (
		@SurveyId,
		@EnumEnclosureId,
		@LengthFromTelcoToNewCabinet,
		@EstimatedCost,
		@Notes ,
		@TelcoCompany)

		SELECT SCOPE_IDENTITY()
END