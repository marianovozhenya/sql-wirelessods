﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 9/3/2014
-- Description:	Selects a SurveryTowerRawLandSite  section row by Id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowSurveyTowerRawLandSiteBySurveyId]
	@SurveyId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[SurveyTowerRawLandSite]
	WHERE [SurveyId] = @SurveyId
END