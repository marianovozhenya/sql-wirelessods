﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/30/2014
-- Description:	This proceedure will update the Azotel AuthorizeNet Token
-- =============================================
CREATE PROCEDURE [dbo].[UpdateCustomerAuthorizeNetToken]
	-- Add the parameters for the stored procedure here
	@CustomerId					INT,
	@AzotelAuthorizeNetToken	VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE [dbo].[Customer]
	SET
		AzotelAuthorizeNetToken = @AzotelAuthorizeNetToken,
		AzotelAuthorizeNetTokenAddedToAzotelAtUtc = GETUTCDATE()
	WHERE  @CustomerId = CustomerId
END