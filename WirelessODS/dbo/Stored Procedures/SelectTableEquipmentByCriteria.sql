﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/9/2014
-- Description:	Selects equipment by criteria
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableEquipmentByCriteria]
	@Name				VARCHAR(100),
	@Model				VARCHAR(50),
	@TypeId				INT,
	@ManufacturerId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[Equipment]
	WHERE
		(@Name IS NULL OR Name LIKE @Name) AND
		(@Model IS NULL OR Model LIKE @Model) AND
		(@TypeId IS NULL OR EquipmentTypeId = @TypeId) AND
		(@ManufacturerId IS NULL OR ManufacturerId = @ManufacturerId)
	ORDER BY Name
END