﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 4/14/2014
-- Description:	This stored proceedure will edit a row
-- =============================================
CREATE PROCEDURE [dbo].[EditUserAdmin] 
	-- Add the parameters for the stored procedure here
	
	@UserId int,
	@FirstName varchar(50),
	@LastName VARCHAR(50),
	@PhoneNumber varchar(20) ,
	@CellNumber varchar(20) ,
	@EmailAddress varchar(100) ,
	@IsEnabled bit,
	@ManagerId int,
	@LastUpdatedByUserId int,
	@TruckId int,
	@DistrictId int,
	@CrewId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE [dbo].[User]
   SET [FirstName] = @FirstName
      ,[LastName] = @LastName
      ,[PhoneNumber] = @PhoneNumber
      ,[CellNumber] = @CellNumber
      ,[EmailAddress] = @EmailAddress
      ,[IsEnabled] = @IsEnabled
      ,[ManagerId] = @ManagerId
      ,[LastUpdatedByUserId] = @LastUpdatedByUserId
	  ,[TruckId] = @TruckId
	  ,[DistrictId] = @DistrictId
	  ,[CrewId] = @CrewId
      ,[LastUpdatedAtUtc] = GETUTCDATE()
 WHERE UserId = @UserId



END