﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 5/22/2014
-- Description:	This proceedure will return if a truckMake exists
-- =============================================
CREATE PROCEDURE [dbo].[SelectTruckMakeExists] 
	-- Add the parameters for the stored procedure here
	@Name varchar(50) = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	select count(*)
	from TruckMake
	where Name = @Name
END