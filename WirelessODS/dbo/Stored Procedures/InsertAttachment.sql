﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 4/23/2014
-- Description:	Inserts an attachment
-- =============================================
CREATE PROCEDURE [dbo].[InsertAttachment]
	@Name					VARCHAR(100),
	@AttachmentTypeId		INT,
	@EnumUploadStatusId		INT,
	@S3Bucket				VARCHAR(100),
	@S3Location				VARCHAR(MAX),
	@LocationId				INT,
	@SiteId					INT,
	@WorkOrderId			INT,
	@CreatedByUserId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Attachment]
           ([Name]
           ,[AttachmentTypeId]
           ,[EnumUploadStatusId]
           ,[S3Bucket]
           ,[S3Location]
           ,[LocationId]
           ,[SiteId]
		   ,[WorkOrderId]
           ,[CreatedByUserId]
           ,[CreatedAtUtc]
           ,[IsSoftDeleted])
     VALUES
           (@Name
           ,@AttachmentTypeId
           ,@EnumUploadStatusId
           ,@S3Bucket
           ,@S3Location
           ,@LocationId
           ,@SiteId
		   ,@WorkOrderId
           ,@CreatedByUserId
           ,GETUTCDATE()
           ,0)

	SELECT SCOPE_IDENTITY()
END