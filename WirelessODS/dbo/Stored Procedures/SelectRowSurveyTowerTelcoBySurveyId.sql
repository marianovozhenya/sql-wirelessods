﻿-- =============================================
-- Author:		Adan Pineda
-- Create date: 8/29/2014
-- Description:	Selects a Survery Tower Telco Information section row by Id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowSurveyTowerTelcoBySurveyId]
	@SurveyId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM [dbo].[SurveyTowerTelco]
	WHERE [SurveyId] = @SurveyId
END