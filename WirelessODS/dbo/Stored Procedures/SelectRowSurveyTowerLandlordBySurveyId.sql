﻿-- =============================================
-- Author:		Jon Zelenik
-- Create date: 8/28/2014
-- Description:	Selects a SurveyTowerLandlord row by SurveyTowerLandlordId
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowSurveyTowerLandlordBySurveyId] 
	@SurveyId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT *
	FROM SurveyTowerLandlord
	WHERE SurveyId = @SurveyId
END