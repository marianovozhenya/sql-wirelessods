﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 7/17/2014
-- Description:	Inserts a new row into the service table.
-- =============================================
CREATE PROCEDURE [dbo].[InsertService]
	@AddressId			INT,
	@CustomerId			INT,
	@DeviceId			INT,
	@CreatedByUserId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[Service]
		([CustomerId]
		,[DeviceId]
		,[AddressId]
		,[CreatedAtUtc]
		,[CreatedByUserId])
	VALUES
		(@CustomerId
		,@DeviceId
		,@AddressId
		,GETUTCDATE()
		,@CreatedByUserId)

	SELECT SCOPE_IDENTITY()
END