﻿-- Stored Procedure

-- =============================================
-- Author:		Kirk Johnson
-- Create date: 7/14/2014
-- Description:	Selects an address by id of Address table and joins to the BaseAddress table to get address data.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowAddressById]
	@AddressId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		a.[AddressId],
		a.[BaseAddressId],
		a.[SuiteType],
		a.[SuiteNumber],
		a.[BoxType],
		a.[BoxNumber],
		a.[AddressFull],
		a.[AddressLine],
		a.[EnumAddressValidationServiceId],
		a.[ValidationDate],
		a.[ValidationKey],
		a.[CompositeKey],
		ba.[BaseCompositeKey],
		ba.[StreetNumber],
		ba.[PreDirection],
		ba.[StreetName],
		ba.[StreetSuffix],
		ba.[PostDirection],
		ba.[City],
		ba.[State],
		ba.[Zip],
		ba.[BaseAddressFull],
		ba.[BaseAddressLine]
	FROM [dbo].[Address] a
	INNER JOIN [dbo].[BaseAddress] ba
		ON a.BaseAddressId = ba.BaseAddressId
	WHERE a.AddressId = @AddressId
END