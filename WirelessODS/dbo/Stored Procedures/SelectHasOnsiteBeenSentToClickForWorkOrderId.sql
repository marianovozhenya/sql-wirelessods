﻿-- Stored Procedure

-- =============================================
-- Author:		Kirk Johnson
-- Create date: 8/1/2014
-- Description:	Selects OnsiteSentToClickAtUtc for the WorkOrderId passed in.
-- =============================================
CREATE PROCEDURE [dbo].[SelectHasOnsiteBeenSentToClickForWorkOrderId]
	@WorkOrderId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		OnsiteSentToClickAtUtc
	FROM [dbo].[WorkOrder]
	WHERE WorkOrderId = @WorkOrderId
END