﻿-- =============================================
-- Author:		Kirk Johnson
-- Create date: 8/28/2014
-- Description:	Selects a Survery Tower Site Information section row by Id.
-- =============================================
CREATE PROCEDURE [dbo].[SelectRowSurveyTowerSiteBySurveyId]
	@SurveyId	INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		*
	FROM [dbo].[SurveyTowerSite]
	WHERE [SurveyId] = @SurveyId
END