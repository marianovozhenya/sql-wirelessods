﻿-- =============================================
-- Author:		Bob Taylor
-- Create date: 5/20/2014
-- Description:	Updates the photo upload status
-- =============================================
CREATE PROCEDURE [dbo].[UpdatePhotoUploadStatus]
	@PhotoId				INT,
	@EnumUploadStatusId		INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE [dbo].[Photo]
	SET
		EnumUploadStatusId = @EnumUploadStatusId
	WHERE PhotoId = @PhotoId
END