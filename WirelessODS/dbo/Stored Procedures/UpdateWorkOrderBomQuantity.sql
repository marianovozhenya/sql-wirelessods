﻿-- =============================================
-- Author:		Clint Roberts
-- Create date: 7/1/2014
-- Description:	This proceedure will update the Quantity of a bom
-- =============================================
CREATE PROCEDURE [dbo].[UpdateWorkOrderBomQuantity] 
	-- Add the parameters for the stored procedure here
	@WorkOrderBomId int, 
	@Quantity int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update WorkOrderBom
	set Quantity = @Quantity
	where WorkOrderBomId =  @WorkOrderBomId

END