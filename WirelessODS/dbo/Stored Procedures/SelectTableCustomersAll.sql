﻿-- Stored Procedure

-- =============================================
-- Author:		Kirk Johnson
-- Create date: 8/13/2014
-- Description:	Pulls Customer data for the table
-- =============================================
CREATE PROCEDURE [dbo].[SelectTableCustomersAll] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT
		cu.CustomerId,
		co.FirstName,
		co.LastName,
		co.EmailAddress [Email],
		co.PhoneNumber [Phone],
		instaddr.AddressFull [InstallationAddress],
		ISNULL(billaddr.AddressFull, '') [BillingAddress]
	FROM Customer cu
		JOIN ContactXCustomer cxc ON cxc.CustomerId = cu.CustomerId
		JOIN Contact co ON co.ContactId = cxc.ContactId
		LEFT OUTER JOIN [Address] billaddr ON billaddr.AddressId = cu.BillingAddressId
		LEFT OUTER JOIN [Service] s ON s.CustomerId = cu.CustomerId
		LEFT OUTER JOIN [Address] instaddr ON instaddr.AddressId = s.AddressId 
	WHERE cxc.EnumCustomerContactTypeId = 1
END