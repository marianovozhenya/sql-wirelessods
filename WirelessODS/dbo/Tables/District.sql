﻿CREATE TABLE [dbo].[District] (
    [DistrictId] INT           IDENTITY (1, 1) NOT NULL,
    [RegionId]   INT           NOT NULL,
    [Name]       VARCHAR (100) NOT NULL,
    CONSTRAINT [PKCX_District_DistrictId] PRIMARY KEY CLUSTERED ([DistrictId] ASC),
    CONSTRAINT [FK_District_RegionId] FOREIGN KEY ([RegionId]) REFERENCES [dbo].[Region] ([RegionId])
);

