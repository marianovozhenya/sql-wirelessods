﻿CREATE TABLE [dbo].[Service] (
    [ServiceId]           INT      IDENTITY (1, 1) NOT NULL,
    [CustomerId]          INT      NOT NULL,
    [DeviceId]            INT      NULL,
    [AddressId]           INT      NOT NULL,
    [EnumServiceStatusId] INT      CONSTRAINT [DEF_User_EnumServiceStatus] DEFAULT ((0)) NOT NULL,
    [CreatedAtUtc]        DATETIME NOT NULL,
    [CreatedByUserId]     INT      NOT NULL,
    CONSTRAINT [PKCX_Service_ServiceId] PRIMARY KEY CLUSTERED ([ServiceId] ASC),
    CONSTRAINT [FK_Service_Address] FOREIGN KEY ([AddressId]) REFERENCES [dbo].[Address] ([AddressId]),
    CONSTRAINT [FK_Service_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_Service_Device] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[Device] ([DeviceId]),
    CONSTRAINT [FK_Service_EnumServiceStatus] FOREIGN KEY ([EnumServiceStatusId]) REFERENCES [dbo].[EnumServiceStatus] ([EnumServiceStatusId])
);


GO
CREATE NONCLUSTERED INDEX [NCX_Service_AddressId]
    ON [dbo].[Service]([AddressId] ASC);


GO
CREATE NONCLUSTERED INDEX [NCX_Service_DeviceId]
    ON [dbo].[Service]([DeviceId] ASC);


GO
CREATE NONCLUSTERED INDEX [NCX_Service_CustomerId]
    ON [dbo].[Service]([CustomerId] ASC);

