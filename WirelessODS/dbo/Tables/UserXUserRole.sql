﻿CREATE TABLE [dbo].[UserXUserRole] (
    [UserXUserRoleId] INT IDENTITY (1, 1) NOT NULL,
    [UserId]          INT NOT NULL,
    [UserRoleId]      INT NOT NULL,
    CONSTRAINT [PKCX_UserXUserRole_UserXUserRoleId] PRIMARY KEY CLUSTERED ([UserXUserRoleId] ASC),
    CONSTRAINT [FK_UserXUserRole_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId]),
    CONSTRAINT [FK_UserXUserRole_UserRole] FOREIGN KEY ([UserRoleId]) REFERENCES [dbo].[UserRole] ([UserRoleId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_UserXUserRole_UserId_UserRoleId]
    ON [dbo].[UserXUserRole]([UserId] ASC, [UserRoleId] ASC);

