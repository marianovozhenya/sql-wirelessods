﻿CREATE TABLE [dbo].[WorkOrderScheduleLog] (
    [WorkOrderScheduleLogId]   INT           IDENTITY (1, 1) NOT NULL,
    [WorkOrderId]              INT           NOT NULL,
    [DateEnteredUtc]           DATETIME      NOT NULL,
    [AppointmentWindowStart]   DATETIME      NULL,
    [AppointmentWindowEnd]     DATETIME      NULL,
    [ChangedByUserId]          INT           NULL,
    [StatusChange]             VARCHAR (50)  NOT NULL,
    [Reason]                   VARCHAR (100) NOT NULL,
    [AssignedUserId]           INT           NULL,
    [AssignedCrewId]           INT           NULL,
    [AcknowledgedByClickAtUtc] DATETIME      NULL,
    CONSTRAINT [PKCX_WorkOrderScheduleLog_WorkOrderScheduleLogId] PRIMARY KEY CLUSTERED ([WorkOrderScheduleLogId] ASC)
);


GO
CREATE NONCLUSTERED INDEX [NCX_WorkOrderScheduleLog_WorkOrderId]
    ON [dbo].[WorkOrderScheduleLog]([WorkOrderId] ASC);

