﻿CREATE TABLE [dbo].[EnumWorkOrderCommentPriority] (
    [EnumWorkOrderCommentPriorityId] INT           NOT NULL,
    [SystemName]                     VARCHAR (50)  NOT NULL,
    [DisplayName]                    VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumWorkOrderCommentPriority_EnumWorkOrderCommentPriorityId] PRIMARY KEY CLUSTERED ([EnumWorkOrderCommentPriorityId] ASC)
);

