﻿CREATE TABLE [dbo].[WorkOrderLogicalDevice] (
    [WorkOrderLogicalDeviceId] INT      IDENTITY (1, 1) NOT NULL,
    [WorkOrderId]              INT      NOT NULL,
    [EquipmentId]              INT      NOT NULL,
    [Provisioned]              BIT      NOT NULL,
    [ProvisionedByUserId]      INT      NULL,
    [ProvisionedAtUtc]         DATETIME NULL,
    CONSTRAINT [PKCX_WorkOrderLogicalDevice_WorkOrderLogicalDeviceId] PRIMARY KEY CLUSTERED ([WorkOrderLogicalDeviceId] ASC),
    CONSTRAINT [WorkOrderLogicalDevice_EquipmentId] FOREIGN KEY ([EquipmentId]) REFERENCES [dbo].[WorkOrder] ([WorkOrderId]),
    CONSTRAINT [WorkOrderLogicalDevice_WorkOrder] FOREIGN KEY ([WorkOrderId]) REFERENCES [dbo].[WorkOrder] ([WorkOrderId])
);


GO
CREATE NONCLUSTERED INDEX [NCX_WorkOrderLogicalDevice_WorkOrderId_EquipmentId]
    ON [dbo].[WorkOrderLogicalDevice]([WorkOrderId] ASC, [EquipmentId] ASC);

