﻿CREATE TABLE [dbo].[Hub] (
    [HubId]               INT           IDENTITY (1, 1) NOT NULL,
    [Name]                VARCHAR (100) NOT NULL,
    [SiteId]              INT           NULL,
    [CreatedAtUtc]        DATETIME      NOT NULL,
    [CreatedByUserId]     INT           NOT NULL,
    [LastUpdatedAtUtc]    DATETIME      NULL,
    [LastUpdatedByUserId] INT           NULL,
    CONSTRAINT [PKCX_Hub_HubId] PRIMARY KEY CLUSTERED ([HubId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_Hub_Name]
    ON [dbo].[Hub]([Name] ASC);

