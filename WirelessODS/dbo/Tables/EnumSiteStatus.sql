﻿CREATE TABLE [dbo].[EnumSiteStatus] (
    [EnumSiteStatusId] INT           NOT NULL,
    [SystemName]       VARCHAR (50)  NOT NULL,
    [DisplayName]      VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumSiteStatus_EnumSiteStatusId] PRIMARY KEY CLUSTERED ([EnumSiteStatusId] ASC)
);

