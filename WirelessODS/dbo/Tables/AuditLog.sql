﻿CREATE TABLE [dbo].[AuditLog] (
    [AuditLogId]              INT           IDENTITY (1, 1) NOT NULL,
    [EnteredAtUtc]            DATETIME      NOT NULL,
    [EnumAuditLogEntryTypeId] INT           NOT NULL,
    [UserId]                  INT           NULL,
    [IPAddress]               VARCHAR (45)  NOT NULL,
    [AffectedUserId]          INT           NULL,
    [Details]                 VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PKCX_AuditLog_AuditLogId] PRIMARY KEY CLUSTERED ([AuditLogId] ASC),
    CONSTRAINT [FK_AuditLog_EnumAuditLogEntryType] FOREIGN KEY ([EnumAuditLogEntryTypeId]) REFERENCES [dbo].[EnumAuditLogEntryType] ([EnumAuditLogEntryTypeId]),
    CONSTRAINT [FK_AuditLog_User_AffectedUserId] FOREIGN KEY ([AffectedUserId]) REFERENCES [dbo].[User] ([UserId]),
    CONSTRAINT [FK_AuditLog_User_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
);

