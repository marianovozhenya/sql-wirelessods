﻿CREATE TABLE [dbo].[EnumStructureType] (
    [EnumStructureTypeId] INT           NOT NULL,
    [SystemName]          VARCHAR (50)  NOT NULL,
    [DisplayName]         VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumStructureType_EnumStructureTypeId] PRIMARY KEY CLUSTERED ([EnumStructureTypeId] ASC)
);

