﻿CREATE TABLE [dbo].[EnumSurveyTowerStatus] (
    [EnumSurveyTowerStatusId] INT           NOT NULL,
    [SystemName]              VARCHAR (50)  NOT NULL,
    [DisplayName]             VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumSurveyTowerStatus_EnumSurveyTowerStatusId] PRIMARY KEY CLUSTERED ([EnumSurveyTowerStatusId] ASC)
);

