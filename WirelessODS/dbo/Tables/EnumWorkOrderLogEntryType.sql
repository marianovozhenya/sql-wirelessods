﻿CREATE TABLE [dbo].[EnumWorkOrderLogEntryType] (
    [EnumWorkOrderLogEntryTypeId] INT           NOT NULL,
    [SystemName]                  VARCHAR (50)  NOT NULL,
    [DisplayName]                 VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumWorkOrderLogEntryType_EnumWorkOrderLogEntryTypeId] PRIMARY KEY CLUSTERED ([EnumWorkOrderLogEntryTypeId] ASC)
);

