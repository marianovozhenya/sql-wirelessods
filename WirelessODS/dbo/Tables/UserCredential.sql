﻿CREATE TABLE [dbo].[UserCredential] (
    [UserCredentialId]     INT           IDENTITY (1, 1) NOT NULL,
    [UserId]               INT           NOT NULL,
    [Username]             VARCHAR (50)  NOT NULL,
    [EnumCredentialTypeId] INT           NOT NULL,
    [CredentialValue]      VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PKCX_UserCredential_UserId] PRIMARY KEY CLUSTERED ([UserId] ASC),
    CONSTRAINT [FK_UserCredential_EnumCredentialType] FOREIGN KEY ([EnumCredentialTypeId]) REFERENCES [dbo].[EnumCredentialType] ([EnumCredentialTypeId]),
    CONSTRAINT [FK_UserCredential_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_UserCredential_Username_EnumCredentialTypeId]
    ON [dbo].[UserCredential]([Username] ASC, [EnumCredentialTypeId] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_UserCredential_UserId]
    ON [dbo].[UserCredential]([UserId] ASC);

