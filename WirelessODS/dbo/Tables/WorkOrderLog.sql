﻿CREATE TABLE [dbo].[WorkOrderLog] (
    [WorkOrderLogId]              INT           IDENTITY (1, 1) NOT NULL,
    [WorkOrderId]                 INT           NOT NULL,
    [EnteredAtUtc]                DATETIME      NOT NULL,
    [EnteredByUserId]             INT           NOT NULL,
    [EnumWorkOrderLogEntryTypeId] INT           NOT NULL,
    [Comments]                    VARCHAR (MAX) NOT NULL,
    [WorkOrderNumber]             VARCHAR (20)  NULL,
    CONSTRAINT [PKCX_WorkOrderLog_WorkOrderLogId] PRIMARY KEY CLUSTERED ([WorkOrderLogId] ASC),
    CONSTRAINT [FK_WorkOrderLog_EnumWorkOrderLogEntryType] FOREIGN KEY ([EnumWorkOrderLogEntryTypeId]) REFERENCES [dbo].[EnumWorkOrderLogEntryType] ([EnumWorkOrderLogEntryTypeId]),
    CONSTRAINT [FK_WorkOrderLog_WorkOrder] FOREIGN KEY ([WorkOrderId]) REFERENCES [dbo].[WorkOrder] ([WorkOrderId])
);

