﻿CREATE TABLE [dbo].[EnumWorkOrderPriority] (
    [EnumWorkOrderPriorityId] INT           NOT NULL,
    [SystemName]              VARCHAR (50)  NOT NULL,
    [DisplayName]             VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumWorkOrderPriority_EnumWorkOrderPriorityId] PRIMARY KEY CLUSTERED ([EnumWorkOrderPriorityId] ASC)
);

