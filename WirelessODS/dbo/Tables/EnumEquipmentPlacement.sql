﻿CREATE TABLE [dbo].[EnumEquipmentPlacement] (
    [EnumEquipmentPlacementId] INT           NOT NULL,
    [SystemName]               VARCHAR (50)  NOT NULL,
    [DisplayName]              VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumEquipmentPlacement_EnumEquipmentPlacementId] PRIMARY KEY CLUSTERED ([EnumEquipmentPlacementId] ASC)
);

