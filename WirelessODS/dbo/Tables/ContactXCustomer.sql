﻿CREATE TABLE [dbo].[ContactXCustomer] (
    [ContactXCustomerId]        INT          IDENTITY (1, 1) NOT NULL,
    [ContactId]                 INT          NOT NULL,
    [CustomerId]                INT          NOT NULL,
    [EnumCustomerContactTypeId] INT          NOT NULL,
    [Role]                      VARCHAR (50) NOT NULL,
    CONSTRAINT [PKCX_ContactXCustomer] PRIMARY KEY CLUSTERED ([ContactXCustomerId] ASC),
    CONSTRAINT [FK_ContactXCustomer_EnumCustomerContactType] FOREIGN KEY ([EnumCustomerContactTypeId]) REFERENCES [dbo].[EnumCustomerContactType] ([EnumCustomerContactTypeId])
);


GO
CREATE NONCLUSTERED INDEX [NCX_ContactXCustomer_CustomerId]
    ON [dbo].[ContactXCustomer]([CustomerId] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_ContactXCustomer_ContactId_CustomerId_EnumCustomerContactTypeId]
    ON [dbo].[ContactXCustomer]([ContactId] ASC, [CustomerId] ASC, [EnumCustomerContactTypeId] ASC);

