﻿CREATE TABLE [dbo].[Crew] (
    [CrewId]     INT           IDENTITY (1, 1) NOT NULL,
    [Name]       VARCHAR (100) NOT NULL,
    [DistrictId] INT           NOT NULL,
    CONSTRAINT [PKCX_Crew_CrewId] PRIMARY KEY CLUSTERED ([CrewId] ASC),
    CONSTRAINT [FK_Crew_District] FOREIGN KEY ([DistrictId]) REFERENCES [dbo].[District] ([DistrictId])
);

