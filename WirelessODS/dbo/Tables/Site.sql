﻿CREATE TABLE [dbo].[Site] (
    [SiteId]              INT           IDENTITY (1000, 1) NOT NULL,
    [LocationId]          INT           NOT NULL,
    [Name]                VARCHAR (100) NOT NULL,
    [EnumSiteTypeId]      INT           NOT NULL,
    [EnumSiteStatusId]    INT           NOT NULL,
    [Place]               VARCHAR (MAX) NOT NULL,
    [Directions]          VARCHAR (MAX) NOT NULL,
    [HubId]               INT           NULL,
    [CreatedAtUtc]        DATETIME      NOT NULL,
    [CreatedByUserId]     INT           NOT NULL,
    [LastUpdatedAtUtc]    DATETIME      NULL,
    [LastUpdatedByUserId] INT           NULL,
    CONSTRAINT [PKCX_Site_SiteId] PRIMARY KEY CLUSTERED ([SiteId] ASC),
    CONSTRAINT [FK_Site_EnumSiteStatus] FOREIGN KEY ([EnumSiteStatusId]) REFERENCES [dbo].[EnumSiteStatus] ([EnumSiteStatusId]),
    CONSTRAINT [FK_Site_EnumSiteType] FOREIGN KEY ([EnumSiteTypeId]) REFERENCES [dbo].[EnumSiteType] ([EnumSiteTypeId]),
    CONSTRAINT [FK_Site_Hub] FOREIGN KEY ([HubId]) REFERENCES [dbo].[Hub] ([HubId]),
    CONSTRAINT [FK_Site_Location] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([LocationId])
);


GO
CREATE NONCLUSTERED INDEX [NCX_Site_HubId]
    ON [dbo].[Site]([HubId] ASC);


GO
CREATE NONCLUSTERED INDEX [NCX_Site_EnumSiteStatusId]
    ON [dbo].[Site]([EnumSiteStatusId] ASC);


GO
CREATE NONCLUSTERED INDEX [NCX_Site_EnumSiteTypeId]
    ON [dbo].[Site]([EnumSiteTypeId] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_Site_Name]
    ON [dbo].[Site]([Name] ASC);


GO
CREATE NONCLUSTERED INDEX [NCX_Site_LocationId]
    ON [dbo].[Site]([LocationId] ASC);

