﻿CREATE TABLE [dbo].[EnumRADMountType] (
    [EnumRADMountTypeId] INT           NOT NULL,
    [SystemName]         VARCHAR (50)  NOT NULL,
    [DisplayName]        VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumRADMountType_EnumRADMountTypeId] PRIMARY KEY CLUSTERED ([EnumRADMountTypeId] ASC)
);

