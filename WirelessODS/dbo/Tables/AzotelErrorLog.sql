﻿CREATE TABLE [dbo].[AzotelErrorLog] (
    [AzotelErrorLogId]        INT           IDENTITY (1, 1) NOT NULL,
    [CustomerId]              INT           NOT NULL,
    [ServiceId]               INT           NULL,
    [PaymentProfileId]        INT           NULL,
    [ServiceAddressId]        INT           NULL,
    [BillingAddressId]        INT           NULL,
    [AzotelId]                INT           NULL,
    [Error]                   VARCHAR (MAX) NOT NULL,
    [CreatedAtUtc]            DATETIME      NOT NULL,
    [CreatedByUserId]         INT           NOT NULL,
    [AzotelAuthorizeNetToken] VARCHAR (100) NULL,
    [MethodName]              VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_AzotelErrorLog_AzotelErrorLogId] PRIMARY KEY CLUSTERED ([AzotelErrorLogId] ASC)
);

