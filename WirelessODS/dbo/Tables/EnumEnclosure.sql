﻿CREATE TABLE [dbo].[EnumEnclosure] (
    [EnumEnclosureId] INT           NOT NULL,
    [SystemName]      VARCHAR (50)  NOT NULL,
    [DisplayName]     VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumEnclosure_EnumEnclosureId] PRIMARY KEY CLUSTERED ([EnumEnclosureId] ASC)
);

