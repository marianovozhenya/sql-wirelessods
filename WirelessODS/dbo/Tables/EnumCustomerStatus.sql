﻿CREATE TABLE [dbo].[EnumCustomerStatus] (
    [EnumCustomerStatusId] INT           NOT NULL,
    [SystemName]           VARCHAR (50)  NOT NULL,
    [DisplayName]          VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumCustomerStatus_EnumCustomerStatusId] PRIMARY KEY CLUSTERED ([EnumCustomerStatusId] ASC)
);

