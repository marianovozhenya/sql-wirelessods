﻿CREATE TABLE [dbo].[UserRole] (
    [UserRoleId]         INT          NOT NULL,
    [Name]               VARCHAR (50) NOT NULL,
    [EnumUserRoleTypeId] INT          NOT NULL,
    CONSTRAINT [PKCX_UserRole_UserRoleId] PRIMARY KEY CLUSTERED ([UserRoleId] ASC),
    CONSTRAINT [FK_UserRole_EnumUserRoleTypeId] FOREIGN KEY ([EnumUserRoleTypeId]) REFERENCES [dbo].[EnumUserRoleType] ([EnumUserRoleTypeId])
);

