﻿CREATE TABLE [dbo].[Location] (
    [LocationId]                  INT            IDENTITY (1000, 1) NOT NULL,
    [Name]                        VARCHAR (100)  NOT NULL,
    [EnumLocationTypeId]          INT            NOT NULL,
    [Latitude]                    FLOAT (53)     NOT NULL,
    [Longitude]                   FLOAT (53)     NOT NULL,
    [CreatedAtUtc]                DATETIME       NOT NULL,
    [CreatedByUserId]             INT            NOT NULL,
    [LastUpdatedAtUtc]            DATETIME       NULL,
    [LastUpdatedByUserId]         INT            NULL,
    [AddressId]                   INT            NOT NULL,
    [EnumMelissaGeoCodeQualityId] INT            CONSTRAINT [DEF_Location_EnumMelissaGeoCodeQuality] DEFAULT ((0)) NOT NULL,
    [GeoCodePrecision]            DECIMAL (6, 3) CONSTRAINT [DEF_Location_GeoCodePrecision] DEFAULT ((0.0)) NOT NULL,
    CONSTRAINT [PKCX_Location_LocationId] PRIMARY KEY CLUSTERED ([LocationId] ASC),
    CONSTRAINT [FK_Location_Address] FOREIGN KEY ([AddressId]) REFERENCES [dbo].[Address] ([AddressId]),
    CONSTRAINT [FK_Location_EnumLocationType] FOREIGN KEY ([EnumLocationTypeId]) REFERENCES [dbo].[EnumLocationType] ([EnumLocationTypeId]),
    CONSTRAINT [FK_Location_EnumMelissaGeoCodeQuality] FOREIGN KEY ([EnumMelissaGeoCodeQualityId]) REFERENCES [dbo].[EnumMelissaGeoCodeQuality] ([EnumMelissaGeoCodeQualityId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_Location_AddressId]
    ON [dbo].[Location]([AddressId] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_Location_Name]
    ON [dbo].[Location]([Name] ASC) WHERE ([Name]<>'');

