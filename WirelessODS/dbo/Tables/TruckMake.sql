﻿CREATE TABLE [dbo].[TruckMake] (
    [TruckMakeId]     INT          IDENTITY (1, 1) NOT NULL,
    [Name]            VARCHAR (50) NOT NULL,
    [CreatedAtUtc]    DATETIME     NOT NULL,
    [CreatedByUserId] INT          NOT NULL,
    CONSTRAINT [PKCX_TruckMake_TruckMakeId] PRIMARY KEY CLUSTERED ([TruckMakeId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_TruckMake_Name]
    ON [dbo].[TruckMake]([Name] ASC);

