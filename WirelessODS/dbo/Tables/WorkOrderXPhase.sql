﻿CREATE TABLE [dbo].[WorkOrderXPhase] (
    [WorkOrderXPhaseId] INT IDENTITY (1, 1) NOT NULL,
    [WorkOrderId]       INT NOT NULL,
    [WorkOrderPhaseId]  INT NOT NULL,
    CONSTRAINT [PKCX_WorkOrderXPhase_WorkOrderXPhaseId] PRIMARY KEY CLUSTERED ([WorkOrderXPhaseId] ASC),
    CONSTRAINT [FK_WorkOrderXPhase_WorkOrderId] FOREIGN KEY ([WorkOrderId]) REFERENCES [dbo].[WorkOrder] ([WorkOrderId]),
    CONSTRAINT [FK_WorkOrderXPhase_WorkOrderPhaseId] FOREIGN KEY ([WorkOrderPhaseId]) REFERENCES [dbo].[WorkOrderPhase] ([WorkOrderPhaseId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_WorkOrderXPhase_WorkOrderId_WorkOrderPhaseId]
    ON [dbo].[WorkOrderXPhase]([WorkOrderId] ASC, [WorkOrderPhaseId] ASC);

