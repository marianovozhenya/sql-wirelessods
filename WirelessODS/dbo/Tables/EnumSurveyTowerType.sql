﻿CREATE TABLE [dbo].[EnumSurveyTowerType] (
    [EnumSurveyTowerTypeId] INT           NOT NULL,
    [SystemName]            VARCHAR (50)  NOT NULL,
    [DisplayName]           VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumSurveyTowerType_EnumSurveyTowerTypeId] PRIMARY KEY CLUSTERED ([EnumSurveyTowerTypeId] ASC)
);

