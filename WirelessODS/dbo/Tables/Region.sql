﻿CREATE TABLE [dbo].[Region] (
    [RegionId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]     VARCHAR (100) NOT NULL,
    CONSTRAINT [PKCX_Region_RegionId] PRIMARY KEY CLUSTERED ([RegionId] ASC)
);

