﻿CREATE TABLE [dbo].[DeviceHistory] (
    [DeviceHistoryId]     INT           IDENTITY (1, 1) NOT NULL,
    [UpdatedAtUtc]        DATETIME      NOT NULL,
    [UpdatedByUserId]     INT           NOT NULL,
    [DeviceId]            INT           NOT NULL,
    [Name]                VARCHAR (256) NOT NULL,
    [CustomName]          VARCHAR (250) NOT NULL,
    [EquipmentId]         INT           NOT NULL,
    [EnumDeviceUseTypeId] INT           NOT NULL,
    [SiteId]              INT           NOT NULL,
    [IPv4Address]         BINARY (4)    NULL,
    [MacAddress]          BINARY (6)    NULL,
    [EnumDeviceStatusId]  INT           NOT NULL,
    [NorthBoundDeviceId]  INT           NULL,
    [WorkOrderId]         INT           NULL,
    [WirelessMacAddress]  BINARY (6)    NULL,
    CONSTRAINT [PKCX_DeviceHistory_DeviceHistoryId] PRIMARY KEY CLUSTERED ([DeviceHistoryId] ASC),
    CONSTRAINT [FK_DeviceHistory_WorkOrder] FOREIGN KEY ([WorkOrderId]) REFERENCES [dbo].[WorkOrder] ([WorkOrderId])
);


GO
CREATE NONCLUSTERED INDEX [NCX_DeviceHistory_DeviceId]
    ON [dbo].[DeviceHistory]([DeviceId] ASC);

