﻿CREATE TABLE [dbo].[Interface] (
    [InterfaceId]         INT          IDENTITY (1, 1) NOT NULL,
    [DeviceId]            INT          NOT NULL,
    [EnumInterfaceTypeId] INT          NOT NULL,
    [Name]                VARCHAR (50) NOT NULL,
    [MacAddress]          BINARY (6)   NOT NULL,
    [IPv4Address]         BINARY (4)   NULL,
    CONSTRAINT [PKCX_Interface_InterfaceId] PRIMARY KEY CLUSTERED ([InterfaceId] ASC),
    CONSTRAINT [FK_Interface_Device] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[Device] ([DeviceId]),
    CONSTRAINT [FK_Interface_EnumInterfaceType] FOREIGN KEY ([EnumInterfaceTypeId]) REFERENCES [dbo].[EnumInterfaceType] ([EnumInterfaceTypeId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_Interface_DeviceId_EnumInterfaceTypeId]
    ON [dbo].[Interface]([DeviceId] ASC, [EnumInterfaceTypeId] ASC);

