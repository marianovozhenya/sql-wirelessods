﻿CREATE TABLE [dbo].[WorkOrderResultCode] (
    [WorkOrderResultCodeId]   INT          IDENTITY (1, 1) NOT NULL,
    [Name]                    VARCHAR (50) NOT NULL,
    [IsNewWorkOrderCreated]   BIT          NOT NULL,
    [EnumWorkOrderTypeId]     INT          NULL,
    [WorkOrderSubTypeId]      INT          NULL,
    [DispatchCodeId]          INT          NULL,
    [EnumWorkOrderPriorityId] INT          NULL,
    CONSTRAINT [PKCX_WorkOrderResultCode_WorkOrderResultCodeId] PRIMARY KEY CLUSTERED ([WorkOrderResultCodeId] ASC),
    CONSTRAINT [FK_WorkOrderResultCode_DispatchCode] FOREIGN KEY ([DispatchCodeId]) REFERENCES [dbo].[DispatchCode] ([DispatchCodeId]),
    CONSTRAINT [FK_WorkOrderResultCode_EnumWorkOrderPriority] FOREIGN KEY ([EnumWorkOrderPriorityId]) REFERENCES [dbo].[EnumWorkOrderPriority] ([EnumWorkOrderPriorityId]),
    CONSTRAINT [FK_WorkOrderResultCode_EnumWorkOrderType] FOREIGN KEY ([EnumWorkOrderTypeId]) REFERENCES [dbo].[EnumWorkOrderType] ([EnumWorkOrderTypeId]),
    CONSTRAINT [FK_WorkOrderResultCode_WorkOrderSubType] FOREIGN KEY ([WorkOrderSubTypeId]) REFERENCES [dbo].[WorkOrderSubType] ([WorkOrderSubTypeId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_WorkOrderResultCode_Name]
    ON [dbo].[WorkOrderResultCode]([Name] ASC);

