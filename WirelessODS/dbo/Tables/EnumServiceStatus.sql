﻿CREATE TABLE [dbo].[EnumServiceStatus] (
    [EnumServiceStatusId] INT           NOT NULL,
    [SystemName]          VARCHAR (50)  NOT NULL,
    [DisplayName]         VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumServiceStatus_EnumServiceStatusId] PRIMARY KEY CLUSTERED ([EnumServiceStatusId] ASC)
);

