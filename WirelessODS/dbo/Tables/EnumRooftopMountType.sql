﻿CREATE TABLE [dbo].[EnumRooftopMountType] (
    [EnumRooftopMountTypeId] INT           NOT NULL,
    [SystemName]             VARCHAR (50)  NOT NULL,
    [DisplayName]            VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumRooftopMountType_EnumRooftopMountTypeId] PRIMARY KEY CLUSTERED ([EnumRooftopMountTypeId] ASC)
);

