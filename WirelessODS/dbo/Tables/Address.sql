﻿CREATE TABLE [dbo].[Address] (
    [AddressId]                      INT           IDENTITY (1, 1) NOT NULL,
    [BaseAddressId]                  INT           NOT NULL,
    [SuiteType]                      VARCHAR (20)  NULL,
    [SuiteNumber]                    VARCHAR (20)  NULL,
    [BoxType]                        VARCHAR (20)  NULL,
    [BoxNumber]                      VARCHAR (20)  NULL,
    [AddressFull]                    VARCHAR (MAX) NULL,
    [EnumAddressValidationServiceId] INT           NOT NULL,
    [ValidationDate]                 DATE          NULL,
    [ValidationKey]                  VARCHAR (100) NULL,
    [AddressLine]                    VARCHAR (MAX) NULL,
    [CompositeKey]                   VARCHAR (50)  NOT NULL,
    CONSTRAINT [PKCX_Address_AddressId] PRIMARY KEY CLUSTERED ([AddressId] ASC),
    CONSTRAINT [FK_Address_BaseAddress] FOREIGN KEY ([BaseAddressId]) REFERENCES [dbo].[BaseAddress] ([BaseAddressId]),
    CONSTRAINT [FK_Address_EnumAddressValidationService] FOREIGN KEY ([EnumAddressValidationServiceId]) REFERENCES [dbo].[EnumAddressValidationService] ([EnumAddressValidationServiceId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_Address_CompositeKey]
    ON [dbo].[Address]([AddressId] ASC);

