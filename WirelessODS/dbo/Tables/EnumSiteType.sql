﻿CREATE TABLE [dbo].[EnumSiteType] (
    [EnumSiteTypeId] INT           NOT NULL,
    [SystemName]     VARCHAR (50)  NOT NULL,
    [DisplayName]    VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumSiteType_EnumSiteTypeId] PRIMARY KEY CLUSTERED ([EnumSiteTypeId] ASC)
);

