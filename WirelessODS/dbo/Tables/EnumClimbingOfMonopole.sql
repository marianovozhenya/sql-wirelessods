﻿CREATE TABLE [dbo].[EnumClimbingOfMonopole] (
    [EnumClimbingOfMonopoleId] INT           NOT NULL,
    [SystemName]               VARCHAR (50)  NOT NULL,
    [DisplayName]              VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumClimbingOfMonopole_EnumClimbingOfMonopoleId] PRIMARY KEY CLUSTERED ([EnumClimbingOfMonopoleId] ASC)
);

