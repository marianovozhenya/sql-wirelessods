﻿CREATE TABLE [dbo].[EnumUserRoleType] (
    [EnumUserRoleTypeId] INT           NOT NULL,
    [SystemName]         VARCHAR (50)  NOT NULL,
    [DisplayName]        VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumUserRoleType_EnumUserRoleTypeId] PRIMARY KEY CLUSTERED ([EnumUserRoleTypeId] ASC)
);

