﻿CREATE TABLE [dbo].[AttachmentType] (
    [AttachmentTypeId]    INT           IDENTITY (1, 1) NOT NULL,
    [Name]                VARCHAR (100) NOT NULL,
    [CreatedAtUtc]        DATETIME      NOT NULL,
    [CreatedByUserId]     INT           NOT NULL,
    [LastUpdatedAtUtc]    DATETIME      NULL,
    [LastUpdatedByUserId] INT           NULL,
    CONSTRAINT [PKCX_AttachmentType_AttachmentTypeId] PRIMARY KEY CLUSTERED ([AttachmentTypeId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_AttachmentType_Name]
    ON [dbo].[AttachmentType]([Name] ASC);

