﻿CREATE TABLE [dbo].[EnumAccessOfCable] (
    [EnumAccessOfCableId] INT           NOT NULL,
    [SystemName]          VARCHAR (50)  NOT NULL,
    [DisplayName]         VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumAccessOfCable_EnumAccessOfCableId] PRIMARY KEY CLUSTERED ([EnumAccessOfCableId] ASC)
);

