﻿CREATE TABLE [dbo].[EnumLandZoned] (
    [EnumLandZonedId] INT           NOT NULL,
    [SystemName]      VARCHAR (50)  NOT NULL,
    [DisplayName]     VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumLandZoned_EnumLandZonedId] PRIMARY KEY CLUSTERED ([EnumLandZonedId] ASC)
);

