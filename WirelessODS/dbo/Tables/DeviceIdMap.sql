﻿CREATE TABLE [dbo].[DeviceIdMap] (
    [DeviceId]             INT         NOT NULL,
    [StringRepresentation] VARCHAR (5) NOT NULL,
    CONSTRAINT [PKCX_DeviceIdMap_DeviceId] PRIMARY KEY CLUSTERED ([DeviceId] ASC)
);

