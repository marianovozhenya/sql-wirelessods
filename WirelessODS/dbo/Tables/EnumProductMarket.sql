﻿CREATE TABLE [dbo].[EnumProductMarket] (
    [EnumProductMarketId] INT           NOT NULL,
    [SystemName]          VARCHAR (50)  NOT NULL,
    [DisplayName]         VARCHAR (255) NOT NULL,
    CONSTRAINT [PK_EnumProductMarket] PRIMARY KEY CLUSTERED ([EnumProductMarketId] ASC)
);

