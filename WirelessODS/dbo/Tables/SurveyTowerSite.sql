﻿CREATE TABLE [dbo].[SurveyTowerSite] (
    [SurveyTowerSiteId]          INT            IDENTITY (1, 1) NOT NULL,
    [SurveyId]                   INT            NOT NULL,
    [StructureHeight]            DECIMAL (7, 2) NULL,
    [ProposedTowerHeight]        DECIMAL (7, 2) NULL,
    [GroundTerrainOther]         VARCHAR (100)  NOT NULL,
    [Notes]                      VARCHAR (MAX)  NOT NULL,
    [EnumInsideGroundTerrainId]  INT            NULL,
    [EnumOutsideGroundTerrainId] INT            NULL,
    [EnumSurveyTowerTypeId]      INT            NULL,
    [EnumSurveyTowerStatusId]    INT            NULL,
    [EnumStructureTypeId]        INT            NULL,
    CONSTRAINT [PKCX_SurveyTowerSite_SurveyTowerSiteId] PRIMARY KEY CLUSTERED ([SurveyTowerSiteId] ASC),
    CONSTRAINT [FK_SurveyTowerSite_EnumInsideGroundTerrain] FOREIGN KEY ([EnumInsideGroundTerrainId]) REFERENCES [dbo].[EnumGroundTerrain] ([EnumGroundTerrainId]),
    CONSTRAINT [FK_SurveyTowerSite_EnumOutsideGroundTerrain] FOREIGN KEY ([EnumOutsideGroundTerrainId]) REFERENCES [dbo].[EnumGroundTerrain] ([EnumGroundTerrainId]),
    CONSTRAINT [FK_SurveyTowerSite_EnumStructureType] FOREIGN KEY ([EnumStructureTypeId]) REFERENCES [dbo].[EnumStructureType] ([EnumStructureTypeId]),
    CONSTRAINT [FK_SurveyTowerSite_EnumSurveyTowerStatus] FOREIGN KEY ([EnumSurveyTowerStatusId]) REFERENCES [dbo].[EnumSurveyTowerStatus] ([EnumSurveyTowerStatusId]),
    CONSTRAINT [FK_SurveyTowerSite_EnumSurveyTowerType] FOREIGN KEY ([EnumSurveyTowerTypeId]) REFERENCES [dbo].[EnumSurveyTowerType] ([EnumSurveyTowerTypeId]),
    CONSTRAINT [FK_SurveyTowerSite_Survey] FOREIGN KEY ([SurveyId]) REFERENCES [dbo].[Survey] ([SurveyId])
);

