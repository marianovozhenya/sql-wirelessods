﻿CREATE TABLE [dbo].[EnumRoadType] (
    [EnumRoadTypeId] INT           NOT NULL,
    [SystemName]     VARCHAR (50)  NOT NULL,
    [DisplayName]    VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumRoadType_EnumRoadTypeId] PRIMARY KEY CLUSTERED ([EnumRoadTypeId] ASC)
);

