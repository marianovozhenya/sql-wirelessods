﻿CREATE TABLE [dbo].[EquipmentBarcode] (
    [EquipmentBarcodeId]         INT          IDENTITY (1, 1) NOT NULL,
    [Barcode]                    VARCHAR (50) NOT NULL,
    [EquipmentId]                INT          NOT NULL,
    [EnumEquipmentBarcodeTypeId] INT          NOT NULL,
    CONSTRAINT [PKCX_EquipmentBarcode_EquipmentBarcodeId] PRIMARY KEY CLUSTERED ([EquipmentBarcodeId] ASC),
    CONSTRAINT [FK_EquipmentBarcode_EnumEquipmentBarcodeType] FOREIGN KEY ([EnumEquipmentBarcodeTypeId]) REFERENCES [dbo].[EnumEquipmentBarcodeType] ([EnumEquipmentBarcodeTypeId]),
    CONSTRAINT [FK_EquipmentBarcode_Equipment] FOREIGN KEY ([EquipmentId]) REFERENCES [dbo].[Equipment] ([EquipmentId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_EquipmentBarcode_Barcode]
    ON [dbo].[EquipmentBarcode]([Barcode] ASC);

