﻿CREATE TABLE [dbo].[EnumSurveyStatus] (
    [EnumSurveyStatusId] INT           NOT NULL,
    [SystemName]         VARCHAR (50)  NOT NULL,
    [DisplayName]        VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumSurveyStatus_EnumSurveyStatusId] PRIMARY KEY CLUSTERED ([EnumSurveyStatusId] ASC)
);

