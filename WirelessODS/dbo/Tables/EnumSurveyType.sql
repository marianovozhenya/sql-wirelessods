﻿CREATE TABLE [dbo].[EnumSurveyType] (
    [EnumSurveyTypeId] INT           NOT NULL,
    [SystemName]       VARCHAR (50)  NOT NULL,
    [DisplayName]      VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumSurveyType_EnumSurveyTypeId] PRIMARY KEY CLUSTERED ([EnumSurveyTypeId] ASC)
);

