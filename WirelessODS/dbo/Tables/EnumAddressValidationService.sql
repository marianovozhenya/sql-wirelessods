﻿CREATE TABLE [dbo].[EnumAddressValidationService] (
    [EnumAddressValidationServiceId] INT           NOT NULL,
    [SystemName]                     VARCHAR (50)  NOT NULL,
    [DisplayName]                    VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumAddressValidationService_EnumAddressValidationServiceId] PRIMARY KEY CLUSTERED ([EnumAddressValidationServiceId] ASC)
);

