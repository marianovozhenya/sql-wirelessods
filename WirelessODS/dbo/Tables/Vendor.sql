﻿CREATE TABLE [dbo].[Vendor] (
    [VendorId]        INT           IDENTITY (1, 1) NOT NULL,
    [Name]            VARCHAR (100) NOT NULL,
    [CreatedAtUtc]    DATETIME      NOT NULL,
    [CreatedByUserId] INT           NOT NULL,
    CONSTRAINT [PKCX_Vendor_VendorId] PRIMARY KEY CLUSTERED ([VendorId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_Vendor_Name]
    ON [dbo].[Vendor]([Name] ASC);

