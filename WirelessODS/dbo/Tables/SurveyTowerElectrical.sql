﻿CREATE TABLE [dbo].[SurveyTowerElectrical] (
    [SurveyTowerElectricalId]     INT            IDENTITY (1, 1) NOT NULL,
    [SurveyId]                    INT            NOT NULL,
    [CanSharePower]               BIT            NULL,
    [IsMeterBank]                 BIT            NULL,
    [IsSpaceToAddMeter]           BIT            NULL,
    [IsExistingMeterNumber]       BIT            NULL,
    [LengthFromMeterToNewCabinet] DECIMAL (7, 2) NULL,
    [ServiceProvider]             VARCHAR (100)  NOT NULL,
    [ServiceContact]              VARCHAR (100)  NOT NULL,
    [IsPermitRequiredForSite]     BIT            NULL,
    [EstimatedCost]               SMALLMONEY     NULL,
    [Notes]                       VARCHAR (MAX)  NOT NULL,
    [EnumMainPowerFeedId]         INT            NULL,
    [EnumEnclosureId]             INT            NULL,
    CONSTRAINT [PKCX_SurveyTowerElectrical_SurveyTowerElectricalId] PRIMARY KEY CLUSTERED ([SurveyTowerElectricalId] ASC),
    CONSTRAINT [FK_SurveyTowerElectrical_EnumEnclosure] FOREIGN KEY ([EnumEnclosureId]) REFERENCES [dbo].[EnumEnclosure] ([EnumEnclosureId]),
    CONSTRAINT [FK_SurveyTowerElectrical_EnumMainPowerFeed] FOREIGN KEY ([EnumMainPowerFeedId]) REFERENCES [dbo].[EnumMainPowerFeed] ([EnumMainPowerFeedId]),
    CONSTRAINT [FK_SurveyTowerElectrical_Survey] FOREIGN KEY ([SurveyId]) REFERENCES [dbo].[Survey] ([SurveyId])
);

