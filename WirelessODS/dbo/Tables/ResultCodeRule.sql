﻿CREATE TABLE [dbo].[ResultCodeRule] (
    [ResultCodeRuleId]         INT          NOT NULL,
    [EnumResultCodeRuleTypeId] INT          NOT NULL,
    [Name]                     VARCHAR (50) NOT NULL,
    CONSTRAINT [PKCX_ResultCodeRule_ResultCodeRuleId] PRIMARY KEY CLUSTERED ([ResultCodeRuleId] ASC),
    CONSTRAINT [FK_ResultCodeRule_EnumResultCodeRuleType] FOREIGN KEY ([EnumResultCodeRuleTypeId]) REFERENCES [dbo].[EnumResultCodeRuleType] ([EnumResultCodeRuleTypeId])
);

