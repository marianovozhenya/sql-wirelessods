﻿CREATE TABLE [dbo].[WorkOrderBom] (
    [WorkOrderBomId]  INT IDENTITY (1, 1) NOT NULL,
    [WorkOrderId]     INT NOT NULL,
    [EquipmentId]     INT NOT NULL,
    [Quantity]        INT NOT NULL,
    [Pulled]          INT NOT NULL,
    [IsInventoryShip] BIT NOT NULL,
    [DeviceCount]     INT NOT NULL,
    CONSTRAINT [PKCX_WorkOrderBom_WorkOrderBomId] PRIMARY KEY CLUSTERED ([WorkOrderBomId] ASC),
    CONSTRAINT [FK_WorkOrderBom_Equipment] FOREIGN KEY ([EquipmentId]) REFERENCES [dbo].[Equipment] ([EquipmentId]),
    CONSTRAINT [FK_WorkOrderBom_WorkOrder] FOREIGN KEY ([WorkOrderId]) REFERENCES [dbo].[WorkOrder] ([WorkOrderId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_WorkOrderBom_WorkOrderId]
    ON [dbo].[WorkOrderBom]([WorkOrderId] ASC, [EquipmentId] ASC);

