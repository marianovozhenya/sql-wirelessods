﻿CREATE TABLE [dbo].[WorkOrderInventoryTransaction] (
    [WorkOrderInventoryTransactionId] INT           IDENTITY (1, 1) NOT NULL,
    [WorkOrderId]                     INT           NOT NULL,
    [EquipmentId]                     INT           NOT NULL,
    [Quantity]                        INT           NOT NULL,
    [PerformedAtUtc]                  DATETIME      NOT NULL,
    [PerformedByUserId]               INT           NOT NULL,
    [RDARRB]                          VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_WorkOrderInventoryTransaction_WorkOrderInventoryTransactionId] PRIMARY KEY CLUSTERED ([WorkOrderInventoryTransactionId] ASC),
    CONSTRAINT [FK_WorkOrderInventoryTransaction_Equipment] FOREIGN KEY ([EquipmentId]) REFERENCES [dbo].[Equipment] ([EquipmentId]),
    CONSTRAINT [FK_WorkOrderInventoryTransaction_WorkOrder] FOREIGN KEY ([WorkOrderId]) REFERENCES [dbo].[WorkOrder] ([WorkOrderId])
);


GO
CREATE NONCLUSTERED INDEX [NCX_WorkOrderInventoryTransaction_WorkOrderId_EquipmentId]
    ON [dbo].[WorkOrderInventoryTransaction]([WorkOrderId] ASC, [EquipmentId] ASC);

