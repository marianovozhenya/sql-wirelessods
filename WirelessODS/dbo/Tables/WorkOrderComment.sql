﻿CREATE TABLE [dbo].[WorkOrderComment] (
    [WorkOrderCommentId]             INT           IDENTITY (1, 1) NOT NULL,
    [WorkOrderId]                    INT           NOT NULL,
    [EnumWorkOrderCommentTypeId]     INT           NOT NULL,
    [Rating]                         INT           NULL,
    [EnumWorkOrderCommentPriorityId] INT           NOT NULL,
    [Comment]                        VARCHAR (MAX) NOT NULL,
    [CreatedByUserId]                INT           NOT NULL,
    [CreatedAtUtc]                   DATETIME      NOT NULL,
    CONSTRAINT [PKCX_WorkOrderComment_WorkOrderCommentId] PRIMARY KEY CLUSTERED ([WorkOrderCommentId] ASC),
    CONSTRAINT [FK_WorkOrderComment_EnumWorkOrderCommentPriority] FOREIGN KEY ([EnumWorkOrderCommentPriorityId]) REFERENCES [dbo].[EnumWorkOrderCommentPriority] ([EnumWorkOrderCommentPriorityId]),
    CONSTRAINT [FK_WorkOrderComment_EnumWorkOrderCommentType] FOREIGN KEY ([EnumWorkOrderCommentTypeId]) REFERENCES [dbo].[EnumWorkOrderCommentType] ([EnumWorkOrderCommentTypeId]),
    CONSTRAINT [FK_WorkOrderComment_WorkOrderId] FOREIGN KEY ([WorkOrderId]) REFERENCES [dbo].[WorkOrder] ([WorkOrderId])
);


GO
CREATE NONCLUSTERED INDEX [NCX_WorkOrderComment_WorkOrderId]
    ON [dbo].[WorkOrderComment]([WorkOrderId] ASC);

