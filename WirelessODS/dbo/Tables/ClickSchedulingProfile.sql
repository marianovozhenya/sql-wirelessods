﻿CREATE TABLE [dbo].[ClickSchedulingProfile] (
    [ClickSchedulingProfileId] INT           IDENTITY (1, 1) NOT NULL,
    [ProfileName]              VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_ClickSchedulingProfile_ClickSchedulingProfileId] PRIMARY KEY CLUSTERED ([ClickSchedulingProfileId] ASC)
);

