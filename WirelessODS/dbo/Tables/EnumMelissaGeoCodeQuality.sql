﻿CREATE TABLE [dbo].[EnumMelissaGeoCodeQuality] (
    [EnumMelissaGeoCodeQualityId] INT           NOT NULL,
    [SystemName]                  VARCHAR (50)  NOT NULL,
    [DisplayName]                 VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumMelissaGeoCodeQuality_EnumMelissaGeoCodeQualityId] PRIMARY KEY CLUSTERED ([EnumMelissaGeoCodeQualityId] ASC)
);

