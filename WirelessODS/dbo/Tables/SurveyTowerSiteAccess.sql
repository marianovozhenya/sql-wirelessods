﻿CREATE TABLE [dbo].[SurveyTowerSiteAccess] (
    [SurveyTowerSiteAccessId]        INT           IDENTITY (1, 1) NOT NULL,
    [SurveyId]                       INT           NOT NULL,
    [IsLandlordNotificationRequired] BIT           NULL,
    [IsMultipleGates]                BIT           NULL,
    [IsLivestockRoaming]             BIT           NULL,
    [EnumRoadTypeId]                 INT           NULL,
    [EnumRoadConditionId]            INT           NULL,
    [AccessRoadOther]                VARCHAR (100) NOT NULL,
    [AdditionalAccessInformation]    VARCHAR (MAX) NOT NULL,
    [GateCombo]                      VARCHAR (10)  NOT NULL,
    CONSTRAINT [PKCX_SurveyTowerSiteAccess_SurveyTowerSiteAccessId] PRIMARY KEY CLUSTERED ([SurveyTowerSiteAccessId] ASC),
    CONSTRAINT [FK_SurveyTowerSiteAccess_EnumRoadCondition] FOREIGN KEY ([EnumRoadConditionId]) REFERENCES [dbo].[EnumRoadCondition] ([EnumRoadConditionId]),
    CONSTRAINT [FK_SurveyTowerSiteAccess_EnumRoadType] FOREIGN KEY ([EnumRoadTypeId]) REFERENCES [dbo].[EnumRoadType] ([EnumRoadTypeId]),
    CONSTRAINT [FK_SurveyTowerSiteAccess_Survey] FOREIGN KEY ([SurveyId]) REFERENCES [dbo].[Survey] ([SurveyId])
);

