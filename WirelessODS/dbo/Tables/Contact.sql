﻿CREATE TABLE [dbo].[Contact] (
    [ContactId]       INT           IDENTITY (1, 1) NOT NULL,
    [FirstName]       VARCHAR (50)  NOT NULL,
    [LastName]        VARCHAR (50)  NOT NULL,
    [EnumPhoneTypeId] INT           NOT NULL,
    [PhoneNumber]     BIGINT        NOT NULL,
    [EmailAddress]    VARCHAR (255) NOT NULL,
    [CreatedAtUtc]    DATETIME      NOT NULL,
    [CreatedByUserId] INT           NOT NULL,
    CONSTRAINT [PKCX_Contact_ContactId] PRIMARY KEY CLUSTERED ([ContactId] ASC),
    CONSTRAINT [FK_Contact_EnumPhoneType] FOREIGN KEY ([EnumPhoneTypeId]) REFERENCES [dbo].[EnumPhoneType] ([EnumPhoneTypeId])
);


GO
CREATE NONCLUSTERED INDEX [NCX_Contact_PhoneNumber]
    ON [dbo].[Contact]([PhoneNumber] ASC);


GO
CREATE NONCLUSTERED INDEX [NCX_Contact_FirstName_LastName]
    ON [dbo].[Contact]([FirstName] ASC, [LastName] ASC);

