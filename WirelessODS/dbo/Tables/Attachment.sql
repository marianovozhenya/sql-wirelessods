﻿CREATE TABLE [dbo].[Attachment] (
    [AttachmentId]       INT           IDENTITY (1, 1) NOT NULL,
    [Name]               VARCHAR (100) NOT NULL,
    [AttachmentTypeId]   INT           NOT NULL,
    [S3Bucket]           VARCHAR (100) NOT NULL,
    [S3Location]         VARCHAR (MAX) NOT NULL,
    [CreatedAtUtc]       DATETIME      NOT NULL,
    [CreatedByUserId]    INT           NOT NULL,
    [LocationId]         INT           NOT NULL,
    [SiteId]             INT           NULL,
    [WorkOrderId]        INT           NULL,
    [IsSoftDeleted]      BIT           NOT NULL,
    [DeletedAtUtc]       DATETIME      NULL,
    [EnumUploadStatusId] INT           NOT NULL,
    CONSTRAINT [PKCX_Attachment_AttachmentId] PRIMARY KEY CLUSTERED ([AttachmentId] ASC),
    CONSTRAINT [FK_Attachment_AttachmentType] FOREIGN KEY ([AttachmentTypeId]) REFERENCES [dbo].[AttachmentType] ([AttachmentTypeId]),
    CONSTRAINT [FK_Attachment_EnumUploadStatus] FOREIGN KEY ([EnumUploadStatusId]) REFERENCES [dbo].[EnumUploadStatus] ([EnumUploadStatusId]),
    CONSTRAINT [FK_Attachment_Location] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([LocationId]),
    CONSTRAINT [FK_Attachment_Site] FOREIGN KEY ([SiteId]) REFERENCES [dbo].[Site] ([SiteId]),
    CONSTRAINT [FK_Attachment_WorkOrder] FOREIGN KEY ([WorkOrderId]) REFERENCES [dbo].[WorkOrder] ([WorkOrderId])
);


GO
CREATE NONCLUSTERED INDEX [NCX_Attachment_SiteId]
    ON [dbo].[Attachment]([SiteId] ASC);


GO
CREATE NONCLUSTERED INDEX [NCX_Attachment_LocationId]
    ON [dbo].[Attachment]([LocationId] ASC);


GO
CREATE NONCLUSTERED INDEX [NCX_Attachment_AttachmentTypeId]
    ON [dbo].[Attachment]([AttachmentTypeId] ASC);

