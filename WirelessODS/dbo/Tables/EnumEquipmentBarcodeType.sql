﻿CREATE TABLE [dbo].[EnumEquipmentBarcodeType] (
    [EnumEquipmentBarcodeTypeId] INT           NOT NULL,
    [SystemName]                 VARCHAR (50)  NOT NULL,
    [DisplayName]                VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumEquipmentBarcodeType_EnumEquipmentBarcodeTypeId] PRIMARY KEY CLUSTERED ([EnumEquipmentBarcodeTypeId] ASC)
);

