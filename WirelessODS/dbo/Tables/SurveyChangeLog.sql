﻿CREATE TABLE [dbo].[SurveyChangeLog] (
    [SurveyChangeLogId] INT           IDENTITY (1, 1) NOT NULL,
    [SiteId]            INT           NOT NULL,
    [Name]              VARCHAR (100) NOT NULL,
    [Value]             VARCHAR (100) NOT NULL,
    [CreatedAtUtc]      DATETIME      NOT NULL,
    [CreatedByUserId]   INT           NOT NULL,
    [WorkOrderId]       INT           NULL,
    CONSTRAINT [PKCX_SurveyChangeLog_SurveyChangeLogId] PRIMARY KEY CLUSTERED ([SurveyChangeLogId] ASC),
    CONSTRAINT [FK_SurveyChangeLog_Site] FOREIGN KEY ([SiteId]) REFERENCES [dbo].[Site] ([SiteId]),
    CONSTRAINT [FK_SurveyChangeLog_WorkOrder] FOREIGN KEY ([WorkOrderId]) REFERENCES [dbo].[WorkOrder] ([WorkOrderId])
);

