﻿CREATE TABLE [dbo].[Customer] (
    [CustomerId]                                INT           IDENTITY (1, 1) NOT NULL,
    [AzotelId]                                  INT           NULL,
    [BillingAddressId]                          INT           NULL,
    [PrivateNotes]                              VARCHAR (MAX) NOT NULL,
    [CreatedInAzotelAtUtc]                      DATETIME      NULL,
    [AzotelAuthorizeNetToken]                   VARCHAR (100) NOT NULL,
    [AzotelAuthorizeNetTokenAddedToAzotelAtUtc] DATETIME      NULL,
    [EnumCustomerStatusId]                      INT           CONSTRAINT [DEF_Customer_EnumCustomerStatusId] DEFAULT ((0)) NOT NULL,
    [CreatedAtUtc]                              DATETIME      NOT NULL,
    [CreatedByUserId]                           INT           NOT NULL,
    CONSTRAINT [PKCX_Customer_CustomerId] PRIMARY KEY CLUSTERED ([CustomerId] ASC),
    CONSTRAINT [FK_Customer_Address_Billing] FOREIGN KEY ([BillingAddressId]) REFERENCES [dbo].[Address] ([AddressId]),
    CONSTRAINT [FK_Customer_EnumCustomerStatus] FOREIGN KEY ([EnumCustomerStatusId]) REFERENCES [dbo].[EnumCustomerStatus] ([EnumCustomerStatusId])
);


GO
CREATE NONCLUSTERED INDEX [NCX_Customer_BillingAddressId]
    ON [dbo].[Customer]([BillingAddressId] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_Customer_AzotelId]
    ON [dbo].[Customer]([AzotelId] ASC) WHERE ([AzotelId] IS NOT NULL);

