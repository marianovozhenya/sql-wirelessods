﻿CREATE TABLE [dbo].[EnumPhoneType] (
    [EnumPhoneTypeId] INT           NOT NULL,
    [SystemName]      VARCHAR (50)  NOT NULL,
    [DisplayName]     VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumPhoneType_EnumPhoneTypeId] PRIMARY KEY CLUSTERED ([EnumPhoneTypeId] ASC)
);

