﻿CREATE TABLE [dbo].[WorkOrderNote] (
    [WorkOrderNoteId] INT           IDENTITY (1, 1) NOT NULL,
    [WorkOrderId]     INT           NOT NULL,
    [CreatedByUserId] INT           NOT NULL,
    [CreatedAtUtc]    DATETIME      NOT NULL,
    [Note]            VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PKCX_WorkOrderNote_WorkOrderNoteId] PRIMARY KEY CLUSTERED ([WorkOrderNoteId] ASC),
    CONSTRAINT [FK_WorkOrderNote_WorkOrder] FOREIGN KEY ([WorkOrderId]) REFERENCES [dbo].[WorkOrder] ([WorkOrderId])
);


GO
CREATE NONCLUSTERED INDEX [NCX_WorkOrderNote_WorkOrder]
    ON [dbo].[WorkOrderNote]([WorkOrderId] ASC);

