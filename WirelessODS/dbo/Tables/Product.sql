﻿CREATE TABLE [dbo].[Product] (
    [ProductId]             INT           IDENTITY (1, 1) NOT NULL,
    [AzotelSubscriptionId]  INT           NOT NULL,
    [Price]                 SMALLMONEY    NOT NULL,
    [Description]           VARCHAR (255) NOT NULL,
    [EnumProductCategoryId] INT           NOT NULL,
    [IsInstallPlusOne]      BIT           NOT NULL,
    [EnumProductMarketId]   INT           NOT NULL,
    [DisplayOrder]          INT           NULL,
    CONSTRAINT [PKCX_Product_ProductId] PRIMARY KEY CLUSTERED ([ProductId] ASC),
    CONSTRAINT [FK_Product_EnumProductCategory] FOREIGN KEY ([EnumProductCategoryId]) REFERENCES [dbo].[EnumProductCategory] ([EnumProductCategoryId])
);

