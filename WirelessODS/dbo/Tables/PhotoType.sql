﻿CREATE TABLE [dbo].[PhotoType] (
    [PhotoTypeId]     INT           IDENTITY (1, 1) NOT NULL,
    [Name]            VARCHAR (100) NOT NULL,
    [CreatedAtUtc]    DATETIME      NOT NULL,
    [CreatedByUserId] INT           NOT NULL,
    CONSTRAINT [PKCX_PhotoType_PhotoTypeId] PRIMARY KEY CLUSTERED ([PhotoTypeId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_PhotoType_Name]
    ON [dbo].[PhotoType]([Name] ASC);

