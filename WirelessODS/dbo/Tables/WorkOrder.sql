﻿CREATE TABLE [dbo].[WorkOrder] (
    [WorkOrderId]               INT           IDENTITY (1, 1) NOT NULL,
    [EnumWorkOrderTypeId]       INT           NOT NULL,
    [WorkOrderSubTypeId]        INT           NOT NULL,
    [DispatchCodeId]            INT           NOT NULL,
    [EnumWorkOrderPriorityId]   INT           NOT NULL,
    [EnumWorkOrderStatusId]     INT           NOT NULL,
    [ContactName]               VARCHAR (100) NOT NULL,
    [ContactPhone]              VARCHAR (20)  NOT NULL,
    [SiteId]                    INT           NULL,
    [CreatedAtUtc]              DATETIME      NOT NULL,
    [CreatedByUserId]           INT           NOT NULL,
    [LastUpdatedAtUtc]          DATETIME      NULL,
    [LastUpdatedByUserId]       INT           NULL,
    [WorkOrderResultCodeId]     INT           NULL,
    [CreatedFromWorkOrderId]    INT           NULL,
    [IsClosed]                  BIT           CONSTRAINT [DF_WorkOrder_IsClosed] DEFAULT ((0)) NOT NULL,
    [WorkOrderNumber]           VARCHAR (20)  NULL,
    [IsScheduled]               BIT           NOT NULL,
    [AddressId]                 INT           NOT NULL,
    [CustomerId]                INT           NULL,
    [ServiceId]                 INT           NULL,
    [ClickAssignedUserId]       INT           NULL,
    [ClickAssignedCrewId]       INT           NULL,
    [TravelSentToClickAtUtc]    DATETIME      NULL,
    [OnsiteSentToClickAtUtc]    DATETIME      NULL,
    [CompletedSentToClickAtUtc] DATETIME      NULL,
    [LocationId]                INT           CONSTRAINT [DF_WorkOrder_LocationId] DEFAULT ((1)) NOT NULL,
    [AppointmentWindowStart]    DATETIME      NULL,
    [AppointmentWindowEnd]      DATETIME      NULL,
    CONSTRAINT [PKCX_WorkOrder_WorkOrderId] PRIMARY KEY CLUSTERED ([WorkOrderId] ASC),
    CONSTRAINT [FK_WorkOrder_AddressId] FOREIGN KEY ([AddressId]) REFERENCES [dbo].[Address] ([AddressId]),
    CONSTRAINT [FK_WorkOrder_ClickAssignedCrewId] FOREIGN KEY ([ClickAssignedCrewId]) REFERENCES [dbo].[Crew] ([CrewId]),
    CONSTRAINT [FK_WorkOrder_ClickAssignedUserId] FOREIGN KEY ([ClickAssignedUserId]) REFERENCES [dbo].[User] ([UserId]),
    CONSTRAINT [FK_WorkOrder_CreatedFromWorkOrderId] FOREIGN KEY ([CreatedFromWorkOrderId]) REFERENCES [dbo].[WorkOrder] ([WorkOrderId]),
    CONSTRAINT [FK_WorkOrder_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId]),
    CONSTRAINT [FK_WorkOrder_DispatchCode] FOREIGN KEY ([DispatchCodeId]) REFERENCES [dbo].[DispatchCode] ([DispatchCodeId]),
    CONSTRAINT [FK_WorkOrder_EnumWorkOrderPriority] FOREIGN KEY ([EnumWorkOrderPriorityId]) REFERENCES [dbo].[EnumWorkOrderPriority] ([EnumWorkOrderPriorityId]),
    CONSTRAINT [FK_WorkOrder_EnumWorkOrderStatus] FOREIGN KEY ([EnumWorkOrderStatusId]) REFERENCES [dbo].[EnumWorkOrderStatus] ([EnumWorkOrderStatusId]),
    CONSTRAINT [FK_WorkOrder_EnumWorkOrderType] FOREIGN KEY ([EnumWorkOrderTypeId]) REFERENCES [dbo].[EnumWorkOrderType] ([EnumWorkOrderTypeId]),
    CONSTRAINT [FK_WorkOrder_Location] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([LocationId]),
    CONSTRAINT [FK_WorkOrder_Service] FOREIGN KEY ([ServiceId]) REFERENCES [dbo].[Service] ([ServiceId]),
    CONSTRAINT [FK_WorkOrder_Site] FOREIGN KEY ([SiteId]) REFERENCES [dbo].[Site] ([SiteId]),
    CONSTRAINT [FK_WorkOrder_WorkOrderResultCode] FOREIGN KEY ([WorkOrderResultCodeId]) REFERENCES [dbo].[WorkOrderResultCode] ([WorkOrderResultCodeId]),
    CONSTRAINT [FK_WorkOrder_WorkOrderSubType] FOREIGN KEY ([WorkOrderSubTypeId]) REFERENCES [dbo].[WorkOrderSubType] ([WorkOrderSubTypeId])
);


GO
ALTER TABLE [dbo].[WorkOrder] NOCHECK CONSTRAINT [FK_WorkOrder_Location];


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_WorkOrder_WorkOrderNumber]
    ON [dbo].[WorkOrder]([WorkOrderNumber] ASC) WHERE ([WorkOrderNumber] IS NOT NULL);

