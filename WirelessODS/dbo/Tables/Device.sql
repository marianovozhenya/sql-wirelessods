﻿CREATE TABLE [dbo].[Device] (
    [DeviceId]            INT           IDENTITY (1, 1) NOT NULL,
    [Name]                VARCHAR (256) NULL,
    [CustomName]          VARCHAR (250) NOT NULL,
    [EquipmentId]         INT           NOT NULL,
    [EnumDeviceUseTypeId] INT           NOT NULL,
    [SiteId]              INT           NOT NULL,
    [EnumDeviceStatusId]  INT           NOT NULL,
    [CreatedAtUtc]        DATETIME      NOT NULL,
    [CreatedByUserId]     INT           NOT NULL,
    [LastUpdatedAtUtc]    DATETIME      NULL,
    [LastUpdatedByUserId] INT           NULL,
    [NorthBoundDeviceId]  INT           NULL,
    [WorkOrderId]         INT           NULL,
    [WirelessMacAddress]  BINARY (6)    NULL,
    CONSTRAINT [PKCX_Device_DeviceId] PRIMARY KEY CLUSTERED ([DeviceId] ASC),
    CONSTRAINT [FK_Device_DeviceIdMap_ThisIsOnPurpose] FOREIGN KEY ([DeviceId]) REFERENCES [dbo].[DeviceIdMap] ([DeviceId]),
    CONSTRAINT [FK_Device_EnumDeviceStatus] FOREIGN KEY ([EnumDeviceStatusId]) REFERENCES [dbo].[EnumDeviceStatus] ([EnumDeviceStatusId]),
    CONSTRAINT [FK_Device_EnumDeviceUseType] FOREIGN KEY ([EnumDeviceUseTypeId]) REFERENCES [dbo].[EnumDeviceUseType] ([EnumDeviceUseTypeId]),
    CONSTRAINT [FK_Device_Equipment] FOREIGN KEY ([EquipmentId]) REFERENCES [dbo].[Equipment] ([EquipmentId]),
    CONSTRAINT [FK_Device_Site] FOREIGN KEY ([SiteId]) REFERENCES [dbo].[Site] ([SiteId]),
    CONSTRAINT [FK_Device_WorkOrder] FOREIGN KEY ([WorkOrderId]) REFERENCES [dbo].[WorkOrder] ([WorkOrderId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_Device_Name]
    ON [dbo].[Device]([Name] ASC) WHERE ([Name] IS NOT NULL);

