﻿CREATE TABLE [dbo].[WorkOrderAdditionalUser] (
    [WorkOrderAdditionalUserId] INT IDENTITY (1, 1) NOT NULL,
    [WorkOrderId]               INT NOT NULL,
    [UserId]                    INT NOT NULL,
    CONSTRAINT [PKCX_WorkOrderAdditionalUser] PRIMARY KEY CLUSTERED ([WorkOrderAdditionalUserId] ASC),
    CONSTRAINT [FK_WorkOrderAdditionalUser_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId]),
    CONSTRAINT [FK_WorkOrderAdditionalUser_WorkOrderId] FOREIGN KEY ([WorkOrderId]) REFERENCES [dbo].[WorkOrder] ([WorkOrderId])
);

