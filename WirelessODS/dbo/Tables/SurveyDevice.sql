﻿CREATE TABLE [dbo].[SurveyDevice] (
    [SurveyDeviceId] INT           IDENTITY (1, 1) NOT NULL,
    [SurveyId]       INT           NOT NULL,
    [DeviceName]     VARCHAR (100) NOT NULL,
    [SSID]           VARCHAR (50)  NOT NULL,
    [RadioType]      VARCHAR (50)  NOT NULL,
    [IPAddress]      VARCHAR (50)  NOT NULL,
    [WirelessMAC]    VARCHAR (50)  NOT NULL,
    [ParentIP]       VARCHAR (50)  NOT NULL,
    [ParentDevice]   VARCHAR (100) NOT NULL,
    CONSTRAINT [PKCX_SurveyDevice_SurveyDeviceId] PRIMARY KEY CLUSTERED ([SurveyDeviceId] ASC),
    CONSTRAINT [FK_SurveyDevice_Survey] FOREIGN KEY ([SurveyId]) REFERENCES [dbo].[Survey] ([SurveyId])
);

