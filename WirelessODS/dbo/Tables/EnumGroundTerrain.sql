﻿CREATE TABLE [dbo].[EnumGroundTerrain] (
    [EnumGroundTerrainId] INT           NOT NULL,
    [SystemName]          VARCHAR (50)  NOT NULL,
    [DisplayName]         VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumGroundTerrain_EnumGroundTerrainId] PRIMARY KEY CLUSTERED ([EnumGroundTerrainId] ASC)
);

