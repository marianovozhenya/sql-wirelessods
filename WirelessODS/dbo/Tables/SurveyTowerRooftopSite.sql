﻿CREATE TABLE [dbo].[SurveyTowerRooftopSite] (
    [SurveyTowerRooftopSiteId]   INT            IDENTITY (1, 1) NOT NULL,
    [SurveyId]                   INT            NOT NULL,
    [Description]                VARCHAR (100)  NOT NULL,
    [BuildingName]               VARCHAR (50)   NOT NULL,
    [NumberOfStories]            TINYINT        NULL,
    [SquareFootageLeased]        DECIMAL (7, 2) NULL,
    [IsAntennaSpaceAvailable]    BIT            NULL,
    [EnumRooftopMountTypeId]     INT            NULL,
    [IsElevatorToEquipmentSpace] BIT            NULL,
    [IsPowerAvailable]           BIT            NULL,
    [PowerAvailableFt]           DECIMAL (7, 2) NULL,
    [IsUnrestrictedAccess]       BIT            NULL,
    [UnrestrictedAccessNote]     VARCHAR (100)  NOT NULL,
    CONSTRAINT [PKCX_SurveyTowerRooftopSite_SurveyTowerRooftopSiteId] PRIMARY KEY CLUSTERED ([SurveyTowerRooftopSiteId] ASC),
    CONSTRAINT [FK_SurveyTowerRooftopSite_EnumRooftopMountType] FOREIGN KEY ([EnumRooftopMountTypeId]) REFERENCES [dbo].[EnumRooftopMountType] ([EnumRooftopMountTypeId]),
    CONSTRAINT [FK_SurveyTowerRooftopSite_Survey] FOREIGN KEY ([SurveyId]) REFERENCES [dbo].[Survey] ([SurveyId])
);

