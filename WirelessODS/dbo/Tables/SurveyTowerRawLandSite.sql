﻿CREATE TABLE [dbo].[SurveyTowerRawLandSite] (
    [SurveyTowerRawLandSiteId] INT            IDENTITY (1, 1) NOT NULL,
    [SurveyId]                 INT            NOT NULL,
    [SquareFootageLeased]      DECIMAL (7, 2) NULL,
    [ProposedStructureHeight]  DECIMAL (7, 2) NULL,
    [IsPowerAvailable]         BIT            NULL,
    [PowerAvailableFt]         DECIMAL (7, 2) NULL,
    [EnumLandZonedId]          INT            NULL,
    [IsFloodConcern]           BIT            NULL,
    CONSTRAINT [PKCX_SurveyTowerRawLandSite_SurveyTowerRawLandSiteId] PRIMARY KEY CLUSTERED ([SurveyTowerRawLandSiteId] ASC),
    CONSTRAINT [FK_SurveyTowerRawLandSite_EnumLandZoned] FOREIGN KEY ([EnumLandZonedId]) REFERENCES [dbo].[EnumLandZoned] ([EnumLandZonedId]),
    CONSTRAINT [FK_SurveyTowerRawLandSite_Survey] FOREIGN KEY ([SurveyId]) REFERENCES [dbo].[Survey] ([SurveyId])
);

