﻿CREATE TABLE [dbo].[EnumAuditLogEntryType] (
    [EnumAuditLogEntryTypeId] INT           NOT NULL,
    [SystemName]              VARCHAR (50)  NOT NULL,
    [DisplayName]             VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumAuditLogEntryType_EnumAuditLogEntryTypeId] PRIMARY KEY CLUSTERED ([EnumAuditLogEntryTypeId] ASC)
);

