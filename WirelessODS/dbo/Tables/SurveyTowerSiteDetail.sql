﻿CREATE TABLE [dbo].[SurveyTowerSiteDetail] (
    [SurveyTowerSiteDetailId] INT            IDENTITY (1, 1) NOT NULL,
    [SurveyId]                INT            NOT NULL,
    [Cab1From]                VARCHAR (50)   NOT NULL,
    [Cab1ToCabinetFt]         DECIMAL (7, 2) NULL,
    [Cab2From]                VARCHAR (50)   NOT NULL,
    [Cab2ToCabinetFt]         DECIMAL (7, 2) NULL,
    [Cab3From]                VARCHAR (50)   NOT NULL,
    [Cab3ToCabinetFt]         DECIMAL (7, 2) NULL,
    [Cab4From]                VARCHAR (50)   NOT NULL,
    [Cab4ToCabinetFt]         DECIMAL (7, 2) NULL,
    [Notes]                   VARCHAR (MAX)  NOT NULL,
    CONSTRAINT [PKCX_SurveyTowerSiteDetail_SurveyTowerSiteDetailId] PRIMARY KEY CLUSTERED ([SurveyTowerSiteDetailId] ASC),
    CONSTRAINT [FK_SurveyTowerSiteDetail_Survey] FOREIGN KEY ([SurveyId]) REFERENCES [dbo].[Survey] ([SurveyId])
);

