﻿CREATE TABLE [dbo].[EnumWorkOrderType] (
    [EnumWorkOrderTypeId] INT           NOT NULL,
    [SystemName]          VARCHAR (50)  NOT NULL,
    [DisplayName]         VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumWorkOrderType_EnumWorkOrderTypeId] PRIMARY KEY CLUSTERED ([EnumWorkOrderTypeId] ASC)
);

