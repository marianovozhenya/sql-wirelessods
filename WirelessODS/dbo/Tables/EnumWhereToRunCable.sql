﻿CREATE TABLE [dbo].[EnumWhereToRunCable] (
    [EnumWhereToRunCableId] INT           NOT NULL,
    [SystemName]            VARCHAR (50)  NOT NULL,
    [DisplayName]           VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumWhereToRunCable_EnumWhereToRunCableId] PRIMARY KEY CLUSTERED ([EnumWhereToRunCableId] ASC)
);

