﻿CREATE TABLE [dbo].[SurveyTowerCompound] (
    [SurveyTowerCompoundId]    INT            IDENTITY (1, 1) NOT NULL,
    [SurveyId]                 INT            NOT NULL,
    [EnumEquipmentPlacementId] INT            NULL,
    [EquipmentOther]           VARCHAR (50)   NOT NULL,
    [CompoundLength]           DECIMAL (7, 2) NULL,
    [CompoundWidth]            DECIMAL (7, 2) NULL,
    [IsSpaceInCompound]        BIT            NULL,
    [IsAdaquateGrounding]      BIT            NULL,
    [IsSpaceInShelter]         BIT            NULL,
    [IsProximityIssue]         BIT            NULL,
    CONSTRAINT [PKCX_SurveyTowerCompound_SurveyTowerCompoundId] PRIMARY KEY CLUSTERED ([SurveyTowerCompoundId] ASC),
    CONSTRAINT [FK_SurveyTowerCompound_EnumEquipmentPlacement] FOREIGN KEY ([EnumEquipmentPlacementId]) REFERENCES [dbo].[EnumEquipmentPlacement] ([EnumEquipmentPlacementId]),
    CONSTRAINT [FK_SurveyTowerCompound_Survey] FOREIGN KEY ([SurveyId]) REFERENCES [dbo].[Survey] ([SurveyId])
);

