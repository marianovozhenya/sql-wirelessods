﻿CREATE TABLE [dbo].[WorkOrderSubTypeXPhase] (
    [WorkOrderSubTypeXPhaseId] INT IDENTITY (1, 1) NOT NULL,
    [WorkOrderSubTypeId]       INT NOT NULL,
    [WorkOrderPhaseId]         INT NOT NULL,
    CONSTRAINT [PKCX_WorkOrderSubTypeXPhase_WorkOrderSubTypeXPhaseId] PRIMARY KEY CLUSTERED ([WorkOrderSubTypeXPhaseId] ASC),
    CONSTRAINT [FK_WorkOrderSubTypeXPhase_WorkOrderPhase] FOREIGN KEY ([WorkOrderPhaseId]) REFERENCES [dbo].[WorkOrderPhase] ([WorkOrderPhaseId]),
    CONSTRAINT [FK_WorkOrderSubTypeXPhase_WorkOrderSubType] FOREIGN KEY ([WorkOrderSubTypeId]) REFERENCES [dbo].[WorkOrderSubType] ([WorkOrderSubTypeId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_WorkOrderSubType_WorkOrderSubTypeId_WorkOrderPhaseId]
    ON [dbo].[WorkOrderSubTypeXPhase]([WorkOrderSubTypeId] ASC, [WorkOrderPhaseId] ASC);

