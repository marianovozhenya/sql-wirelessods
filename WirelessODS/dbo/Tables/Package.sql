﻿CREATE TABLE [dbo].[Package] (
    [PackageId]             INT           IDENTITY (1, 1) NOT NULL,
    [EnumProductMarketId]   INT           NOT NULL,
    [Name]                  VARCHAR (255) NULL,
    [EnumProductCategoryId] INT           NULL,
    CONSTRAINT [PK_EnumPackage] PRIMARY KEY CLUSTERED ([PackageId] ASC),
    CONSTRAINT [FK_EnumProductCategory_EnumProductCategoryId] FOREIGN KEY ([EnumProductCategoryId]) REFERENCES [dbo].[EnumProductCategory] ([EnumProductCategoryId]),
    CONSTRAINT [FK_EnumProductMarket_EnumProductMarketId] FOREIGN KEY ([EnumProductMarketId]) REFERENCES [dbo].[EnumProductMarket] ([EnumProductMarketId])
);

