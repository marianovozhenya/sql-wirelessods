﻿CREATE TABLE [dbo].[ZipCodeXDistrict] (
    [ZipCodeXDistrictId] INT         IDENTITY (1, 1) NOT NULL,
    [DistrictId]         INT         NOT NULL,
    [ZipCode]            VARCHAR (5) NOT NULL,
    CONSTRAINT [PK_ZipCodeXDistrict_ZipCodeXDistrictId] PRIMARY KEY CLUSTERED ([ZipCodeXDistrictId] ASC),
    CONSTRAINT [FK_ZipCodeXDistrict_DistrictId] FOREIGN KEY ([DistrictId]) REFERENCES [dbo].[District] ([DistrictId]),
    CONSTRAINT [UNCX_ZipCodeXDistrict_ZipCode] UNIQUE NONCLUSTERED ([ZipCode] ASC)
);

