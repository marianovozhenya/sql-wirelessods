﻿CREATE TABLE [dbo].[WpdaLog] (
    [WpdaLogId]          INT           IDENTITY (1, 1) NOT NULL,
    [UserId]             INT           NOT NULL,
    [RequestContext]     VARCHAR (MAX) NOT NULL,
    [StartDateTimeLocal] DATETIME      NOT NULL,
    [EndDateTimeLocal]   DATETIME      NULL,
    [WasSuccessful]      BIT           NOT NULL,
    [ExceptionDetails]   VARCHAR (MAX) NOT NULL,
    CONSTRAINT [PKCX_WpdaLog_WpdaLogId] PRIMARY KEY CLUSTERED ([WpdaLogId] ASC)
);

