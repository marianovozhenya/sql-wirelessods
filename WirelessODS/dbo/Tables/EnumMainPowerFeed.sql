﻿CREATE TABLE [dbo].[EnumMainPowerFeed] (
    [EnumMainPowerFeedId] INT           NOT NULL,
    [SystemName]          VARCHAR (50)  NOT NULL,
    [DisplayName]         VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumMainPowerFeed_EnumMainPowerFeedId] PRIMARY KEY CLUSTERED ([EnumMainPowerFeedId] ASC)
);

