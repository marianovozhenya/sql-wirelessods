﻿CREATE TABLE [dbo].[Manufacturer] (
    [ManufacturerId]  INT           IDENTITY (1, 1) NOT NULL,
    [Name]            VARCHAR (100) NOT NULL,
    [CreatedAtUtc]    DATETIME      NOT NULL,
    [CreatedByUserId] INT           NOT NULL,
    CONSTRAINT [PKCX_Manufacturer_ManufacturerId] PRIMARY KEY CLUSTERED ([ManufacturerId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_Manufacturer_Name]
    ON [dbo].[Manufacturer]([Name] ASC);

