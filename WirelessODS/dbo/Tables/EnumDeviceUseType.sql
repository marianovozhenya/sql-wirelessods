﻿CREATE TABLE [dbo].[EnumDeviceUseType] (
    [EnumDeviceUseTypeId] INT           NOT NULL,
    [SystemName]          VARCHAR (50)  NOT NULL,
    [DisplayName]         VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumDeviceUseType_EnumDeviceUseTypeId] PRIMARY KEY CLUSTERED ([EnumDeviceUseTypeId] ASC)
);

