﻿CREATE TABLE [dbo].[WorkOrderPhase] (
    [WorkOrderPhaseId] INT          NOT NULL,
    [Name]             VARCHAR (50) NOT NULL,
    CONSTRAINT [PKCX_WorkOrderPhase_WorkOrderPhaseId] PRIMARY KEY CLUSTERED ([WorkOrderPhaseId] ASC)
);

