﻿CREATE TABLE [dbo].[PaymentProfile] (
    [PaymentProfileId]             INT           IDENTITY (1, 1) NOT NULL,
    [AuthorizeNetProfileId]        VARCHAR (50)  NOT NULL,
    [AuthorizeNetPaymentProfileId] VARCHAR (50)  NOT NULL,
    [CustomerId]                   INT           NOT NULL,
    [CardLastFour]                 INT           NOT NULL,
    [ExpirationMonth]              TINYINT       NOT NULL,
    [ExpirationYear]               INT           NOT NULL,
    [CardCode]                     INT           NOT NULL,
    [CreatedAtUtc]                 DATETIME      NOT NULL,
    [CreatedByUserId]              INT           NOT NULL,
    [CardHolder]                   VARCHAR (100) NOT NULL,
    [CardType]                     VARCHAR (30)  NOT NULL,
    CONSTRAINT [PKCX_PaymentProfile_PaymentProfileId] PRIMARY KEY CLUSTERED ([PaymentProfileId] ASC),
    CONSTRAINT [FK_PaymentProfile_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customer] ([CustomerId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_PaymentProfile_CustomerId]
    ON [dbo].[PaymentProfile]([CustomerId] ASC);

