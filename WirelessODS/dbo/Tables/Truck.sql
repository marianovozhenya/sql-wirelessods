﻿CREATE TABLE [dbo].[Truck] (
    [TruckId]             INT          IDENTITY (1, 1) NOT NULL,
    [TruckNumber]         VARCHAR (50) NOT NULL,
    [TruckMakeId]         INT          NOT NULL,
    [TruckModelId]        INT          NOT NULL,
    [Year]                INT          NOT NULL,
    [RegistrationDate]    DATETIME     NULL,
    [RegistrationState]   VARCHAR (2)  NOT NULL,
    [LicensePlate]        VARCHAR (15) NOT NULL,
    [Gvw]                 INT          NULL,
    [CreatedAtUtc]        DATETIME     NOT NULL,
    [CreatedByUserId]     INT          NOT NULL,
    [LastUpdatedAtUtc]    DATETIME     NULL,
    [LastUpdatedByUserId] INT          NULL,
    CONSTRAINT [PKCX_Truck_TruckId] PRIMARY KEY CLUSTERED ([TruckId] ASC),
    CONSTRAINT [FK_Truck_TruckMake] FOREIGN KEY ([TruckMakeId]) REFERENCES [dbo].[TruckMake] ([TruckMakeId]),
    CONSTRAINT [FK_Truck_TruckModel] FOREIGN KEY ([TruckModelId]) REFERENCES [dbo].[TruckModel] ([TruckModelId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_Truck_TruckNum]
    ON [dbo].[Truck]([TruckNumber] ASC);

