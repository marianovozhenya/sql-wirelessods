﻿CREATE TABLE [dbo].[WorkOrderTime] (
    [WorkOrderTimeId]  INT      IDENTITY (1, 1) NOT NULL,
    [WorkOrderId]      INT      NOT NULL,
    [WorkOrderPhaseId] INT      NOT NULL,
    [UserId]           INT      NOT NULL,
    [InAtUtc]          DATETIME NOT NULL,
    [OutAtUtc]         DATETIME NOT NULL,
    CONSTRAINT [PKCX_WorkOrderTime_WorkOrderTimeId] PRIMARY KEY CLUSTERED ([WorkOrderTimeId] ASC),
    CONSTRAINT [FK_WorkOrderTime_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([UserId]),
    CONSTRAINT [FK_WorkOrderTime_WorkOrder] FOREIGN KEY ([WorkOrderId]) REFERENCES [dbo].[WorkOrder] ([WorkOrderId]),
    CONSTRAINT [FK_WorkOrderTime_WorkOrderPhase] FOREIGN KEY ([WorkOrderPhaseId]) REFERENCES [dbo].[WorkOrderPhase] ([WorkOrderPhaseId])
);

