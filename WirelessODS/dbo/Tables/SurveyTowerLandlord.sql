﻿CREATE TABLE [dbo].[SurveyTowerLandlord] (
    [SurveyTowerLandlordId]    INT           IDENTITY (1, 1) NOT NULL,
    [SurveyId]                 INT           NOT NULL,
    [GroundLandlordName]       VARCHAR (100) NOT NULL,
    [GroundLandlordContact]    VARCHAR (100) NOT NULL,
    [StructureLandlordName]    VARCHAR (100) NOT NULL,
    [StructureLandlordContact] VARCHAR (100) NOT NULL,
    CONSTRAINT [PKCX_SurveyTowerLandlord_SurveyTowerLandlordId] PRIMARY KEY CLUSTERED ([SurveyTowerLandlordId] ASC),
    CONSTRAINT [FK_SurveyTowerLandlord_Survey] FOREIGN KEY ([SurveyId]) REFERENCES [dbo].[Survey] ([SurveyId])
);

