﻿CREATE TABLE [dbo].[SiteAttribute] (
    [SiteAttributeId] INT            IDENTITY (1, 1) NOT NULL,
    [SiteId]          INT            NOT NULL,
    [Latitude]        DECIMAL (9, 6) NULL,
    [Longitude]       DECIMAL (9, 6) NULL,
    [Owner]           VARCHAR (100)  NOT NULL,
    [OwnerPhone]      VARCHAR (20)   NOT NULL,
    [CreatedAtUtc]    DATETIME       NOT NULL,
    [CreatedByUserId] INT            NOT NULL,
    CONSTRAINT [PKCX_SiteAttribute_SiteAttributeId] PRIMARY KEY CLUSTERED ([SiteAttributeId] ASC),
    CONSTRAINT [FK_SiteAttribute_Site] FOREIGN KEY ([SiteId]) REFERENCES [dbo].[Site] ([SiteId])
);

