﻿CREATE TABLE [dbo].[EquipmentChangeLog] (
    [EquipmentChangeLogId] INT          IDENTITY (1, 1) NOT NULL,
    [EquipmentId]          INT          NOT NULL,
    [ChangeTypeId]         INT          NOT NULL,
    [Change]               VARCHAR (50) NOT NULL,
    [ChangeByUserId]       INT          NOT NULL,
    [ChangedDate]          DATETIME     NOT NULL,
    CONSTRAINT [FK_EquipmentChangeLog_Equipment] FOREIGN KEY ([EquipmentId]) REFERENCES [dbo].[Equipment] ([EquipmentId])
);

