﻿CREATE TABLE [dbo].[EnumUploadStatus] (
    [EnumUploadStatusId] INT           NOT NULL,
    [SystemName]         VARCHAR (50)  NOT NULL,
    [DisplayName]        VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumUploadStatus_EnumUploadStatusId] PRIMARY KEY CLUSTERED ([EnumUploadStatusId] ASC)
);

