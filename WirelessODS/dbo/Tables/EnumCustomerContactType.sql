﻿CREATE TABLE [dbo].[EnumCustomerContactType] (
    [EnumCustomerContactTypeId] INT           NOT NULL,
    [SystemName]                VARCHAR (50)  NOT NULL,
    [DisplayName]               VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumCustomerContactType_EnumCustomerContactTypeId] PRIMARY KEY CLUSTERED ([EnumCustomerContactTypeId] ASC)
);

