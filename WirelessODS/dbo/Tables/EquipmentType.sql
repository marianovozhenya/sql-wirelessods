﻿CREATE TABLE [dbo].[EquipmentType] (
    [EquipmentTypeId] INT           IDENTITY (1, 1) NOT NULL,
    [Name]            VARCHAR (100) NOT NULL,
    [CreatedAtUtc]    DATETIME      NOT NULL,
    [CreatedByUserId] INT           NOT NULL,
    CONSTRAINT [PKCX_EquipmentType_EquipmentTypeId] PRIMARY KEY CLUSTERED ([EquipmentTypeId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_EquipmentType_Name]
    ON [dbo].[EquipmentType]([Name] ASC);

