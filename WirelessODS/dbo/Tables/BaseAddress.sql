﻿CREATE TABLE [dbo].[BaseAddress] (
    [BaseAddressId]                  INT           IDENTITY (1, 1) NOT NULL,
    [BaseCompositeKey]               VARCHAR (50)  NOT NULL,
    [StreetNumber]                   VARCHAR (100) NULL,
    [PreDirection]                   VARCHAR (5)   NULL,
    [StreetName]                     VARCHAR (120) NULL,
    [StreetSuffix]                   VARCHAR (20)  NULL,
    [PostDirection]                  VARCHAR (5)   NULL,
    [City]                           VARCHAR (50)  NOT NULL,
    [State]                          VARCHAR (2)   NOT NULL,
    [Zip]                            VARCHAR (10)  NOT NULL,
    [BaseAddressFull]                VARCHAR (MAX) NULL,
    [EnumAddressValidationServiceId] INT           NOT NULL,
    [ValidationDate]                 DATE          NULL,
    [ValidationKey]                  VARCHAR (100) NULL,
    [BaseAddressLine]                VARCHAR (MAX) NULL,
    CONSTRAINT [PKCX_BaseAddress_BaseAddressId] PRIMARY KEY CLUSTERED ([BaseAddressId] ASC),
    CONSTRAINT [FK_BaseAddress_EnumAddressValidationService] FOREIGN KEY ([EnumAddressValidationServiceId]) REFERENCES [dbo].[EnumAddressValidationService] ([EnumAddressValidationServiceId])
);


GO
CREATE NONCLUSTERED INDEX [NCX_BaseAddress_Zip]
    ON [dbo].[BaseAddress]([Zip] ASC);


GO
CREATE NONCLUSTERED INDEX [NCX_BaseAddress_City]
    ON [dbo].[BaseAddress]([City] ASC);


GO
CREATE NONCLUSTERED INDEX [NCX_BaseAddress_StreetName]
    ON [dbo].[BaseAddress]([StreetName] ASC);


GO
CREATE NONCLUSTERED INDEX [NCX_BaseAddress_StreetNumber]
    ON [dbo].[BaseAddress]([StreetNumber] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_BaseAddress_BaseCompositeKey]
    ON [dbo].[BaseAddress]([BaseCompositeKey] ASC);

