﻿CREATE TABLE [dbo].[DispatchCode] (
    [DispatchCodeId]           INT           NOT NULL,
    [Name]                     VARCHAR (100) NOT NULL,
    [WorkOrderSubTypeId]       INT           NOT NULL,
    [ClickSchedulingProfileId] INT           CONSTRAINT [DEF_DispatchCode_ClickSchedulingProfileId] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PKCX_DispatchCode_DispatchCodeId] PRIMARY KEY CLUSTERED ([DispatchCodeId] ASC),
    CONSTRAINT [FK_DispatchCode_ClickSchedulingProfile] FOREIGN KEY ([ClickSchedulingProfileId]) REFERENCES [dbo].[ClickSchedulingProfile] ([ClickSchedulingProfileId]),
    CONSTRAINT [FK_DispatchCode_WorkOrderSubType] FOREIGN KEY ([WorkOrderSubTypeId]) REFERENCES [dbo].[WorkOrderSubType] ([WorkOrderSubTypeId])
);


GO
CREATE NONCLUSTERED INDEX [NCX_DispatchCode_WorkOrderSubTypeId]
    ON [dbo].[DispatchCode]([WorkOrderSubTypeId] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_DispatchCode_Name]
    ON [dbo].[DispatchCode]([Name] ASC);

