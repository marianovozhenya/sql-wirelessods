﻿CREATE TABLE [dbo].[User] (
    [UserId]                      INT           IDENTITY (1000, 1) NOT NULL,
    [FirstName]                   VARCHAR (50)  NOT NULL,
    [LastName]                    VARCHAR (50)  NOT NULL,
    [PhoneNumber]                 VARCHAR (20)  NOT NULL,
    [CellNumber]                  VARCHAR (20)  NOT NULL,
    [EmailAddress]                VARCHAR (100) NOT NULL,
    [LastFailedLoginAttemptAtUtc] DATETIME      NULL,
    [FailedLoginAttemptCounter]   INT           NOT NULL,
    [LastLoginAtUtc]              DATETIME      NULL,
    [IsEnabled]                   BIT           NOT NULL,
    [ManagerId]                   INT           NULL,
    [CreatedByUserId]             INT           NOT NULL,
    [CreatedAtUtc]                DATETIME      NOT NULL,
    [LastUpdatedByUserId]         INT           NULL,
    [LastUpdatedAtUtc]            DATETIME      NULL,
    [TruckId]                     INT           NULL,
    [CrewId]                      INT           NULL,
    [CurrentWorkOrderId]          INT           NULL,
    [CurrentWorkOrderPhaseId]     INT           NULL,
    [EnteredPhaseAtUtc]           DATETIME      NULL,
    [DistrictId]                  INT           NULL,
    CONSTRAINT [PKCX_User_UserId] PRIMARY KEY CLUSTERED ([UserId] ASC),
    CONSTRAINT [FK_User_Crew] FOREIGN KEY ([CrewId]) REFERENCES [dbo].[Crew] ([CrewId]),
    CONSTRAINT [FK_User_CurrentWorkOrderId] FOREIGN KEY ([CurrentWorkOrderId]) REFERENCES [dbo].[WorkOrder] ([WorkOrderId]),
    CONSTRAINT [FK_User_CurrentWorkOrderPhaseId] FOREIGN KEY ([CurrentWorkOrderPhaseId]) REFERENCES [dbo].[WorkOrderPhase] ([WorkOrderPhaseId]),
    CONSTRAINT [FK_User_District] FOREIGN KEY ([DistrictId]) REFERENCES [dbo].[District] ([DistrictId]),
    CONSTRAINT [FK_User_User_CreatedByUserId] FOREIGN KEY ([CreatedByUserId]) REFERENCES [dbo].[User] ([UserId]),
    CONSTRAINT [FK_User_User_LastUpdatedByUserId] FOREIGN KEY ([LastUpdatedByUserId]) REFERENCES [dbo].[User] ([UserId]),
    CONSTRAINT [FK_User_User_ManagerId] FOREIGN KEY ([ManagerId]) REFERENCES [dbo].[User] ([UserId])
);


GO
CREATE NONCLUSTERED INDEX [NCX_User_CrewId]
    ON [dbo].[User]([CrewId] ASC);

