﻿CREATE TABLE [dbo].[EnumCredentialType] (
    [EnumCredentialTypeId] INT           NOT NULL,
    [SystemName]           VARCHAR (50)  NOT NULL,
    [DisplayName]          VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumCredentialType_EnumCredentialTypeId] PRIMARY KEY CLUSTERED ([EnumCredentialTypeId] ASC)
);

