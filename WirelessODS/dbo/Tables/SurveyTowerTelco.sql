﻿CREATE TABLE [dbo].[SurveyTowerTelco] (
    [SurveyTowerTelcoId]          INT            IDENTITY (1, 1) NOT NULL,
    [SurveyId]                    INT            NOT NULL,
    [EnumEnclosureId]             INT            NULL,
    [LengthFromTelcoToNewCabinet] DECIMAL (7, 2) NULL,
    [EstimatedCost]               SMALLMONEY     NULL,
    [Notes]                       VARCHAR (MAX)  NOT NULL,
    [TelcoCompany]                VARCHAR (50)   NOT NULL,
    CONSTRAINT [PKCX_SurveyTowerTelco_SurveyTowerTelcoId] PRIMARY KEY CLUSTERED ([SurveyTowerTelcoId] ASC),
    CONSTRAINT [FK_SurveyTowerTelco_EnumEnclosure] FOREIGN KEY ([EnumEnclosureId]) REFERENCES [dbo].[EnumEnclosure] ([EnumEnclosureId]),
    CONSTRAINT [FK_SurveyTowerTelco_Survey] FOREIGN KEY ([SurveyId]) REFERENCES [dbo].[Survey] ([SurveyId])
);

