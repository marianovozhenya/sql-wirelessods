﻿CREATE TABLE [dbo].[EnumWorkOrderCommentType] (
    [EnumWorkOrderCommentTypeId] INT           NOT NULL,
    [SystemName]                 VARCHAR (50)  NOT NULL,
    [DisplayName]                VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumWorkOrderCommentType_WorkOrderCommentTypeId] PRIMARY KEY CLUSTERED ([EnumWorkOrderCommentTypeId] ASC)
);

