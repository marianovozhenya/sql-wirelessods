﻿CREATE TABLE [dbo].[TruckModel] (
    [TruckModelId]    INT          IDENTITY (1, 1) NOT NULL,
    [Name]            VARCHAR (50) NOT NULL,
    [CreatedAtUtc]    DATETIME     NOT NULL,
    [CreatedByUserId] INT          NOT NULL,
    [TruckMakeId]     INT          NOT NULL,
    CONSTRAINT [PKCX_TruckModel_TruckModelId] PRIMARY KEY CLUSTERED ([TruckModelId] ASC),
    CONSTRAINT [FK_TruckModel_TruckMakeId] FOREIGN KEY ([TruckMakeId]) REFERENCES [dbo].[TruckMake] ([TruckMakeId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_TruckModel_Name]
    ON [dbo].[TruckModel]([Name] ASC);

