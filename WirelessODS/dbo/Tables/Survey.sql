﻿CREATE TABLE [dbo].[Survey] (
    [SurveyId]           INT      IDENTITY (1, 1) NOT NULL,
    [SiteId]             INT      NOT NULL,
    [WorkOrderId]        INT      NULL,
    [EnumSurveyTypeId]   INT      NOT NULL,
    [EnumSurveyStatusId] INT      NOT NULL,
    [CreatedAtUtc]       DATETIME NOT NULL,
    [CreatedByUserId]    INT      NOT NULL,
    [ClosedAtUtc]        DATETIME NULL,
    [ClosedByUserId]     INT      NULL,
    [IsCurrent]          BIT      NOT NULL,
    CONSTRAINT [PKCX_Survey_SurveyId] PRIMARY KEY CLUSTERED ([SurveyId] ASC),
    CONSTRAINT [FK_Survey_EnumSurveyStatus] FOREIGN KEY ([EnumSurveyStatusId]) REFERENCES [dbo].[EnumSurveyStatus] ([EnumSurveyStatusId]),
    CONSTRAINT [FK_Survey_EnumSurveyType] FOREIGN KEY ([EnumSurveyTypeId]) REFERENCES [dbo].[EnumSurveyType] ([EnumSurveyTypeId]),
    CONSTRAINT [FK_Survey_Site] FOREIGN KEY ([SiteId]) REFERENCES [dbo].[Site] ([SiteId]),
    CONSTRAINT [FK_Survey_WorkOrder] FOREIGN KEY ([WorkOrderId]) REFERENCES [dbo].[WorkOrder] ([WorkOrderId])
);

