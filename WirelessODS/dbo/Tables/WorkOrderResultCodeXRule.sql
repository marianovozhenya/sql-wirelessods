﻿CREATE TABLE [dbo].[WorkOrderResultCodeXRule] (
    [WorkOrderResultCodeXRuleId] INT IDENTITY (1, 1) NOT NULL,
    [WorkOrderResultCodeId]      INT NOT NULL,
    [ResultCodeRuleId]           INT NOT NULL,
    CONSTRAINT [PKCX_WorkOrderResultCodeXRule_WorkOrderResultCodeXRuleId] PRIMARY KEY CLUSTERED ([WorkOrderResultCodeXRuleId] ASC),
    CONSTRAINT [FK_WorkOrderResultCodeXRule_ResultCodeRule] FOREIGN KEY ([ResultCodeRuleId]) REFERENCES [dbo].[ResultCodeRule] ([ResultCodeRuleId]),
    CONSTRAINT [FK_WorkOrderResultCodeXRule_WorkOrderResultCode] FOREIGN KEY ([WorkOrderResultCodeId]) REFERENCES [dbo].[WorkOrderResultCode] ([WorkOrderResultCodeId])
);

