﻿CREATE TABLE [dbo].[Photo] (
    [PhotoId]                 INT             IDENTITY (1, 1) NOT NULL,
    [Name]                    VARCHAR (100)   NOT NULL,
    [PhotoTypeId]             INT             NOT NULL,
    [S3Bucket]                VARCHAR (100)   NOT NULL,
    [S3LocationForMediumSize] VARCHAR (MAX)   NOT NULL,
    [S3LocationForFullSize]   VARCHAR (MAX)   NOT NULL,
    [LocationId]              INT             NOT NULL,
    [SiteId]                  INT             NULL,
    [EnumUploadStatusId]      INT             NOT NULL,
    [IsSoftDeleted]           BIT             NOT NULL,
    [CreatedAtUtc]            DATETIME        NOT NULL,
    [CreatedByUserId]         INT             NOT NULL,
    [DeletedAtUtc]            DATETIME        NULL,
    [Thumbnail]               VARBINARY (MAX) NOT NULL,
    [WorkOrderId]             INT             NULL,
    CONSTRAINT [PKCX_Photo_PhotoId] PRIMARY KEY CLUSTERED ([PhotoId] ASC),
    CONSTRAINT [FK_Photo_EnumUploadStatus] FOREIGN KEY ([EnumUploadStatusId]) REFERENCES [dbo].[EnumUploadStatus] ([EnumUploadStatusId]),
    CONSTRAINT [FK_Photo_Location] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([LocationId]),
    CONSTRAINT [FK_Photo_PhotoType] FOREIGN KEY ([PhotoTypeId]) REFERENCES [dbo].[PhotoType] ([PhotoTypeId]),
    CONSTRAINT [FK_Photo_Site] FOREIGN KEY ([SiteId]) REFERENCES [dbo].[Site] ([SiteId]),
    CONSTRAINT [FK_Photos_WorkOrderId] FOREIGN KEY ([WorkOrderId]) REFERENCES [dbo].[WorkOrder] ([WorkOrderId])
);


GO
CREATE NONCLUSTERED INDEX [NCX_Photo_SiteId]
    ON [dbo].[Photo]([SiteId] ASC);


GO
CREATE NONCLUSTERED INDEX [NCX_Photo_LocationId]
    ON [dbo].[Photo]([LocationId] ASC);


GO
CREATE NONCLUSTERED INDEX [NCX_Photo_PhotoTypeId]
    ON [dbo].[Photo]([PhotoTypeId] ASC);

