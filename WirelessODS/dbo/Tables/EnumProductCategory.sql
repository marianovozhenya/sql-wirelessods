﻿CREATE TABLE [dbo].[EnumProductCategory] (
    [EnumProductCategoryId] INT           NOT NULL,
    [SystemName]            VARCHAR (50)  NOT NULL,
    [DisplayName]           VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumProductCategory_EnumProductCategoryId] PRIMARY KEY CLUSTERED ([EnumProductCategoryId] ASC)
);

