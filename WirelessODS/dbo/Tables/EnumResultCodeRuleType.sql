﻿CREATE TABLE [dbo].[EnumResultCodeRuleType] (
    [EnumResultCodeRuleTypeId] INT           NOT NULL,
    [SystemName]               VARCHAR (50)  NOT NULL,
    [DisplayName]              VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumResultCodeRuleType_EnumResultCodeRuleTypeId] PRIMARY KEY CLUSTERED ([EnumResultCodeRuleTypeId] ASC)
);

