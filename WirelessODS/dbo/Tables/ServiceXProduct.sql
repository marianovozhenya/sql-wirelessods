﻿CREATE TABLE [dbo].[ServiceXProduct] (
    [ServiceXProductId]    INT      IDENTITY (1, 1) NOT NULL,
    [ServiceId]            INT      NOT NULL,
    [ProductId]            INT      NOT NULL,
    [CreatedInAzotelAtUtc] DATETIME NULL,
    CONSTRAINT [PKCX_ServiceXProduct_ServiceXProductId] PRIMARY KEY CLUSTERED ([ServiceXProductId] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_ServiceXProduct_ServiceId_ProductId]
    ON [dbo].[ServiceXProduct]([ServiceId] ASC, [ProductId] ASC);

