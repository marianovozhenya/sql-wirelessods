﻿CREATE TABLE [dbo].[WorkOrderSubType] (
    [WorkOrderSubTypeId]  INT          IDENTITY (1, 1) NOT NULL,
    [Name]                VARCHAR (50) NOT NULL,
    [EnumWorkOrderTypeId] INT          NOT NULL,
    CONSTRAINT [PKCX_WorkOrderSubType_WorkOrderSubTypeId] PRIMARY KEY CLUSTERED ([WorkOrderSubTypeId] ASC),
    CONSTRAINT [FK_WorkOrderSubType_EnumWorkOrderType] FOREIGN KEY ([EnumWorkOrderTypeId]) REFERENCES [dbo].[EnumWorkOrderType] ([EnumWorkOrderTypeId])
);


GO
CREATE NONCLUSTERED INDEX [NCX_WorkOrderSubType_EnumWorkOrderTypeId]
    ON [dbo].[WorkOrderSubType]([EnumWorkOrderTypeId] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_WorkOrderSubType_Name_EnumWorkOrderTypeId]
    ON [dbo].[WorkOrderSubType]([Name] ASC, [EnumWorkOrderTypeId] ASC);

