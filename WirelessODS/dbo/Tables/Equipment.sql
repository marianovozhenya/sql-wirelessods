﻿CREATE TABLE [dbo].[Equipment] (
    [EquipmentId]         INT           IDENTITY (1, 1) NOT NULL,
    [Name]                VARCHAR (100) NOT NULL,
    [VendorId]            INT           NULL,
    [ManufacturerId]      INT           NOT NULL,
    [Sku]                 VARCHAR (50)  NOT NULL,
    [Model]               VARCHAR (50)  NOT NULL,
    [EquipmentTypeId]     INT           NOT NULL,
    [IsActive]            BIT           NOT NULL,
    [CreatedAtUtc]        DATETIME      NOT NULL,
    [CreatedByUserId]     INT           NOT NULL,
    [LastUpdatedAtUtc]    DATETIME      NULL,
    [LastUpdatedByUserId] INT           NULL,
    [IsDevice]            BIT           CONSTRAINT [DF_Equipment_IsDevice] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PKCX_Equipment_EquipmentId] PRIMARY KEY CLUSTERED ([EquipmentId] ASC),
    CONSTRAINT [FK_Equipment_EquipmentType] FOREIGN KEY ([EquipmentTypeId]) REFERENCES [dbo].[EquipmentType] ([EquipmentTypeId]),
    CONSTRAINT [FK_Equipment_Manufacturer] FOREIGN KEY ([ManufacturerId]) REFERENCES [dbo].[Manufacturer] ([ManufacturerId]),
    CONSTRAINT [FK_Equipment_Vendor] FOREIGN KEY ([VendorId]) REFERENCES [dbo].[Vendor] ([VendorId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [UNCX_Equipment_Name]
    ON [dbo].[Equipment]([Name] ASC);

