﻿CREATE TABLE [dbo].[EnumWorkOrderStatus] (
    [EnumWorkOrderStatusId] INT           NOT NULL,
    [SystemName]            VARCHAR (50)  NOT NULL,
    [DisplayName]           VARCHAR (255) NOT NULL,
    CONSTRAINT [PKCX_EnumWorkOrderStatus_EnumWorkOrderStatusId] PRIMARY KEY CLUSTERED ([EnumWorkOrderStatusId] ASC)
);

